#No need to launch Outlook
import win32com.client
import time

f = open("Report\\timestamp.txt", 'r')
ts = f.readline()
f.close()

ISOTIMEFORMAT='%Y-%m-%d %X'
oOutlook = win32com.client.Dispatch("Outlook.Application")
myNamespace = oOutlook.GetNamespace("MAPI")
myNamespace.Logon("asia1", "svc") #Use account "asia1" as Organizer
print "Logon Profile '%s'" %(myNamespace.CurrentProfileName)
appt = oOutlook.CreateItem(1) # 1 - olAppointmentItem
appt.Start = time.strftime( ISOTIMEFORMAT, time.localtime() )
#appt.Start = '2015-03-29 12:00'
appt.Subject = 'S03' + ts
appt.Duration = 30
appt.Location = 'tj'
appt.MeetingStatus = 1 # 1 - olMeeting; Changing the appointment to meeting
#only after changing the meeting status recipients can be added
appt.Recipients.Add("katharine4@asia.qagood.com")
appt.Save()
appt.Send()
time.sleep(30)
print ("Successfully sent meeting request S03%s") %(ts)
time.sleep(60)
print "Quit account 'asia1'"
myNamespace.Logoff()
oOutlook.Quit()
