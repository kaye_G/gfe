# Please make sure Outlook has two profile 'asia1' and 'benw01';

import os, sys
import smtplib
import time, datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

#Map a network drive then set path to watch
#path_to_watch = "C:\\GFE\\Build\\"
path_to_watch = "Z:\\"

#Set build number for upgrading testing
#build Dev#5245 is the same date build which Trunk #415 on May 14th
#upgrading_build_number = "5245"
upgrading_build_number = "415"


#Set deviceid
#Nexus 4 003ddc48d609b81d
#Nexus 5 019ac524f0a69398
#Samsung Note 4 6460c626
#Samsung S5 43cf964d
deviceid = "6460c626"



def testdatapreparation (buildversion):
	print "-----Start Test Data Preparation-----"
    # Write build version to version.txt
	fv = file('Report\\version.txt', 'w+')
	fv.write(buildversion)
	fv.close()

	# Write timestamp to timestamp.txt
	ft = file("Report\\timestamp.txt", 'w')
	ft.write(time.strftime('%d%H%M%S',time.localtime(time.time())))
	ft.close()

	# Force close Outlook
	print "-----Force close Outlook-----"
	os.system('taskkill /im OUTLOOK.EXE')
	
    # Uninstall and install test build on device
	print "-----Uninstall old build from device-----"
	os.system ('adb -s ' + deviceid +' uninstall com.good.android.gfe')
	print "-----Install new build on device-----"
	os.system ('adb -s ' + deviceid + ' install -r ' + path_to_watch + buildversion + '\\gfe-release.apk')
	
    # Remove report.txt and log.txt on device
	print "-----Remove report.txt and log.txt and on device-----"
	os.system ('adb shell rm /data/local/tmp/report.txt')
	os.system ('adb shell rm /data/local/tmp/log.txt')
	os.system ('adb shell rm /data/local/tmp/result.txt')
	os.system ('adb shell rm /data/local/tmp/ProvisionResult.txt')
	os.system ('adb push Report\\timestamp.txt /data/local/tmp/timestamp.txt')
	
	#os.system ('adb shell rm /data/local/tmp/*.*')
	#os.system ('adb -s ' + deviceid +' push ./TestCase4GFE/bin/GFESmoke.jar /data/local/tmp/')
	
	
	# Logon profile 'asia1'
	print "-----Send Meeting Request 02-----"
	os.system ('python SMOKE02.py')
	print "-----Send Meeting Request 03-----"
	os.system ('python SMOKE03.py') # Quit profile 'asia1' after send mr 03
	time.sleep(5)
	
	# Send test data
	print "-----Send Email 07-----"
	os.system ('python SMOKE07.py')
	print "-----Send Email 08-----"
	os.system ('python SMOKE08.py')
	print "-----Send Email 11-----"
	os.system ('python SMOKE11.py')
	
	# Logon profile 'benw01'
	print "-----Send Meeting Request 01-----"
	os.system ('python SMOKE01.py') # Profile 'benw01' is quit after the MR sent out
	print "-----Send Meeting Request 04-----"
	os.system ('python SMOKE04Original.py')
	print "-----Send Meeting Request 05-----"
	os.system ('python SMOKE05.py')
	
	# Send test data
	print "-----Send Email 13-----"
	os.system ('python SMOKE13.py')
	print "-----Send Email 15-----"
	os.system ('python SMOKE15.py')
	print "-----Test Data preparation is completed!-----"
	
	
def provisiongfe():
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.Provision')
	os.system('adb -s ' + deviceid +' pull /data/local/tmp/ProvisionResult.txt C:/GFE/Report')


def testcaseexecution (buildversion):
	print "-----Start testcaseexecution-----"	
	# Email folder initial sync & Accept meeting request SmokeID04
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.Preparation')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID04Preparation')
	# Send meeting update SmokeID04
	print "-----Send Meeting Update 04-----"
	os.system ('python SMOKE04Update.py') #Close the profile after meeting update is sent out
	time.sleep(20)
	
# Start run testcase
	print "-----Below testcase will automatically run, no need to interrupt-----"
	# Sync previous test data in profile 'asia1' on Outlook
	os.system ('python Recover\\Send_Recv_asia1.py')
	os.system ('python Recover\\launchOL.py')
	
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID01AcceptSingleMR')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID02DownloadAttachmentInOccurrence')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID03CancelMeeting')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID04UpdateMeeting')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID05AcceptMeetingInCalendar')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID06SubscribeContactSubfolder')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID07OpenDocument')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID08ForwardEmail')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID09AddImageFromGallary')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID10SendThrough')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID11EMLViewer')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID12SendEmailToMultipleRecipients')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID15ReplyIcon')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID13MoveEmail')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID14DeleteEmail')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID16NewEmailAddFile')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID25EmailSubscribeFolder')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID26OutOfOffice')
	#os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID18EasyActivationSamsungS5Note4')
	#os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID19EmailAttachFileFromGoodShare')
	#os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID20EmailSaveFileToGoodShare')
	#os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID21SendFileandLinktoGFEwhenGFElock')
	#os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID22ProvisionWifiOFFSamsungS5Note4')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID23Resync')
	# GET test result - Send GFE log
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.SendLogs')
	#os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID29CopyPasteSamsungS5Note4')
	# Below is precondition for ID24GFEUpgrade
	os.system ('adb -s ' + deviceid +' uninstall com.good.android.gfe')
	os.system ('adb -s ' + deviceid +' install -r ' + path_to_watch + upgrading_build_number + '\\gfe-release.apk')
	time.sleep(3)
	os.system ('adb shell rm /data/local/tmp/ProvisionResult.txt')
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.Provision')
	time.sleep(60)
	#os.system ('adb -s ' + deviceid +' install -r ' + path_to_watch + upgrading_build_number + '\\gfe-release.apk')
	os.system ('adb -s ' + deviceid +' install -r ' + path_to_watch + buildversion + '\\gfe-release.apk')
	# Start run testcase ID24GFEUpgrade
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.ID24GFEUpgrade')
	# GET test result - Send GFE log
	os.system ('adb -s ' + deviceid +' shell uiautomator runtest GFESmoke.jar -c com.good.android.gfetest.SendLogs')
	
# Clean exist Email, Events, Contacts, Tasks on test account 'asia1'
	print "-----Clean exist Events on test account-----"
	os.system ('python Recover\\CleanCalendar.py') # Quit profile 'asia1'
	time.sleep(3)
	print "-----Clean exist Email on test account-----"
	os.system ('python Recover\\CleanEmail.py')
	time.sleep(3)
	print "-----Clean exist Contacts on test account-----"
	os.system ('python Recover\\CleanContacts.py')
	time.sleep(3)
	print "-----Clean exist Tasks on test account-----"
	os.system ('python Recover\\CleanTask.py')
	time.sleep(20)
	
# Clean exist Email, Events, Contacts, Tasks on test account 'benw01'
	os.system ('python Recover\\Send_Recv_ben1.py')
	os.system ('python Recover\\launchOL.py')
	time.sleep(60)
	print "-----Clean exist Contacts on test account-----"
	os.system ('python Recover\\CleanContacts-ben1.py')
	time.sleep(3)
	print "-----Clean exist Tasks on test account-----"
	os.system ('python Recover\\CleanTask-ben1.py')
	time.sleep(3)
	print "-----Clean exist Email on test account-----"
	os.system ('python Recover\\CleanEmail-ben1.py')
	time.sleep(3)
	print "-----Clean exists Events on Organizer's Outlook-----"
	os.system ('python Recover\\CleanCalendar-ben1.py')
	time.sleep(30)
	
	#Pull testing report to local drive
	#os.system('adb -s ' + deviceid +' pull /data/local/tmp/report.txt C:/GFE/Report')
	os.system('adb -s ' + deviceid +' pull /data/local/tmp/result.txt C:/GFE/Report')
	os.system('adb -s ' + deviceid +' pull /data/local/tmp/log.txt C:/GFE/Report')

# Send testing result by email
	print "-----Will send testing report by email-----"
	os.system ('call Report\\GenerateReport_Core.bat')

	#fromaddr='gfeandroid@163.com'
	#toaddrs = '1ben@hyperv.qagood.com'

	#recipients = ['benwang@good.com','gfeandroid@163.com','kaye@good.com']
	#subject = ['TestingReport', time.ctime()]

	#msg = MIMEMultipart('alternative')
	#msg['Subject'] = ", ".join(subject)
	#msg['From'] = fromaddr
	#msg['To'] = toaddrs
	#msg['To'] = ", ".join(recipients)

	#att1 = MIMEText(open('C:\\gfe\\report\\report.txt', 'rb').read(), 'base64', 'utf-8')
	#att1["Content-Type"] = 'application/octet-stream'
	#att1["Content-Disposition"] = 'attachment; filename="TestingReport.txt"'# 
	#msg.attach(att1)

	#html="<html><h1>TestingReport attached</h1></html>"
	#mime = MIMEText(html,'html')
	#msg.attach(mime)

	#fileName = 'C:\\gfe\\report\\report.txt'
	#with open(fileName, 'r') as fp:
	#	attachment = MIMEText(fp.read())
	#	fp.close()
	#	msg.attach(attachment)
		
		

	#Account Credentials
	#username = 'gfeandroid@163.com'
	#password = 'good111111'

	#server = smtplib.SMTP('smtp.163.com')
	#server = smtplib.SMTP('smtp.gmail.com:587')
	#server.starttls()
	#server.login(username,password)
	#server.sendmail(fromaddr, toaddrs, msg.as_string())
	#server.sendmail(fromaddr, recipients, msg.as_string())

	#server.quit()

def recovery():
	# Force close Outlook
	print "-----Force close Outlook-----"
	os.system('taskkill /im OUTLOOK.EXE')
	time.sleep(10)
	os.system ('python Recover\\Send_Recv_asia1.py')
	os.system ('python Recover\\launchOL.py')
	time.sleep(60)
	#Clean exist Email, Events, Contacts, Tasks on test account again
	print "-----Clean exist Events on test account-----"
	os.system ('python Recover\\CleanCalendar.py')
	time.sleep(3)
	print "-----Clean exist Email on test account-----"
	os.system ('python Recover\\CleanEmail.py')
	time.sleep(3)
	print "-----Clean exist Contacts on test account-----"
	os.system ('python Recover\\CleanContacts.py')
	time.sleep(3)
	print "-----Clean exist Tasks on test account-----"
	os.system ('python Recover\\CleanTask.py')
	time.sleep(3)

def testrun (buildversion):
#for i in range (1, 4):
	a_file = open('C:\\GFE\\Report\\ProvisionResult.txt')
	a_string = a_file.read()
	true = 'ProvisionSuccessfully'
	if true == a_string:
		testcaseexecution(buildversion)
	else:
		print "Provision failed, will try to provision again"
		recovery()

		

	
	
while 1:
  before = dict ([(f, None) for f in os.listdir (path_to_watch)])
  time.sleep (30)
  after = dict ([(f, None) for f in os.listdir (path_to_watch)])
  added = [f for f in after if not f in before]
  #removed = [f for f in before if not f in after]
  #if removed: print "Removed: ", ", ".join (removed)
  if added: 
	#will auto install GFE build on device
	print'-----There is a NEW BUILD, Install Latest GFE build #' + added [len(added)-1]+ "-----"
	newbuildversion = added [len(added)-1]
	testdatapreparation(newbuildversion)
	provisiongfe()
	testrun(newbuildversion)

  else:
    #will auto install current latest build
  	print'-----No new build, Install CURRENT latest GFE build #' + max(after)+ "-----"
	testdatapreparation(max(after))
	provisiongfe()
	testrun(max(after))

  before = after