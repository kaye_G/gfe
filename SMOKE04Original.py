#Please make sure your Outlook is launched and ready to use
import win32com.client
import time

f = open("Report\\timestamp.txt", 'r')
ts = f.readline()
f.close()

ISOTIMEFORMAT='%Y-%m-%d %X'
oOutlook = win32com.client.Dispatch("Outlook.Application")
myNamespace = oOutlook.GetNamespace("MAPI")
myNamespace.Logon("benw01", "svc") #Use account "benw01" as Organizer
print "Logon Profile '%s'" %(myNamespace.CurrentProfileName)
appt = oOutlook.CreateItem(1) # 1 - olAppointmentItem
appt.Start = time.strftime( ISOTIMEFORMAT, time.localtime() )
#appt.Start = '2015-03-29 12:00'
appt.Subject = 'S04' + ts
appt.Duration = 60
appt.Location = 'tj'
appt.ReminderMinutesBeforeStart = 15 #meeting reminder

myPattern = appt.GetRecurrencePattern()
myPattern.RecurrenceType = 0 #0-daily, 1-weekly, 2-monthly, 5-yearly
ISODATEFORMAT='%Y-%m-%d'
myPattern.PatternStartDate = time.strftime( ISODATEFORMAT, time.localtime() )
#myPattern.PatternEndDate = '6/29/2015' #Ends on 2015-06-29
myPattern.Occurrences = 4 #Ends after occur 10 times

appt.MeetingStatus = 1 # 1 - olMeeting; Changing the appointment to meeting
#only after changing the meeting status recipients can be added
appt.Recipients.Add("asia1new@asia.qagood.com")
appt.Save()
appt.Send()
time.sleep(30)
print ("Successfully sent meeting request S04%s") %(ts)
