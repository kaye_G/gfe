use GFESmoke;
set @sqlstr=concat('rename table SmokeResult to SmokeResult_',date_format(now(),'%Y%m%d%H%i%S'));
PREPARE stmt1 FROM @sqlstr;
EXECUTE stmt1;

