from email.mime.text import MIMEText 
from email.mime.multipart import MIMEMultipart
import smtplib
import MySQLdb
import time, datetime

# Get GFE version
f = open("Report\\version.txt", 'r')
ver = f.readline()
f.close()

# Open database connection
db = MySQLdb.connect('10.104.150.30','root','root','GFESmoke')

# Use cursor()method to get cursor
cursor = db.cursor()
# Use execute() method to execute sql
sql_cp = """select count(*) from SmokeResult where result='%s'""" %('Pass')
cursor.execute(sql_cp)
# Use fetchone() method to get one data
cp_t = cursor.fetchone()
cp = cp_t[0]

sql_cf = """select count(*) from SmokeResult where result='%s'""" %('Fail')
cursor.execute(sql_cf)
cf_t = cursor.fetchone()
cf = cf_t[0]

# Define Total case number
ct = 21
# Compute Fail case number
cn = ct - cp - cf

time = time.strftime('%b %d,%Y',time.localtime(time.time()))

sql_start = """select starts from SmokeResult where id='%d'""" %(7)
cursor.execute(sql_start)
result_start = cursor.fetchall();
for row in result_start:
	date_start = row[0]

sql_end = """select ends from SmokeResult where id='%d'""" %(24)
cursor.execute(sql_end)
result_end = cursor.fetchall();
for row in result_end:
	date_end = row[0]

per = date_end-date_start


t0 = """
<html>
<body bgcolor="FFFFCC">
<img src="C:\\GFE\\report\\good.jpg" width="118" height="34" />
<h1 align="center" style="background:red"><font color='FFFFCC'><b>GFE Android Smoke Automation Report</b></font></h1>
<p><b><u>Test Environments</u></b></p>
<ul>
	<li><b>Build Version: </b>Trunk #%s</li>
	<li><b>Device Model: </b>Samsung Note 4(OS 5.0.1)</li>
	<li><b>Handheld: </b>asia1new@asia.qagood.com</li>
	<li><b>GMC: </b>https://pl03.asia.qagood.com:8443/</li>
	<li><b>GMMS: </b>LK01-EWS-BDB(EWS\BDB)</li>
	<li><b>Duration: </b>%s</li>
</ul>
<p><b><u>Test Result Summary</u></b></p>
</table>
	<table width="300" border="1" bordercolor="black" cellspacing="2">
	<tr bgcolor="red">
		<b><font color='FFFFCC'><th>TOTAL</th><th>PASS</th><th>FAIL</th><th>NA</th></font></b>
	</tr>
	<tr>
		<b><td align="center"><font color='black'>%s</font></td><td align="center"><font color='green'>%s</font></td><td align="center"><font color='red'>%s</font></td><td align="center"><font color='grey'>%s</font></td></b>
	</tr>
</table>
""" %(ver, per, ct, cp, cf, cn)

### t1
sql_id1 = """select * from SmokeResult where id='%d'""" %(1)
cursor.execute(sql_id1)
result_1 = cursor.fetchall();
for row in result_1:
	id_1 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_1 = row[1]
	start_1 = row[2]
	end_1 = row[3]
t1 = """
</table>
<br>
<p><b><u>Test Details</u></b></p>
</table>
	<table width="700" border="1" bordercolor="black" cellspacing="2">
	<tr bgcolor="red">
		<b><font color='FFFFCC'><th><b>Test Case</b></th><th><b>Test Result</b></th><th><b>Start Time</b></th><th><b>End Time</b></th><th><b>Duration</b></th></font></b>
	</tr>
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3640504">AcceptSingleMR</a>
		</b></td><td><font color=%s>%s</font></td><td>%s</td><td>%s</td><td>%s</td>
	</tr>
""" %(fcolor, res_1, start_1, end_1, end_1-start_1)

### t2
sql_id2 = """select * from SmokeResult where id='%d'""" %(2)
cursor.execute(sql_id2)
result_2 = cursor.fetchall();
for row in result_2:
	id_2 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_2 = row[1]
	start_2 = row[2]
	end_2 = row[3]
t2 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3059863">DownloadAttachmentInOccurrence</a>
		</b></td><td><font color=%s>%s</font></td><td>%s</td><td>%s</td><td>%s</td>
	</tr>
""" %(fcolor,res_2, start_2, end_2, end_2-start_2)

### t3
sql_id3 = """select * from SmokeResult where id='%d'""" %(3)
cursor.execute(sql_id3)
result_3 = cursor.fetchall();
for row in result_3:
	id_3 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_3 = row[1]
	start_3 = row[2]
	end_3 = row[3]
t3 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3060090">CancelMeeting</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_3, start_3, end_3, end_3-start_3)

### t4
sql_id4 = """select * from SmokeResult where id='%d'""" %(4)
cursor.execute(sql_id4)
result_4 = cursor.fetchall();
for row in result_4:
	id_4 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_4 = row[1]
	start_4 = row[2]
	end_4 = row[3]
t4 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3060392">UpdateMeeting</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_4, start_4, end_4, end_4-start_4)

### t5
sql_id5 = """select * from SmokeResult where id='%d'""" %(5)
cursor.execute(sql_id5)
result_5 = cursor.fetchall();
for row in result_5:
	id_5 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_5 = row[1]
	start_5 = row[2]
	end_5 = row[3]
t5 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3060459">AcceptMeetingInCalendar</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_5, start_5, end_5, end_5-start_5)

### t6
sql_id6 = """select * from SmokeResult where id='%d'""" %(6)
cursor.execute(sql_id6)
result_6 = cursor.fetchall();
for row in result_6:
	id_6 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_6 = row[1]
	start_6 = row[2]
	end_6 = row[3]
t6 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3060878">SubscribeContactSubfolder</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_6, start_6, end_6, end_6-start_6)

### t7
sql_id7 = """select * from SmokeResult where id='%d'""" %(7)
cursor.execute(sql_id7)
result_7 = cursor.fetchall();
for row in result_7:
	id_7 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_7 = row[1]
	start_7 = row[2]
	end_7 = row[3]
t7 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3061285">OpenDocument</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_7, start_7, end_7, end_7-start_7)

###t8
sql_id8 = """select * from SmokeResult where id='%d'""" %(8)
cursor.execute(sql_id8)
result_8 = cursor.fetchall();
for row in result_8:
	id_8 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_8 = row[1]
	start_8 = row[2]
	end_8 = row[3]
t8 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3061367">ForwardEmail</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_8, start_8, end_8, end_8-start_8)

###t9
sql_id9 = """select * from SmokeResult where id='%d'""" %(9)
cursor.execute(sql_id9)
result_9 = cursor.fetchall();
for row in result_9:
	id_9 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_9 = row[1]
	start_9 = row[2]
	end_9 = row[3]
t9 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3061372">AddImageFromGallary</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_9, start_9, end_9, end_9-start_9)

### t10
sql_id10 = """select * from SmokeResult where id='%d'""" %(10)
cursor.execute(sql_id10)
result_10 = cursor.fetchall();
for row in result_10:
	id_10 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_10 = row[1]
	start_10 = row[2]
	end_10 = row[3]
t10 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3061457">SendThrough</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_10, start_10, end_10, end_10-start_10)

### t11
sql_id11 = """select * from SmokeResult where id='%d'""" %(11)
cursor.execute(sql_id11)
result_11 = cursor.fetchall();
for row in result_11:
	id_11 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_11 = row[1]
	start_11 = row[2]
	end_11 = row[3]
t11 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3061520">EMLViewer</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_11, start_11, end_11, end_11-start_11)

### t12
sql_id12 = """select * from SmokeResult where id='%d'""" %(12)
cursor.execute(sql_id12)
result_12 = cursor.fetchall();
for row in result_12:
	id_12 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_12 = row[1]
	start_12 = row[2]
	end_12 = row[3]
t12 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3061563">SendEmailToMultipleRecipients</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_12, start_12, end_12, end_12-start_12)

### t13
sql_id13 = """select * from SmokeResult where id='%d'""" %(13)
cursor.execute(sql_id13)
result_13 = cursor.fetchall();
for row in result_13:
	id_13 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_13 = row[1]
	start_13 = row[2]
	end_13 = row[3]
t13 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3061863">MoveEmail</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_13, start_13, end_13, end_13-start_13)

### t14
sql_id14 = """select * from SmokeResult where id='%d'""" %(14)
cursor.execute(sql_id14)
result_14 = cursor.fetchall();
for row in result_14:
	id_14 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_14 = row[1]
	start_14 = row[2]
	end_14 = row[3]
t14 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3061871">DeleteEmail</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_14, start_14, end_14, end_14-start_14)

### t15
sql_id15 = """select * from SmokeResult where id='%d'""" %(15)
cursor.execute(sql_id15)
result_15 = cursor.fetchall();
for row in result_15:
	id_15 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_15 = row[1]
	start_15 = row[2]
	end_15 = row[3]
t15 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3062062">ReplyIcon</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_15, start_15, end_15, end_15-start_15)

### t16
sql_id16 = """select * from SmokeResult where id='%d'""" %(16)
cursor.execute(sql_id16)
result_16 = cursor.fetchall();
for row in result_16:
	id_16 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_16 = row[1]
	start_16 = row[2]
	end_16 = row[3]
t16 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3062351">NewEmailAddFile</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_16, start_16, end_16, end_16-start_16)

### t17
sql_id17 = """select * from SmokeResult where id='%d'""" %(17)
cursor.execute(sql_id17)
result_17 = cursor.fetchall();
for row in result_17:
	id_17 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_17 = row[1]
	start_17 = row[2]
	end_17 = row[3]
t17 = """
	<tr>
		<td><b>
		ID17
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" #%(fcolor,res_17, start_17, end_17, end_17-start_17)

### t18
sql_id18 = """select * from SmokeResult where id='%d'""" %(18)
cursor.execute(sql_id18)
result_18 = cursor.fetchall();
for row in result_18:
	id_18 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_18 = row[1]
	start_18 = row[2]
	end_18 = row[3]
t18 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3062549">EasyActivation</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" #%(fcolor,res_18, start_18, end_18, end_18-start_18)

### t19
sql_id19 = """select * from SmokeResult where id='%d'""" %(19)
cursor.execute(sql_id19)
result_19 = cursor.fetchall();
for row in result_19:
	id_19 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_19 = row[1]
	start_19 = row[2]
	end_19 = row[3]
t19 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3062657">EmailAttachFileFromGoodShare</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" #%(fcolor,res_19, start_19, end_19, end_19-start_19)

### t20
sql_id20 = """select * from SmokeResult where id='%d'""" %(20)
cursor.execute(sql_id20)
result_20 = cursor.fetchall();
for row in result_20:
	id_20 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_20 = row[1]
	start_20 = row[2]
	end_20 = row[3]
t20 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3062761">EmailSaveFileToGoodShare</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_20, start_20, end_20, end_20-start_20)

### t21
sql_id21 = """select * from SmokeResult where id='%d'""" %(21)
cursor.execute(sql_id21)
result_21 = cursor.fetchall();
for row in result_21:
	id_21 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_21 = row[1]
	start_21 = row[2]
	end_21 = row[3]
t21 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3062775">SendFileandLinktoGFEwhenGFElock</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" #%(fcolor,res_21, start_21, end_21, end_21-start_21)

### t22
sql_id22 = """select * from SmokeResult where id='%d'""" %(22)
cursor.execute(sql_id22)
result_22 = cursor.fetchall();
for row in result_22:
	id_22 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_22 = row[1]
	start_22 = row[2]
	end_22 = row[3]
t22 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3063123">ProvisionWifiOFF</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
"""	#%(fcolor,res_22, start_22, end_22, end_22-start_22)
	
### t23
sql_id23 = """select * from SmokeResult where id='%d'""" %(23)
cursor.execute(sql_id23)
result_23 = cursor.fetchall();
for row in result_23:
	id_23 = row[0]
	if row[1] == 'Pass':
		res_23 = """<font color='green'>""" + row[1] + """</font>"""
	elif row[1] == 'Fail':
		res_23 = """<font color='red'>""" + row[1] + """</font>"""
	else:
		res_23 = row[1]
	start_23 = row[2]
	end_23 = row[3]
t23 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3063172">Resync</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_23, start_23, end_23, end_23-start_23)

### t24
sql_id24 = """select * from SmokeResult where id='%d'""" %(24)
cursor.execute(sql_id24)
result_24 = cursor.fetchall();
for row in result_24:
	id_24 = row[0]
	if row[1] == 'Pass':
		res_24 = """<font color='green'>""" + row[1] + """</font>"""
	elif row[1] == 'Fail':
		res_24 = """<font color='red'>""" + row[1] + """</font>"""
	else:
		res_24 = row[1]
	start_24 = row[2]
	end_24 = row[3]
t24 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3063218">GFEUpgrade</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_24, start_24, end_24, end_24-start_24)

### t25
sql_id25 = """select * from SmokeResult where id='%d'""" %(25)
cursor.execute(sql_id25)
result_25 = cursor.fetchall();
for row in result_25:
	id_25 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_25 = row[1]
	start_25 = row[2]
	end_25 = row[3]
t25 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3063368">EmailSubscribeFolder</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_25, start_25, end_25, end_25-start_25)

### t26
sql_id26 = """select * from SmokeResult where id='%d'""" %(26)
cursor.execute(sql_id26)
result_26 = cursor.fetchall();
for row in result_26:
	id_26 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_26 = row[1]
	start_26 = row[2]
	end_26 = row[3]
t26 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3063444">OutOfOffice</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" %(fcolor,res_26, start_26, end_26, end_26-start_26)

### t27
sql_id27 = """select * from SmokeResult where id='%d'""" %(27)
cursor.execute(sql_id27)
result_27 = cursor.fetchall();
for row in result_27:
	id_27 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_27 = row[1]
	start_27 = row[2]
	end_27 = row[3]
t27 = """
	<tr>
		<td><b>
		ID27
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" #%(fcolor,res_27, start_27, end_27, end_27-start_27)

### t28
sql_id28 = """select * from SmokeResult where id='%d'""" %(28)
cursor.execute(sql_id28)
result_28 = cursor.fetchall();
for row in result_28:
	id_28 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_28 = row[1]
	start_28 = row[2]
	end_28 = row[3]	
t28 = """
	<tr>
		<td><b>
		ID28
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" #%(fcolor,res_28, start_28, end_28, end_28-start_28)

### t29
sql_id29 = """select * from SmokeResult where id='%d'""" %(29)
cursor.execute(sql_id29)
result_29 = cursor.fetchall();
for row in result_29:
	id_29 = row[0]
	if row[1] == 'Pass':
		fcolor = 'green'
	elif row[1] == 'Fail':
		fcolor = 'red'
	else:
		fcolor = 'black'
	res_29 = row[1]
	start_29 = row[2]
	end_29 = row[3]
t29 = """
	<tr>
		<td><b>
		<a href="https://testrail.corp.good.com/testrail/index.php?/tests/view/3063674">CopyPaste</a>
		</b></td>
		<td><font color=%s>%s</font></td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>
""" #%(fcolor,res_29, start_29, end_29, end_29-start_29)

#html= t0 + t1 + t2 + t3 + t4 + t5 + t6 + t7 + t8 + t9 + t10 + t11 + t12 + t13 + t14 + t15 + t16 + t17 + t18 + t19 + t20 + t21 + t22 + t23 + t24 + t25 + t26 + t27 + t28 + t29
html= t0 + t1 + t2 + t3 + t4 + t5 + t6 + t7 + t8 + t9 + t10 + t11 + t12 + t13 + t14 + t15 + t16 + t20 + t23 + t24 + t25 + t26

att1 = MIMEText(open('C:\\GFE\\report\\log.txt', 'rb').read(), 'base64', 'utf-8')
att1["Content-Type"] = 'application/octet-stream'
att1["Content-Disposition"] = 'attachment; filename="log.txt"'
att2 = MIMEText(open('C:\\GFE\\report\\result.txt', 'rb').read(), 'base64', 'utf-8')
att2["Content-Type"] = 'application/octet-stream'
att2["Content-Disposition"] = 'attachment; filename="result.txt"'
att3 = MIMEText(open('C:\\GFE\\report\\report.txt', 'rb').read(), 'base64', 'utf-8')
att3["Content-Type"] = 'application/octet-stream'
att3["Content-Disposition"] = 'attachment; filename="report.txt"'
msg = MIMEMultipart('alternative')
msg.attach(att1)
msg.attach(att2)
msg.attach(att3)
mime = MIMEText(html,'html')
msg.attach(mime)

#fromaddr = 'katharine4@asia.qagood.com'
fromaddr = 'gfeandroid@163.com'
toaddrs = 'benwang@good.com, kaye@good.com'

msg['Subject'] = "GFE Android Smoke Automation Report (%s)" %time
msg['From'] = fromaddr
recipients = ['benwang@good.com','kaye@good.com']
msg['To'] = ", ".join(recipients)

db.close()


#Account Credentials
#username='katharine4@asia.qagood.com'
#password='svcsvc'


username = 'gfeandroid@163.com'
password = 'good111111'




#Send email
#try: 
#server = smtplib.SMTP('BUZEN.asia.qagood.com', 587, timeout=240)
server = smtplib.SMTP('smtp.163.com')
server.starttls()
server.login(username,password)
server.sendmail(fromaddr, recipients, msg.as_string())
server.quit()
print "Successfully sent out Android Smoke Report"
#except Exception:
	#print ("Error: unable to send email")
