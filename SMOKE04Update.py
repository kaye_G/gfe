#Please make sure your Outlook is launched and ready to use
import win32com.client
import time

f = open("Report\\timestamp.txt", 'r')
ts = f.readline()
f.close()

ISOTIMEFORMAT='%Y-%m-%d %X'
oOutlook = win32com.client.Dispatch("Outlook.Application")
myNamespace = oOutlook.GetNamespace("MAPI")
myNamespace.Logon("benw01", "svc") #Use account "benw01" as Organizer
print "Logon Profile '%s'" %(myNamespace.CurrentProfileName)

calendar = myNamespace.GetDefaultFolder("9").Items
count = len(calendar)

for i in range(count, 0, -1):
	objCalendar = calendar.Item(i) 
	sub = objCalendar.Subject
	if sub=="S04" + ts:
		myPattern = objCalendar.GetRecurrencePattern()
		myPattern.RecurrenceType = 1
		myPattern.DayOfWeekMask = 62
		objCalendar.Subject = "Reschedule:S04" + ts
		objCalendar.Save()
		objCalendar.Send()
time.sleep(30)
print ("Successfully rescheduled the meeting S04%s") %(ts)
print "Quit account 'benw01'"
myNamespace.Logoff()
oOutlook.Quit()

