package com.good.android.gfetest;

import com.android.uiautomator.core.*;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.good.android.gfetest.Frequency.Activity;

public class Preparation extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
	private final String subFolder = "SUB1";
	public void testPrepare() {
		Tools.doubleLog("==========================================================================================");
		Tools.doubleLog("Start to do preparation for Smoke");
		UiDevice device = getUiDevice();
		freq.launchGFE(device);
/**
 * Subscribe email subfolder "SUB1" to do prepare for tc13, tc15;		
 */
		try {
			UiObject folderSyncObj = freq.findItemInPreferences("Folder Sync");
			folderSyncObj.waitForExists(freq.tea);
			folderSyncObj.clickAndWaitForNewWindow();
			freq.subscribeEmailFolder(subFolder);
			device.pressBack();
		} catch(UiObjectNotFoundException e) {
			Tools.logInfor(e);
		}
		freq.gotoActivity(Activity.Email);
	}
}
