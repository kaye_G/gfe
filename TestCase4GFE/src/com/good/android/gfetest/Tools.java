/*
 * Tools class, all the methods in this class are static;
 * Author: Katharine Ye
 */

package com.good.android.gfetest;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.android.uiautomator.core.*;

public class Tools {
	private static final String fileName_log = "log.txt";
	private static final String fileName_logcat = "logcat.txt";
	private static final String fileName_report = "report.txt";
	private static final String fileName_result = "result.txt";
	private static final String fileName_ProvisionResult = "ProvisionResult.txt";

	private static final String filePath = "/data/local/tmp";
	private static final String imageName = "screenshot";
	private static final String imagePath = "/data/local/tmp";
	private static final String keyevent_Menu = "input keyevent 82";
	private static final String keyevent_Enter = "input keyevent 66";
	private static final String keyevent_Back = "input keyevent 4";

	
	public static void $Starts(String level, String methodName) {
		Tools.logInfor(level + "Frequency -> Method " + methodName + "() start");
	}
	
	public static void $Ends(String level, String methodName) {
		Tools.logInfor(level + "Frequency -> Method " + methodName + "() finish");
	}
	
	
/* Start with "A" */
/* Start with "B" */
/* Start with "C" */
	
	
/* Start with "D" */
	/**
	 * A way to write a line of output data in files "log.txt" and "result.txt".
	 * @param logString String for output data.
	 */
	public static void doubleLog(String logString) {
		Tools.logInfor(logString);
		Tools.logResult(logString);
	}
	
	
/* Start with "E" */
/* Start with "F" */
	
	
/* Start with "G" */
	/**
	 * Returns the absolute pathname string of this abstract pathname.
	 * @param f The abstract pathname for the file.
	 * @return The absolute pathname string denoting the same file or directory as this abstract pathname.
	 */
	public static String getFile(File f) {
		return f.getAbsolutePath();
	}
	
	/**
	 * Returns the format String for test result.
	 * @param b A boolean.
	 * @return If the argument is true, a String "@Pass /v" is returned; otherwise, a String "@Fail /x" is returned.
	 */
	public static String getResult(boolean b) {
		return (b != false) ? "@Pass /v" : "@Fail /x";
	}
	
	/**
	 * Returns a UI element by using the given className, resourceId and Text.
	 * @param className String for className to be used to get the UI element.
	 * @param resourceId String for resourceId to be used to get the UI element.
	 * @param text String for text to be used to get the UI element.
	 * @return A UI element by using the given className, resourceId and Text.
	 */
	public static UiObject getUiObject(String className, String resourceId, String text) {
		UiObject obj = null;
		if(className.equals("")) {
			obj = new UiObject(new UiSelector().resourceId(resourceId).text(text));
		} else if(resourceId.equals("")) {
			obj = new UiObject(new UiSelector().className(className).text(text));
		} else if(text.equals("")) {
			obj = new UiObject(new UiSelector().resourceId(resourceId).className(className));
		} else {
			obj = new UiObject(new UiSelector().resourceId(resourceId).className(className).text(text));
		}
		return obj;
	}
	
	/**
	 * Returns a UI element by using the given className and index.
	 * @param className String for className to be used to get the UI element.
	 * @param index an int to be used to get the UI element.
	 * @return A UI element by using the given className and index.
	 */
	public static UiObject getUiObject(String className, int index) {
		UiObject obj = new UiObject(new UiSelector().className(className).index(index));
		return obj;
	}
	
	/**
	 * Returns a UI element by using the given className, resourceId and index.
	 * @param className String for className to be used to get the UI element.
	 * @param resourceId String for resourceId to be used to get the UI element.
	 * @param index an int to be used to get the UI element.
	 * @return A UI element by using the given className, resourceId and index.
	 */
	public static UiObject getUiObject(String className, String resourceId, int index) {
		UiObject obj = null;
		if(index >= 0) {
			obj = new UiObject(new UiSelector().className(className).resourceId(resourceId).index(index));
		} else if(index == -1) {
			obj = new UiObject(new UiSelector().className(className).resourceId(resourceId));
		}
		return obj;
	}
	
	
/* Start with "H" */
	
	
/* Start with "I" */
	/**
	 * Returns a boolean representation of test case result according to the results of all steps.
	 * @param stepResult the boolean array.
	 * @return A boolean representation of test case result.
	 */
	public static boolean isTrue(boolean[] stepResult) {
		boolean finalResult = true;
		for(int i=0; i<stepResult.length; i++) {
			finalResult = finalResult & stepResult[i];
		}
		return finalResult;
	}
	
	
/* Start with "J" */
/* Start with "K" */
	
	
/* Start with "L" */
	/**
	 * A way to write a line of output data in file "log.txt".
	 * @param logString String for output data.
	 */
	public static void logInfor(String logString) {
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			FileWriter fw = new FileWriter(setFile(fileName_log), true);
			fw.write(date.format(new Date()));
			System.out.print(date.format(new Date()));
			fw.write(" ");
			System.out.print(" ");
			fw.write(logString);
			System.out.print(logString);
			fw.write("\r\n");
			System.out.println();
			fw.flush();
			fw.close();
		} catch(IOException e) {
			logInfor(e);
		}
	}

	/**
	 * A way to log exception in file "log.txt".
	 * @param e an exception.
	 */
	public static void logInfor(Exception e) {
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			printLogcat();
			FileWriter fw = new FileWriter(setFile(fileName_log), true);
			PrintWriter pw = new PrintWriter(fw);
			fw.write(date.format(new Date()));
			System.out.println(date.format(new Date()));
			fw.write("\r\n");
			e.printStackTrace(pw);
			fw.write("\r\n");
			e.printStackTrace();
			fw.flush();
			fw.close();
		} catch(IOException e1) {
			e1.printStackTrace();
			System.exit(-1);
		}
	}
	
	/**
	 * A way to write a line of output data in file "result.txt".
	 * @param logString String for output data.
	 */
	public static void logResult(String logString) {
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			FileWriter fw = new FileWriter(setFile(fileName_report), true);
			fw.write(date.format(new Date()));
			fw.write(" ");
			fw.write(logString);
			fw.write("\r\n");
			fw.flush();
			fw.close();
		} catch(IOException e) {
			logInfor(e);
		}
	}
	
    /**
     * A way to write a line of output data in file "ProvisionResult.txt".
     * @param logString String for output data.
     */
    public static void logProvisionResult(String logString) {
        try {
            FileWriter fw = new FileWriter(setFile(fileName_ProvisionResult), true);
            fw.write(logString);
            fw.flush();
            fw.close();
        } catch(IOException e) {
            logInfor(e);
        }
    }
	
/* Start with "M" */
/* Start with "N" */
/* Start with "O" */
	
	
/* Start with "P" */
	/**
	 * Simulates a short press on the BACK button.
	 * @throws IOException If an I/O error occurs.
	 */
	public static void pressBack() throws IOException {
		Runtime.getRuntime().exec(keyevent_Back);
	}
	
	/**
	 * Simulates a short press on the ENTER key.
	 * @throws IOException If an I/O error occurs.
	 */
	public static void pressEnter() throws IOException {
		Runtime.getRuntime().exec(keyevent_Enter);
	}
	
	/**
	 * Simulates a short press on the MENU button.
	 * @throws IOException If an I/O error occurs.
	 */
	public static void pressMenu() throws IOException {
		Runtime.getRuntime().exec(keyevent_Menu);
	}
	
	/**
	 * A way to write 50 lines of logcat log in file "logcat.txt".
	 */
	public static void printLogcat() {
		Process process = null;
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			process = Runtime.getRuntime().exec("logcat -t 50");
			BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
			FileWriter fw = new FileWriter(setFile(fileName_logcat), true);
			fw.write(date.format(new Date()));
			fw.write(" ");
			fw.write("shell@Logcat:/$");
			fw.write("\r\n");
			String sl = null;
			while((sl = in.readLine()) != null) {
				fw.write(sl);
			}
			fw.write("\r\n");
			fw.write("\r\n");
			fw.flush();
			fw.close();
			in.close();
		} catch(IOException e) {
			logInfor(e);
		}
	}
	
	
/* Start with "Q" */
/* Start with "R" */
	public static String readData() throws IOException {
		File file = new File(filePath, "timestamp.txt");
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String ts = br.readLine();
//System.out.println(ts);
		br.close();
		fr.close();
		return ts;
	}	
	
/* Start with "S" */
	/**
	 * Create an empty file in default directory by using the given file name.
	 * @param fileName String for file's name.
	 * @return An abstract pathname denoting a newly-created empty file.
	 */
	public static File setFile(String fileName) {
		File logFile = new File(filePath, fileName);
		return logFile;
	}
	
	/**
	 * Create an image which type is ".png" in default directory by using the given file name.
	 * @param time the current system time, measured in milliseconds since the epoch (00:00:00 GMT, January 1, 1970).
	 * @return An abstract pathname denoting an image which type is ".png".
	 */
	public static File setFile(long time) {
		SimpleDateFormat date = new SimpleDateFormat("yyyyMMddHHmmss");
		String dateTime = date.format(time);
		String iName = imageName + dateTime + ".png";
		File file = new File(imagePath, iName);
		return file;
	}
	
	/**
	 * Create an empty file in specified directory by using the given file path and file name.
	 * @param fPath String for file's path.
	 * @param fName String for file's name.
	 * @return An abstract pathname denoting a newly-created empty file.
	 */
	public static File setFile(String fPath, String fName) {
		File file = new File(fPath, fName);
		return file;
	}
	
	
/* Start with "T" */
	/**
	 * Returns true if and only if duration doesn't exceed the specified interval.
	 * @param starts a long value of start time, cannot be empty.
	 * @param interval a long value of interval, cannot be empty.
	 * @param text String to display as prefix.
	 * @return true if duration doesn't exceed the specified interval, false otherwise. 
	 */
	public static boolean timer(long starts, long interval, String text) {
		long ends = new Date().getTime();
		if(ends - starts >= interval) {
			Tools.logInfor(text + "Timer/Timeout!");
			return false;
		} else {
			Tools.logInfor(text + "Timer/Waiting...");
			return true;
		}
	}
	
	
/* Start with "U" */
/* Start with "V" */
	
	
/* Start with "W" */
	/**
	 * A way to write formatted String in file "report.txt".
	 * @param logString String for formatted result.
	 */
	public static void writeData(String logString) {
		try {
			FileWriter fw = new FileWriter(setFile(fileName_result), true);
			fw.write(logString);
			fw.write("\r\n");
			fw.flush();
			fw.close();
		} catch(IOException e) {
			logInfor(e);
		}
	}
	
	
/* Start with "X" */
/* Start with "Y" */
/* Start with "Z" */


	


	

	




}
