/**
 * Offer GFE related test methods;
 * Author: Katharine Ye
 */

package com.good.android.gfetest;

import java.io.IOException;
import java.util.*;

import android.os.*;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;

public class Frequency extends UiAutomatorTestCase {
    public enum Activity {
        Email, Calendar, Contacts, Tasks, ToDo, Applications, Documents, Browser, Preferences
    };
    
    public enum EmailItem {
        To, Cc, Bcc, Subject, Body
    };

    public enum CalendarItem {
        Subject, Location, Required, Optional, Resource
    };

    public enum Respond {
        Accept, Tentative, Decline
    };

    public enum RespondMsg {
        DefaultMsg, Comment, None
    };

    public enum CalendarMode {
        Day, Week, Month
    };

    public int errorCode = 0;
    private static final String pre = "    ";
    private static final String activityClName = "android.widget.RelativeLayout";
    public final String password = "a";
    public final int blink = 100;
    public final int breath = 500;
    public final int hearbeat = 1000;
    public final int tea = 5000;
    public final int read = 60000;
    private final int msgSubNum_def = 7;
    public String keyevent_Menu = "input keyevent 82";
    public String keyevent_Enter = "input keyevent 66";
    public String keyevent_Back = "input keyevent 4";
    Nodes_Calendar nc = new Nodes_Calendar();
    Nodes_Preferences np = new Nodes_Preferences();
    Nodes_Contacts nco = new Nodes_Contacts();
    Nodes_Email ne = new Nodes_Email();

/* Start with "A" */
    /**
     * Select to edit an Occurrence or the whole Series after clicking on Edit button in Event Detail view, call this method in Event Detail view (As Organizer).  
     * @param type String "occurrence" or "series", cannot be empty.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void accessEditRecurringEventScreen(String type) throws UiObjectNotFoundException {
    	Tools.$Starts(pre, "accessEditRecurringEventScreen");
    	UiObject editBtn = null;
    	UiObject eventTypeObj = null;
    	UiObject dialogTitleObj = null;
    	editBtn = nc.getEditBtnInCalendarDetailView();
    	if(editBtn.exists() && editBtn.isEnabled()) {
    		editBtn.clickAndWaitForNewWindow();
    		dialogTitleObj = nc.getEditRecurringEventTitle();
    		if(dialogTitleObj.exists() && dialogTitleObj.isEnabled()) {
    			eventTypeObj = nc.getEditRecurringEventOption(type);
    			eventTypeObj.clickAndWaitForNewWindow();
    			this.tapButton("OK");
    		}
    	}
    	Tools.$Ends(pre, "accessEditRecurringEventScreen");
    }
    
    /**
     * Returns true if and only if add New Contact in Root Contact folder successfully, call this method in Contact Activity.
     * @param personName String of person name, can not be empty.
     * @param phoneNumber String of phone number, 5~13 digit, can not be empty.
     * @param emailAddress String of email address, format like "abc@def.com", cannot be empty.
     * @return true if add new Contact in Root Contact folder successfully, false otherwise.
     */
    public boolean addContact(String personName, String phoneNumber, String emailAddress) {
        UiObject rtContactFolder = null;
        UiObject addContactBtn = null;
        UiObject fName = null;
        UiObject mName = null;
        UiObject lName = null;
        UiObject numberField = null;
        UiObject emailField = null;
        UiObject saveBtn = null;
        Tools.$Starts(pre, "addContact");
        try {
            rtContactFolder = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/title").text("Contacts"));
            rtContactFolder.waitForExists(5 * hearbeat);
            addContactBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/new_contact"));
            addContactBtn.waitForExists(5 * hearbeat);
            if (rtContactFolder.exists() && addContactBtn.exists()) {
                addContactBtn.clickAndWaitForNewWindow(); //Switch to New Contact screen;
            }
            sleep(blink);
            Runtime.getRuntime().exec("input keyevent 4"); //Remove the keyboard;
            fName = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/first_name").text("First name"));
            fName.waitForExists(5 * hearbeat);
            mName = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/middle_name").text("Middle name"));
            mName.waitForExists(5 * hearbeat);
            lName = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/last_name").text("Last name"));
            lName.waitForExists(5 * hearbeat);
            String[] name = personName.split(" ");
            if (name.length < 2) {
                fName.setText(personName); //Input first name;
            } else if (name.length == 2) {
                fName.setText(name[0]);
                lName.setText(name[1]); //Input first name and last name;
            } else if (name.length == 3) {
                fName.setText(name[0]);
                mName.setText(name[1]);
                lName.setText(name[2]); //Input first name, middle name and last name;
            }
            sleep(this.blink);
            numberField = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/data").text("Phone number"));
            numberField.waitForExists(5 * hearbeat);
            if (numberField.exists()) {
                numberField.setText(phoneNumber); //Input phone number;
            }
            sleep(this.blink);
            emailField = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/data").text("Email address"));
            emailField.waitForExists(5 * hearbeat);
            if (emailField.exists()) {
                emailField.setText(emailAddress); //Input email address;
            }
            sleep(this.blink);
            saveBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/saveButton").text("Save"));
            saveBtn.waitForExists(5 * hearbeat);
            if (saveBtn.exists() && saveBtn.isEnabled()) {
                saveBtn.clickAndWaitForNewWindow(); //Save the new contact;
            }
            sleep(breath);
            Tools.$Ends(pre, "addContact");
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
            return false;
        } catch (IOException e) {
            Tools.logInfor(e);
            return false;
        }
        return true;
    }

    /**
     * Returns true if and only if add photo to File Repository successfully, call this method in Docs Activity.
     * @return true if add photo to File Repository successfully, otherwise false.
     */
    public boolean addPhotoToDocs() {
        UiObject docsIcon = null;
        UiObject addBtn = null;
        UiObject takePhoto = null;
        UiObject nameField = null;
        UiObject saveBtn = null;
        UiObject photoItem = null;
        Tools.$Starts(pre, "addPhotoToDocs");
        try {
            docsIcon = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/title").text("Documents"));
            docsIcon.waitForExists(5 * hearbeat);
            if (!docsIcon.exists()) {
                Tools.logInfor("not exists");
                return false;
            }
            addBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                    "com.good.android.gfe:id/add_photo"));
            addBtn.waitForExists(5 * hearbeat);
            if (addBtn.exists() && addBtn.isEnabled()) {
                addBtn.click(); // Trigger "Add Photo" context menu;
            }
            sleep(this.blink);
            takePhoto = new UiObject(new UiSelector().resourceId("android:id/text1").text("Take photo"));
            takePhoto.waitForExists(5 * hearbeat);
            if (takePhoto.exists() && takePhoto.isEnabled()) {
                takePhoto.clickAndWaitForNewWindow();
            }
            sleep(this.blink); // Select "Take photo" item;
            this.takePhoto(); // Call method takePhoto();
            sleep(this.blink);
            nameField = new UiObject(new UiSelector().className("android.widget.EditText").resourceId(
                    "com.good.android.gfe:id/rename_activity_filename_edittext"));
            nameField.waitForExists(5 * hearbeat);
            String fileName = nameField.getText();
            saveBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/save_button").text("Save"));
            saveBtn.waitForExists(5 * hearbeat);
            if (saveBtn.exists() && saveBtn.isEnabled()) {
                saveBtn.clickAndWaitForNewWindow(); // Save the photo to
                                                    // Documents;
            }
            sleep(this.blink);
            photoItem = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/display_name").text(
                    fileName.trim()));
            photoItem.waitForExists(5 * hearbeat);
            if (photoItem.exists()) {
                return true;
            }
            Tools.$Ends(pre, "addPhotoToDocs");
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
            Tools.logInfor("exception");
            return false;
        }
        return true;
    }

    /**
     * Returns true if and only if successfully add image from Camera or Gallary to Attachments screen, call this method in Email/Calendar Attachments Activity.
     * @param flag String "Picture" or "Capture Picture".
     * @return true if successfully add image from Camera or Gallary, otherwise false.
     */
    public boolean attachFile(String flag) {
        Tools.$Starts(pre, "attachFile");
        UiObject clipBtn = null;
        UiObject attachBtn = null;
        UiObject imageN4 = null;
        UiObject pList = null;
        UiObject imageS5 = null;
        UiObject capBtn = null;
        UiObject attBackBtn = null;
        UiObject selectImage = null;
        try {
            clipBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/subject_attachments_button").text("0"));
            clipBtn.waitForExists(5 * hearbeat);
            if (new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/title").text("Compose")).exists()) {
                if (clipBtn.exists() && clipBtn.isEnabled()) {
                    clipBtn.clickAndWaitForNewWindow();
                }
            }
            if (new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/title").text("Attachments")).exists()) {
                attachBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/add_attachments"));
                attachBtn.waitForExists(5 * hearbeat);
                if (attachBtn.exists() && attachBtn.isEnabled()) {
                    attachBtn.click();
                    Tools.logInfor(pre + "Go to Attachment Manager screen");
                    if (new UiObject(new UiSelector().resourceId("android:id/alertTitle").text("Attach")).exists()) {
                        new UiObject(new UiSelector().resourceId("android:id/text1").text(flag)).clickAndWaitForNewWindow();
                        sleep(this.hearbeat);
                        //Select attach file from Gallary
                        if (flag.equals("Picture")) {
                            //Navigate to 3rd party app "Gallary"/"Photo";
                            imageN4 = new UiObject(new UiSelector().className("com.sec.samsung.gallery.glview.composeView.PositionControllerBase$ThumbObject").index(1));
                            pList = new UiObject(new UiSelector().className("android.widget.LinearLayout").resourceId("com.google.android.apps.plus:id/tile_row"));
                            imageS5 = pList.getChild(new UiSelector().className("android.view.View").index(0));
                            if (imageN4.exists()) {
                                //Select image from Galary - Note 4;
                                imageN4.clickAndWaitForNewWindow();
                                Tools.logInfor("Select a photo from Gallery to GFE");
                            } else if (imageS5.exists()) {
                                //Select image from Photos - S5;
                                imageS5.clickAndWaitForNewWindow();
                                Tools.logInfor("Select a photo from Photos to GFE");
                            } else {
                                Tools.logInfor(pre + ">>>>>>@Error: Image is not found!");
                            }
                        } else if (flag.equals("Capture Picture")) {
                            //Select attach file from Camera
                            capBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/tap_prompt").text("Tap the screen to take a photo"));
                            capBtn.waitForExists(5 * hearbeat);
                            if (capBtn.exists() && capBtn.isEnabled()) {
                                capBtn.click();
                                sleep(5000);
                            }
                            //Back to Attachments Manager screen
                            new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/accept_button").text("Accept")).clickAndWaitForNewWindow();
                        }
                        attBackBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/action_bar_home"));
                        attBackBtn.waitForExists(5 * hearbeat);
                        selectImage = new UiObject(new UiSelector().className("android.widget.CheckBox").resourceId("com.good.android.gfe:id/multi_attachment_checkbox"));
                        selectImage.waitForExists(5 * hearbeat);
                        //Back to Compose window from AM screen
                        if (attBackBtn.exists() && selectImage.exists()) {
                            if (selectImage.isClickable()) {
                                Tools.logInfor(pre + "Image is added");
                                attBackBtn.clickAndWaitForNewWindow();
                            } else {
                                Tools.logInfor(pre + ">>>>>>@Error: The image is blocked!");
                            }
                        }
                    }
                } else {
                    Tools.logInfor(pre + ">>>>>>@Error: Clip+ icon is not found!");
                    return false;
                }
            }
            Tools.$Ends(pre, "attachFile");
            return true;
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
            return false;
        }
    }


/* Start with "B" */
    /**
     * Go back to email List view, call this method from email Detail view in Email Activity. 
     */
    public void backToEmailList() {
        Tools.$Starts(pre, "backToEmailList");
        try {
            UiObject backBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                    "com.good.android.gfe:id/back_to_list_button"));
            backBtn.waitForExists(5 * hearbeat);
            if (backBtn.exists() && backBtn.isEnabled()) {
                backBtn.clickAndWaitForNewWindow();
            }
            Tools.logInfor(pre + "Frequency -> Method backToEmailList() finish");
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "backToEmailList");
    }
    
    /**
     * Go back to event Detail view, call this method in Calendar Attachments Activity.
     */
    public void backToEventView() {
        Tools.$Starts(pre, "backToEventView");
        try {
            UiObject backBtn = nc.getBackToEventBtn();
            backBtn.waitForExists(5 * hearbeat);
            if (backBtn.exists() && backBtn.isEnabled()) {
                backBtn.clickAndWaitForNewWindow();
            }
            Tools.logInfor(pre + "Frequency -> Method backToEventView() finish");
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "backToEventView");
    }

    /**
     * Click Today button and go back to today, call this method in Calendar Activity.
     */
    public void backToToday() {
        Tools.$Starts(pre, "backToToday");
        try { //Stay at Calendar Day/Week/Month view to call the method;
            UiObject todayBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/today"));
            todayBtn.waitForExists(5 * hearbeat);
            if (todayBtn.exists() && todayBtn.isEnabled()) {
                todayBtn.clickAndWaitForNewWindow(); //Click Today button to back to today;
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "backToToday");
    }

    
/* Start with "C" */
    /**
     * Change SMIME type to "Signed", "Encrypted" or "Signed & Encrypted", call this method in Email Compose Window.
     * @param mode String "Signed", "Encrypted" or "Signed & Encrypted", cannot be empty.
     */
    public void changeMsgSecurityMode(String mode) {
    	UiObject selectObj = null;
    	UiObject modeObj = null;
        try {
            selectObj = new UiObject(new UiSelector().className("android.view.View").resourceId("com.good.android.gfe:id/smime_email_type_button"));
            selectObj.waitForExists(this.tea);
            selectObj.clickAndWaitForNewWindow();
            modeObj = new UiObject(new UiSelector().resourceId("android:id/text1").text(mode));
            modeObj.waitForExists(this.tea);
            modeObj.clickAndWaitForNewWindow();
        } catch (UiObjectNotFoundException e) {
            e.printStackTrace();
        }
    }

    
    /**
     * Clear APP data through settings -> application manager.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    
    public void clearAppDataSamsung(String nameOfAppToClearData)
            throws UiObjectNotFoundException {
        // Tap Apps then clear data for GFE

        UiScrollable appViews1 = new UiScrollable(new UiSelector());
        appViews1.scrollTextIntoView("Applications");
        UiObject tapapps1 = new UiObject(new UiSelector().textContains("App"));
        tapapps1.clickAndWaitForNewWindow();

        UiObject tapapps2 = new UiObject(
                new UiSelector().textContains("manager"));
        tapapps2.clickAndWaitForNewWindow();

        UiScrollable appViewsGood = new UiScrollable(new UiSelector());
        appViewsGood.scrollTextIntoView(nameOfAppToClearData);

        UiObject tapGFE = new UiObject(new UiSelector().resourceId(
                "com.android.settings:id/app_name").text(nameOfAppToClearData));

        tapGFE.click();
        UiObject GFEVersion = new UiObject(
                new UiSelector().resourceId("com.android.settings:id/app_size"));
        GFEVersion.getText();
        Tools.doubleLog(nameOfAppToClearData + " " + GFEVersion.getText()
                + "\n\n");
        sleep(3000);
        UiObject cleardata = new UiObject(new UiSelector().resourceId(
                "com.android.settings:id/right_button").text("Clear data"));
        cleardata.click();

        UiObject cleardataok = new UiObject(new UiSelector().resourceId(
                "android:id/button1").text("OK"));
        if (cleardataok.exists()) {
            cleardataok.clickAndWaitForNewWindow();
            getUiDevice().pressHome();

        } else {
            getUiDevice().pressHome();
        }

    }
    
    /**
     * Clear Android System clipboard through a APP named "Clipper"
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void clearclipboard() throws UiObjectNotFoundException {
        // // clean system clipboard

        UiObject trashicon = new UiObject(
                new UiSelector()
                        .resourceId("org.rojekti.clipper:id/menu_clear"));
        trashicon.click();
        UiObject trashiconconfirm = new UiObject(
                new UiSelector().text("Delete all"));
        trashiconconfirm.click();

    }
    
    /**
     * Click ^ button in email drawer list, call this method in Email Activity.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void clickChildFolderButton() throws UiObjectNotFoundException {
    	Tools.$Starts(pre, "clickChildFolderButton");
        UiObject cFolderBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/child_folders"));
        cFolderBtn.waitForExists(this.tea);
        cFolderBtn.clickAndWaitForNewWindow();
        Tools.logInfor(pre + "Tap ^ button to list all email subfolders");
        Tools.$Starts(pre, "clickChildFolderButton");
    }
    
    /**
     * Returns true if and only if successfully download and open attachments in Calendar Attachments view, call this method in Event Detail view.
     * @param device A singleton instance of UiDevice.
     * @param num The number of attachments.
     * @return true if successfully download and open attachments in Calendar Attachments view.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public boolean clickClipIconDownloadAttachmentsInEventDetailView(UiDevice device, int num) throws UiObjectNotFoundException {
    	Tools.$Starts(pre, "clickPaperClipIconInEventDetailView");
    	UiObject clip = nc.getPaperClipIconInEventView();
    	clip.waitForExists(tea);
    	int getNum = Integer.parseInt(clip.getText());
    	if(getNum == num) {
    		clip.clickAndWaitForNewWindow();
    		this.downloadAttachments(device, num);
    		this.backToEventView();
    		Tools.$Ends(pre, "clickPaperClipIconInEventDetailView");
    		return true;
    	} else {
    		Tools.$Ends(pre, "clickPaperClipIconInEventDetailView");
    		return false;
    	}
     }

    /**
     * Returns the number of all the email subfolders, call this method in Email List view.
     * @return the number of all the email subfolders.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public int countEmailSubfolder() throws UiObjectNotFoundException {
        Tools.$Starts(pre, "countEmailSubfolder");
        UiObject drawer = null;
        this.openDrawer();
        sleep(this.blink);
        if (new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/child_folders")).exists()) {
            this.unfoldingEmailSub();
        }
        sleep(this.blink);
        drawer = new UiObject(new UiSelector().className("android.widget.ListView").resourceId("com.good.android.gfe:id/email_folders_list"));
        drawer.waitForExists(5 * hearbeat);
        int count = drawer.getChildCount();
        Tools.$Ends(pre, "countEmailSubfolder");
        return (count - this.msgSubNum_def);
    }

    /**
     * Returns true if and only if create a new event successfully, call this method in Calendar Activity.
     * @param subject the enum of CalendarItem.
     * @param subText String for event subject.
     * @param location the enum of CalendarItem.
     * @param locText String for event location.
     * @param invField the enum of CalendarItem.
     * @param invitee String for meeting invitee, format like "abc@def.com".
     * @return true if create a new event successfully, false otherwise.
     */
    public boolean createEvent(CalendarItem subject, String subText, CalendarItem location, String locText, CalendarItem invField, String invitee) {
        Tools.$Starts(pre, "createEvent");
        UiObject newEvent = null;
        UiObject saveBtn = null;
        boolean flag = false;
        try {
            UiObject mode = new UiObject(new UiSelector().packageName("com.good.android.gfe").className("android.widget.ListView"));
            mode.waitForExists(5 * hearbeat);
            if (mode.exists()) {
                Runtime.getRuntime().exec("input keyevent 4");
            }
            new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/new_event")).clickAndWaitForNewWindow(); //Tap to create a new event;
            newEvent = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/title").text("New Event"));
            newEvent.waitForExists(5 * hearbeat);
            if (!newEvent.exists()) {
                return false;
            }
            Tools.logInfor(pre + "Enter event subject");
            enterEventText(subject, subText); //Enter event title;
            Tools.logInfor(pre + "Enter event location");
            enterEventText(location, locText); //Enter event location;
            Tools.logInfor(pre + "Enter event invitee");
            enterEventInvitee(invField, invitee); //Enter event invitee;
            String starts = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/start_date")).getText().trim();
            String ends = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/end_date")).getText().trim();
            saveBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/save").text("Save"));
            saveBtn.waitForExists(5 * hearbeat);
            if (saveBtn.exists() && saveBtn.isEnabled()) {
                saveBtn.clickAndWaitForNewWindow(); //Save the event;
                Tools.logInfor("Create event [Subject=" + subText + "] at [Location=" + locText + "] from [" + starts + "] to [" + ends + "]");
                flag = true;
            }
            Tools.$Ends(pre, "createEvent");
            return flag;
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
            return false;
        } catch (IOException e) {
            Tools.logInfor(e);
            return false;
        }
    }

    /**
     * Returns true if and only if create a new task successfully, call this method in Contacts Activity.
     * @param taskTitle String for task subject.
     * @return true if create a new task successfully, false otherwise.
     */
    public boolean createTask(String taskTitle) {
        Tools.$Starts(pre, "createTask");
        try {
            UiObject mode = new UiObject(new UiSelector().packageName("com.good.android.gfe").className("android.widget.ListView"));
            UiObject allTask = null;
            UiObject newTaskBtn = null;
            UiObject titleField = null;
            UiObject saveBtn = null;
            if (mode.exists()) {
                //Runtime.getRuntime().exec("input keyevent 4");
                allTask = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/action_text").text("All Tasks"));
                sleep(this.blink);
                if (allTask.exists()) {
                    allTask.clickAndWaitForNewWindow();
                    Tools.logInfor("Remove action text menu");
                }
            }
            newTaskBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/new_task"));
            newTaskBtn.waitForExists(10 * hearbeat);
            if (newTaskBtn.exists() && newTaskBtn.isEnabled()) {
                newTaskBtn.clickAndWaitForNewWindow(); //Switch to new task screen;
            }
            sleep(this.blink);
            Runtime.getRuntime().exec("input keyevent 4"); //Remove keyboard;
            titleField = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/title").text("Title"));
            titleField.waitForExists(5 * hearbeat);
            if (titleField.exists()) {
                titleField.setText(taskTitle); //Input task title;
            }
            sleep(this.blink);
            saveBtn = new UiObject(new UiSelector().className("android.widget.TextView").text("SAVE"));
            saveBtn.waitForExists(5 * hearbeat);
            if (saveBtn.exists() && saveBtn.isEnabled()) {
                saveBtn.clickAndWaitForNewWindow(); //Save new task;
            }
            sleep(this.blink);
            Tools.$Ends(pre, "createTask");
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
            return false;
        } catch (IOException e) {
            Tools.logInfor(e);
            return false;
        }
        return true;
    }

    
/* Start with "D" */
    /**
     * Taps to un-check the specified message in Email list view, call this method in Email Activity.
     * @param emailListItem A UI element for specified message.
     */
    public void deselectMsg(UiObject emailListItem) {
        Tools.$Starts(pre, "deselectMsg");
        try {
            UiObject emailCheckbox = emailListItem.getChild(new UiSelector().className("android.widget.CheckBox").resourceId("com.good.android.gfe:id/multi_check"));
            emailCheckbox.waitForExists(5 * hearbeat);
            String emailSubject = emailListItem.getChild(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/subject")).getText();
            if (emailCheckbox.isChecked()) {
                emailCheckbox.click();
                Tools.logInfor(pre + "Email [" + emailSubject + "] is unchecked!");
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "deselectMsg");
    }

    /**
     * Check if the device is wakeup and if it is in natural orientation.
     * @param device A singleton instance of UiDevice.
     */
    public void deviceInitial(UiDevice device) {
        Tools.$Starts(pre, "deviceInitial");
        try {
            device.wakeUp();
            device.setOrientationNatural();
            assertTrue(pre + "ScreenOn: can't wake up", device.isScreenOn());
        } catch (RemoteException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "deviceInitial");
    }

    /**
     * Simulates orienting the device to the left then to the right then into natural orientation.
     * @param device A singleton instance of UiDevice.
     */
    public void deviceRotation(UiDevice device) {
        Tools.$Starts(pre, "deviceRotation");
        try {
            device.setOrientationLeft();
            Tools.logInfor(pre + "Rotate to left");
            sleep(1000);
            device.takeScreenshot(Tools.setFile(new Date().getTime()));
            device.setOrientationRight();
            Tools.logInfor(pre + "Rotate to right");
            sleep(1000);
            device.takeScreenshot(Tools.setFile(new Date().getTime()));
            device.setOrientationNatural();
            Tools.logInfor(pre + "Rotate to nature");
            sleep(1000);
            device.takeScreenshot(Tools.setFile(new Date().getTime()));
        } catch (RemoteException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "deviceRotation");
    }

    /**
     * Returns true if and only if successfully download and open the attachments one by one, call this method in Email/Calendar Attachments view.
     * @param device A singleton instance of UiDevice.
     * @param attNum The number of attachments.
     * @param flag A boolean decides open the attachment or not.
     * @return true if successfully download and open the attachments.
     */
    public boolean downloadAttachments(UiDevice device, int attNum, boolean flag) {
        Tools.$Starts(pre, "downloadAttachments");
        try {
            UiObject attCheckbox = null;
            UiObject attDetail = null;
            UiObject attNameObj = null;
            UiObject attStatusObj = null;
            for (int i = 0; i < attNum; i++) {
                UiObject att = new UiObject(new UiSelector().className("android.widget.LinearLayout").index(i));
                if (!att.waitForExists(3 * tea)) {
                	att = new UiObject(new UiSelector().className("android.widget.LinearLayout").index(i));
                	att.waitForExists(3 * tea);
                }
                attCheckbox = att.getChild(new UiSelector().className("android.widget.CheckBox").resourceId("com.good.android.gfe:id/multi_attachment_checkbox").index(0));
                if (!attCheckbox.waitForExists(3 * tea)) {
                	attCheckbox = att.getChild(new UiSelector().className("android.widget.CheckBox").resourceId("com.good.android.gfe:id/multi_attachment_checkbox").index(0));
                	attCheckbox.waitForExists(3 * tea);
                }
                attDetail = att.getChild(new UiSelector().className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/attachment_detail_container"));
                if (!attDetail.waitForExists(3 * tea)) {
                	attDetail = att.getChild(new UiSelector().className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/attachment_detail_container"));
                	attDetail.waitForExists(3 * tea);
                }
                attNameObj = attDetail.getChild(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/attachment_filename"));
                if (!attNameObj.waitForExists(3 * tea)) {
                	attNameObj = attDetail.getChild(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/attachment_filename"));
                	attNameObj.waitForExists(3 * tea);
                }
                String attName = attNameObj.getText();
                attStatusObj = attDetail.getChild(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/attachment_download_status"));
                if (!attStatusObj.waitForExists(3 * tea)) {
                	attStatusObj = attDetail.getChild(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/attachment_download_status"));
                	attStatusObj.waitForExists(3 * tea);
                }
                String attStatus = attStatusObj.getText();
                if (attCheckbox.isClickable()) {
                    int j = i + 1;
                    Tools.logInfor(pre + j + "-> Skip, Attachment [" + attName + "] has been downloaded");
                    flag = openAttachments(attDetail, device, flag);
                    continue;
                } else if (attStatus.equals("Tap to Download")) {
                    int j = i + 1;
                    Tools.logInfor(pre + j + "-> Tap to download, Attachment [" + attName + "]");
                    sleep(this.blink);
                    attDetail.click();
                    long starts = new Date().getTime();
                    while (!attStatus.equals("Downloaded")) {
                        sleep(5000);
                        attStatusObj = attDetail.getChild(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/attachment_download_status"));
                        if (!attStatusObj.waitForExists(3 * tea)) {
                        	attStatusObj = attDetail.getChild(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/attachment_download_status"));
                        	attStatusObj.waitForExists(3 * tea);
                        }
                        attStatus = attStatusObj.getText();
                        if (!Tools.timer(starts, 5 * this.read, pre)) {
                            break;
                        }
                        if (attStatus.equals("Downloaded")) {
                            Tools.logInfor(pre + "Attachment is downloaded!");
                            flag = openAttachments(attDetail, device, flag);
                        }
                    }
                } else {
                    int j = i + 1;
                    Tools.logInfor(pre + j + "-> Block, Attachment [" + attName + "] has been blocked");
                    continue;
                }
                sleep(500);
            }
            Tools.$Ends(pre, "downloadAttachments");
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        return flag;
    }

    /**
     * Returns true if and only if download and open all the attachments successfully in Email/Calendar Attachments screen.
     * @param device A singleton instance of UiDevice.
     * @param attNum The number of attachments.
     * @return True if download and open all the attachments successfully, false otherwise.
     */
    public boolean downloadAttachments(UiDevice device, int attNum) { //Call this method to download and open attachments one by one;
        Tools.$Starts(pre, "downloadAttachments");
        Map<Integer, Boolean> isAvailable = new HashMap<Integer, Boolean>();
        boolean flag = true;
        try {
            UiObject attCheckbox = null;
            UiObject attDetail = null;
            String attName = null;
            String attStatus = null;
            for (int i = 0; i < attNum; i++) {
                UiObject att = new UiObject(new UiSelector().className("android.widget.LinearLayout").index(i));
                att.waitForExists(5 * hearbeat);
                attCheckbox = att.getChild(new UiSelector().className("android.widget.CheckBox").resourceId("com.good.android.gfe:id/multi_attachment_checkbox").index(0));
                attCheckbox.waitForExists(5 * hearbeat);
                attDetail = att.getChild(new UiSelector().className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/attachment_detail_container"));
                attDetail.waitForExists(5 * hearbeat);
                attName = attDetail.getChild(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/attachment_filename")).getText();
                attStatus = attDetail.getChild(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/attachment_download_status")).getText();
                if (attCheckbox.isClickable()) { //If current attachment is downloaded, open the downloaded attachment
                    int j = i + 1;
                    isAvailable.put(i, true);
                    Tools.logInfor(pre + j + "-> Skip, Attachment [" + attName + "] has been downloaded");
                    openAttachments(attDetail, device);
                    continue;
                } else if (attStatus.equals("Tap to Download")) { //If current attachment isn't downloaded
                    int j = i + 1;
                    isAvailable.put(i, true);
                    Tools.logInfor(pre + j + "-> Tap to download, Attachment [" + attName + "]");
                    this.screenCapture(device);
                    if(attDetail.exists()) {
                    	attDetail.click(); //Tap to download the attachment which status is "tap to download"
                    	this.screenCapture(device);
                    	Tools.logInfor(pre + "Downloading...");
                    }
                    long starts = new Date().getTime();
                    while (!attStatus.equals("Downloaded")) { //Waiting for attachment is being download
                    	sleep(5000);
                    	attDetail.waitForExists(2 * tea);          
                        attStatus = attDetail.getChild(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/attachment_download_status")).getText();    
                        if (!Tools.timer(starts, 5 * this.read, pre)) { //Time for download attachment = 5 min;
                            flag &= false;
                            errorCode = 1;
                            Tools.logInfor(pre + "ErrorCode=1");
                            break;
                        }
                        if (attStatus.equals("Downloaded")) {
                            Tools.logInfor(pre + "Attachment is downloaded!");
                            this.screenCapture(device);
                            openAttachments(attDetail, device);
                        }
                    }
                } else {
                    int j = i + 1;
                    isAvailable.put(i, false);
                    Tools.logInfor(pre + j + "-> Block, Attachment [" + attName + "] has been blocked");
                    continue;
                }
                sleep(500);
            }
            Tools.$Ends(pre, "downloadAttachments");
        } catch (UiObjectNotFoundException e) {
        	errorCode = 400;
        	Tools.logInfor(pre + "ErrorCode=400");
            Tools.logInfor(e);
            flag = false;
        }
        return flag;
    }

    /**
     * Download the Zip file in Email/Calendar Attachments view.
     */
    public void downloadZipFile() {
        Tools.$Starts(pre, "downloadZipFile");

        Tools.$Ends(pre, "downloadZipFile");
    }

    
/* Start with "E" */
    /**
     * Taps on "Time and Date" field in Edit Event view (As Organizer).
     * @param date String "Starts" or "Ends".
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void editTimeAndDate(String date) throws UiObjectNotFoundException {
    	Tools.$Starts(pre, "editTimeAndDate");
    	UiObject startObj = nc.getTimeAndDateOption(date);
    	startObj.waitForExists(this.tea);
    	startObj.clickAndWaitForNewWindow();
    	Tools.$Ends(pre, "editTimeAndDate");
    }
    
    /**
     * Returns the number of selected email, call this method in Email Activity.
     * @param selectCounter A UI element for "? selected" at the top bar.
     * @return the number of selected email.
     */
    public int emailSelectCounter(UiObject selectCounter) {
        Tools.$Starts(pre, "emailSelectCounter");
        int selectCN = 0;
        try {
            String selectCounterText = selectCounter.getText();
            int endIndex = selectCounterText.indexOf("selected");
            String selectCounterNumber = selectCounterText.substring(0, endIndex).trim();
            selectCN = Integer.parseInt(selectCounterNumber);
        } catch (UiObjectNotFoundException e) {
        	this.errorCode = 400;
        	Tools.logInfor("ErrorCode=400");
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "emailSelectCounter");
        return selectCN;
    }
    
    public void enableSignatureOption(boolean flag) throws UiObjectNotFoundException {
    	Tools.$Starts(pre, "enableSignatureOption");
    	UiObject enable = null;
    	if (flag) {
    		Tools.logInfor(pre + "Option Signature has been enabled, no need to enabled again!!!");
    		return;
    	}
    	enable = new UiObject(np.EnableSignatureOption());
    	if (enable.waitForExists(3 * tea)) {
    		enable.clickAndWaitForNewWindow();
    		Tools.logInfor("Tap to enable Signature option!!!");
    	}
    	Tools.$Ends(pre, "enableSignatureOption");
    }

    /**
     * Returns true if and only if successfully entered Invitee into "Required", "Optional" or "Resource" field, call this method in Calendar Activity.
     * @param inviteefield the enum of CalendarItem.
     * @param recipient String for the email address of Invitee.
     * @return true if successfully entered Invitee into "Required", "Optional" or "Resource" field.
     */
    public boolean enterEventInvitee(CalendarItem inviteefield, String recipient) {
        Tools.$Starts(pre, "enterEventInvitee");
        UiObject inviteeField = null;
        UiObject addInvitee	= null;
        UiObject confBtn = null;
        boolean flag = false;
        try {
            String fieldLow = inviteefield.toString().toLowerCase();
            if (!(fieldLow.equals("required") || fieldLow.equals("optional") || fieldLow.equals("resource"))) {
                return false;
            }
            inviteeField = new UiObject(new UiSelector().className("android.widget.TextView").resourceId(
                    "com.good.android.gfe:id/add_" + fieldLow));
            inviteeField.waitForExists(5 * hearbeat);
            if (inviteeField.exists() && inviteeField.isEnabled()) {
                inviteeField.clickAndWaitForNewWindow();
            }
            sleep(this.blink);
            addInvitee = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/invitees").text(
                    "Start typing to search"));
            addInvitee.waitForExists(5 * hearbeat);
            if (addInvitee.exists() && addInvitee.isEnabled()) {
                addInvitee.setText(recipient);
            }
            sleep(this.blink);
            confBtn = new UiObject(new UiSelector().className("android.widget.Button").resourceId(
                    "com.good.android.gfe:id/ok"));
            confBtn.waitForExists(5 * hearbeat);
            if (confBtn.exists() && confBtn.isEnabled()) {
                confBtn.click();
                flag = true;
            }
            Tools.$Ends(pre, "enterEventInvitee");
            return flag;
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
            return false;
        }
    }

    /**
     * Returns true if and only if successfully entered text into "Subject" or "Location" field, call this method in Calendar Activity.
     * @param cfield the enum of CalendarItem. 
     * @param text String for Subject or Location.
     * @return true if successfully entered text into "Subject" or "Location" field.
     */
    public boolean enterEventText(CalendarItem cfield, String text) {
        Tools.$Starts(pre, "enterEventText");
        UiObject textField = null;
        String field = cfield.toString().toLowerCase();
        if (!(field.equals("subject") || field.equals("location"))) {
            return false;
        }
        try {
            textField = new UiObject(new UiSelector().className("android.widget.EditText").resourceId(
                    "com.good.android.gfe:id/" + field.toLowerCase()));
            textField.waitForExists(5 * hearbeat);
            textField.click();
            textField.setText(text);
            sleep(1000);
            Tools.logInfor(pre + "Event " + field.toUpperCase() + " is entered");
            Tools.$Ends(pre, "enterEventText");
            return true;
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
            return false;
        }
    }

    /**
     * Returns true if and only if successfully entered recipients into "To", "Cc" or "Bcc" field, call this method in Email Activity.
     * @param field String "To", "Cc" or "Bcc".
     * @param recipient String for the recipient which format like "abc@de.com".
     * @return true if successfully entered recipients into "To", "Cc" or "Bcc" field.
     */
    public boolean enterMsgRecipent(String field, String recipient) {
        Tools.$Starts(pre, "enterMsgRecipent");
        try {
            UiObject recipField = new UiObject(new UiSelector().className("android.widget.MultiAutoCompleteTextView")
                    .resourceId("com.good.android.gfe:id/" + field.toLowerCase()));
            recipField.waitForExists(5 * hearbeat);
            recipField.click();
            recipField.setText(recipient);
            Tools.logInfor(pre + "Email " + field.toUpperCase() + " is entered");
            Tools.$Ends(pre, "enterMsgRecipent");
            return true;
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
            return false;
        }
    }

    /**
     * Returns true if and only if successfully entered recipients into "To", "Cc" or "Bcc" field, call this method in Email Activity.
     * @param field the enum of EmailItem.
     * @param recipient String for the recipient which format like "abc@de.com".
     * @return true if successfully entered recipients into "To", "Cc" or "Bcc" field.
     */
    public boolean enterMsgRecipent(EmailItem field, String recipient) {
        Tools.$Starts(pre, "enterMsgRecipent");
        try {
            UiObject recipField = new UiObject(new UiSelector().className("android.widget.MultiAutoCompleteTextView")
                    .resourceId("com.good.android.gfe:id/" + field.toString().toLowerCase()));
            recipField.waitForExists(5 * hearbeat);
            recipField.click();
            recipField.setText(recipient);
            Tools.logInfor(pre + "Email " + field.toString().toUpperCase() + " is entered");
            Tools.$Starts(pre, "enterMsgRecipent");
            return true;
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
            return false;
        }
    }

    /**
     * Enter the specified String list of recipients.
     * @param recipients List containing elements to be added to this list.
     */
    public void enterMsgRecipent(List<String> recipients) {
        Tools.$Starts(pre, "enterMsgRecipent");

        Tools.$Ends(pre, "enterMsgRecipent");
    }

    /**
     * Returns true if and only if successfully entered text into "Subject" or "Body" field, call this method in Email Activity.
     * @param field the enum of EmailItem.
     * @param text String for text entered in "Subject" or "Body".
     * @return true if successfully entered text into "Subject" or "Body" field.
     */
    public boolean enterMsgText(EmailItem field, String text) throws IOException {
        Tools.$Starts(pre, "enterMsgText");
        try {
            UiObject textField = new UiObject(new UiSelector().className("android.widget.EditText").resourceId(
                    "com.good.android.gfe:id/" + field.toString().toLowerCase()));
            textField.waitForExists(tea);
            textField.click();
            textField.setText(text);
            sleep(100);
            //Tools.pressBack();
            Tools.logInfor(pre + "Email " + field.toString().toUpperCase() + " is entered");
            Tools.$Ends(pre, "enterMsgText");
            return true;
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
            return false;
        }
    }

    /**
     * Returns true if and only if successfully entered text into "Subject" or "Body" field, call this method in Email Activity.
     * @param field String "Subject" or "Body".
     * @param text String for text entered in "Subject" or "Body". 
     * @return true if successfully entered text into "Subject" or "Body" field. 
     */
    public boolean enterMsgText(String field, String text) {
        Tools.$Starts(pre, "enterMsgText");
        try {
            UiObject textField = new UiObject(new UiSelector().className("android.widget.EditText").resourceId(
                    "com.good.android.gfe:id/" + field.toLowerCase()));
            textField.waitForExists(5 * hearbeat);
            textField.click();
            textField.setText(text);
            sleep(100);
            Tools.logInfor(pre + "Email " + field.toUpperCase() + " is entered");
            Tools.$Ends(pre, "enterMsgText");
            return true;
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
            return false;
        }
    }
    

/* Start with "F" */
    /**
     * Fold the tree of Email folders when the Drawer is displayed, call this method in Email Activity.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void foldingEmailSub() throws UiObjectNotFoundException {
        UiObject subLine2 = new UiObject(new UiSelector(). //Locate the folder under the Inbox (@Line2);
                className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/background_item").index(1));
        subLine2.waitForExists(5 * hearbeat);
        String sub2Text = subLine2.getChild(new UiSelector().className("android.widget.TextView")).getText(); //Get the name of the subfolder at line2;
        if (!sub2Text.startsWith("Outbox")) { //Judge if email subfolder is folding;
            new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                    "com.good.android.gfe:id/child_folders")).click();
            sleep(this.blink);
            Tools.logInfor(pre + "Email subfolder is unfolding, tap to folding"); //If folding, click to folding all subfolders;
        } else {
            Tools.logInfor(pre + "Email subfolder is folding, return");
            return;
        }
    }

    /**
     * Returns a UI element of the specified application in Application Manager screen, call this method in System Settings screen.
     * @param app String for the specified application.
     * @return A UI element of the specified application.
     */
    public UiObject findAppFromAppManager(String app) { //Suit for Samsung device
        Tools.$Starts(pre, "findAppFromAppManager");
        UiObject gfe = null;
        UiObject apps = null;
        UiObject settings =	null;
        UiScrollable appsList = null;
        UiObject application = null;
        UiScrollable settingsListView = null;
        try {
            apps = new UiObject(new UiSelector()
                    .resourceId("com.sec.android.app.launcher:id/home_allAppsIcon").text("Apps"));
            apps.waitForExists(5 * hearbeat);
            if (apps.exists() && apps.isEnabled()) {
                apps.clickAndWaitForNewWindow();
                Tools.logInfor("findAppFromAppManager() -> Apps screen");
            }
            appsList = new UiScrollable(new UiSelector());
            appsList.waitForExists(hearbeat);
            appsList.setAsHorizontalList();
            appsList.scrollToBeginning(1);
            settings = appsList.getChildByText(
                    new UiSelector().className(android.widget.TextView.class.getName()), "Settings"); //Scroll screen to find Settings icon
            settings.waitForExists(hearbeat);
            if (settings.exists() && settings.isEnabled()) {
                settings.clickAndWaitForNewWindow(); //Click on Settings icon
                Tools.logInfor("findAppFromAppManager() -> System settings screen");
            }
            Tools.logInfor("findAppFromAppManager() -> Scroll the screen...to find application manager");
            settingsListView = new UiScrollable(new UiSelector().scrollable(true));
            settingsListView.waitForExists(hearbeat);
            application = settingsListView.getChildByText(
                    new UiSelector().className("android.widget.TextView"), "Application manager"); //Scroll to find Application Manager
            application.waitForExists(hearbeat);
            if (application.exists() && application.isEnabled()) {
                application.clickAndWaitForNewWindow(); //Click on Application Manager item
                Tools.logInfor("findAppFromAppManager() -> System application manager screen");
            }
            Tools.logInfor("findAppFromAppManager() -> Scroll the screen...to find application GFE");
            UiScrollable appListView = new UiScrollable(new UiSelector().scrollable(true));
            appListView.waitForExists(hearbeat);
            gfe = appListView.getChildByText(new UiSelector().className("android.widget.TextView"), app); //Scroll to find app
            gfe.waitForExists(hearbeat);
            Tools.logInfor("Searching app " + app);
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "findAppFromAppManager");
        return gfe;
    }

    /**
     * @deprecated
     * Find the specified application in device Home screen, call this method in Home -> Apps view.
     * @param target the specified application.
     * @param device A singleton instance of UiDevice.
     */
    public void findAppFromHome(String target, UiDevice device) {
        Tools.$Starts(pre, "findAppFromHome");
        UiObject targetApp = null;
        try {
            UiObject appsBtn = new UiObject(new UiSelector().resourceId(
                    "com.sec.android.app.launcher:id/home_allAppsIcon").text("Apps"));
            appsBtn.waitForExists(5 * hearbeat);
            if (appsBtn.exists() && appsBtn.isEnabled()) {
                appsBtn.clickAndWaitForNewWindow();
                Tools.logInfor(pre + "findAppFromHome() -> Apps screen");
            }
            UiScrollable appsList = new UiScrollable(new UiSelector()); //Scroll screen to find the app
            appsList.waitForExists(5 * hearbeat);
            appsList.setAsHorizontalList();
            appsList.scrollToBeginning(1);
            sleep(this.hearbeat);
            targetApp = appsList.getChildByText(new UiSelector().className("android.widget.TextView"), target);
            targetApp.waitForExists(30 * hearbeat);
            if (targetApp.exists() && targetApp.isEnabled()) {
                targetApp.clickAndWaitForNewWindow(); //Click on app icon
                Tools.logInfor(pre + "findAppFromHome() -> Launch app [" + target + "]");
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "findAppFromHome");
    }

    /**
     * Find the specified application in device Home screen, call this method in Home -> Apps view. 
     * @param nameOfApp the specified application.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void findAppFromHome(String nameOfApp) throws UiObjectNotFoundException {
        Tools.$Starts(pre, "findAppFromHome");
        UiObject appToLaunch = null; 
        UiObject allAppsButton = new UiObject(new UiSelector().description("Apps"));
        UiObject allAppsButton1 = new UiObject(new UiSelector().text("Apps"));
        if (allAppsButton.exists()) {
            allAppsButton.clickAndWaitForNewWindow();
        } else if (allAppsButton1.exists()) {
            allAppsButton1.clickAndWaitForNewWindow();
            UiScrollable appViews = new UiScrollable(new UiSelector().scrollable(true));
            //Set the swiping mode to horizontal (the default is vertical)
            appViews.setAsHorizontalList();
            appViews.scrollToBeginning(10); //Otherwise the Apps may be on a later page of apps.
            int maxSearchSwipes = appViews.getMaxSearchSwipes();
            UiSelector selector;
            selector = new UiSelector().className(android.widget.TextView.class.getName());
             //The following loop is to workaround a bug in Android 4.2.2 which fails to scroll more than once into view.
            for (int i = 0; i < maxSearchSwipes; i++) {
                try {
                    appToLaunch = appViews.getChildByText(selector, nameOfApp);
                    if (appToLaunch != null) {
                        //Create a UiSelector to find the Settings app and simulate a user click to launch the app.
                        appToLaunch.clickAndWaitForNewWindow();
                        break;
                    }
                } catch (UiObjectNotFoundException e) {
                    Tools.logInfor(pre + "Did not find match for " + e.getLocalizedMessage());
                }
                for (int j = 0; j < i; j++) {
                    appViews.scrollForward();
                    Tools.logInfor(pre + "scrolling forward 1 page of apps.");
                }
            }
        }
        Tools.$Ends(pre, "findAppFromHome");
    }

    /**
     * Returns true if and only if successfully find the specified event in Calendar Month view, call this method in Calendar Activity.
     * @param eventName String for the specified event name.
     * @return true if successfully find the specified event in Calendar Month view.
     */
    public boolean findEventInCalendar(String eventName, UiDevice device) {
        Tools.$Starts(pre, "findEventInCalendar");
        UiScrollable list = null;
        UiObject eventObj = null;
        boolean flag = false;
        try {
            this.switchToCalenderMode(CalendarMode.Month); //Go to Month view;
            list = new UiScrollable(new UiSelector().className("android.widget.ListView").scrollable(true));
            list.waitForExists(5 * hearbeat);
            eventObj = list.getChildByText(new UiSelector().className("android.widget.TextView"), eventName);
            if (eventObj.waitForExists(5 * hearbeat)) {
                Tools.logInfor("Found the event [" + eventName + "]");
                screenCapture(device);
                flag = true;
            } else {
                Tools.logInfor("@Error: Fail to found event [" + eventName + "]!");
                errorCode = 2;
            	Tools.logInfor("ErrorCode=2");
                flag = false;
            }
        } catch (UiObjectNotFoundException e) {
        	errorCode = 2;
        	Tools.logInfor("ErrorCode=2");
        	Tools.logInfor(pre + "@@Error: Fail to found the event in Calendar!!!");
        }
        Tools.$Ends(pre, "findEventInCalendar");
        return flag;
    }

    /**
     * Returns a UI element for specified event in Calendar Day/Week/Month view, call this method in Calendar Activity.
     * @param eventName String for the specified event name.
     * @param mode the enum of CalendarMode.
     * @return A UI element when successfully find the specified event in Calendar Day/Week/Month view.
     */
    public UiObject findEventInCalendar(String eventName, CalendarMode mode) {
        Tools.$Starts(pre, "findEventInCalendar");
        UiScrollable list = null;
        UiObject eventObj = null;
        
        this.switchToCalenderMode(mode); //Go to Calendar view;
        list = new UiScrollable(new UiSelector().className("android.widget.ListView").scrollable(true));
        list.waitForExists(5 * hearbeat);
        try {
            eventObj = list.getChildByText(new UiSelector().className("android.widget.TextView"), eventName);
            if (eventObj.waitForExists(5 * hearbeat)) {
                Tools.logInfor("Found the event [" + eventName + "]");
                return eventObj;
            } else {
            	errorCode = 2;
				Tools.logInfor("ErrorCode=2");
                Tools.logInfor(pre + "Error: Fail to found event [" + eventName + "]!");
            }
        } catch (UiObjectNotFoundException e) {
        	errorCode = 2;
			Tools.logInfor("ErrorCode=2");
        	Tools.logInfor(e);
        }
        Tools.$Ends(pre, "findEventInCalendar");
        return null;
    }
    
    /**
     * Returns a UI element for specified event in Calendar Day view, call this method in Calendar Activity.
     * @param eventName String for the specified event name.
     * @return A UI element when successfully find the specified event in Calendar Day view.
     */
    public UiObject findEventInDayView(String eventName) {
        Tools.$Starts(pre, "findEventInDayView");
        UiObject eventObj = null;
        try {
            UiScrollable list = nc.getEventDayView();
            list.waitForExists(5 * hearbeat);
            eventObj = list.getChildByText(new UiSelector().className("android.view.View"), eventName);
            eventObj.waitForExists(5 * hearbeat);
            if (eventObj.isEnabled()) {
                Tools.logInfor("Found the event [" + eventName + "]");
                return eventObj;
            } else {
                Tools.logInfor("Error: Fail to found event [" + eventName + "]!");
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "findEventInDayView");
        return null;
    }
    
    /**
     * Returns a UI element for specified item in Preferences tab, call this method in Preferences Activity.
     * @param item String for the specified item.
     * @return A UI element for specified item in Preferences tab.
     */
    public UiObject findItemInPreferences(String item) {
        Tools.$Starts(pre, "findItemInPreferences");
        UiObject itemObj = null;
        try {
            this.gotoActivity(Activity.Preferences);
            UiScrollable preferencesListView = new UiScrollable(new UiSelector().className("android.widget.ListView")
                    .scrollable(true));
            preferencesListView.waitForExists(5 * hearbeat);
            itemObj = preferencesListView.getChildByText(new UiSelector().className("android.widget.TextView"), item);
            Tools.logInfor(pre + "Navigate to Preferences->" + item + "view");
            itemObj.waitForExists(5 * hearbeat);
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "findItemInPreferences");
        return itemObj;
    }

    /**
     * Returns true if and only if successfully force stop the specified application in System Settings, call this method in System Settings view.
     * @param app A UI element for the specified application.
     * @return true if successfully force stop the specified application in System Settings.
     */
    public boolean forceStopApp(UiObject app) {
        Tools.$Starts(pre, "forceStopApp");
        UiObject stopBtn = null;
        UiObject stopDialog = null;
        UiObject okBtn = null;
        try {
            if (app.exists() && app.isEnabled()) {
                app.clickAndWaitForNewWindow(); //Open the app in App Manager;
                Tools.logInfor("Open the app in App Manager");
            }
            stopBtn = new UiObject(new UiSelector().resourceId("com.android.settings:id/left_button").text(
                    "Force stop"));
            stopBtn.waitForExists(5 * hearbeat);
            if (stopBtn.exists() && stopBtn.isEnabled()) {
                stopBtn.click(); //Click on Force Stop button;
                stopDialog = new UiObject(new UiSelector().resourceId("android:id/alertTitle").text(
                        "Force stop"));
                stopDialog.waitForExists(5 * hearbeat);
                okBtn = new UiObject(new UiSelector().resourceId("android:id/button1").text("OK"));
                okBtn.waitForExists(5 * hearbeat);
                if (stopDialog.exists()) {
                    okBtn.click();
                    Tools.logInfor("Force stop the application");
                }
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
            return false;
        }
        Tools.$Ends(pre, "forceStopApp");
        return true;
    }
    

/* Start with "G" */
    /**
     * Gets device API level in System Settings view.
     */
    public void getAPILevel() {
        Tools.$Starts(pre, "getAPILevel");

        Tools.$Ends(pre, "getAPILevel");
    }

    /**
     * Returns an array for the name of a certain amount of attachments.
     * @param attNum The number of attachments.
     * @return An array for attachments name.
     */
    public String[] getAttachmentsName(int attNum) {
        String[] attName = new String[attNum];
        try {
            UiObject attCheckbox = null;
            UiObject attDetail = null;
            UiObject att = null;
            for (int i = 0; i < attNum; i++) {
                att = new UiObject(new UiSelector().className("android.widget.LinearLayout").index(i));
                att.waitForExists(5 * hearbeat);
                attCheckbox = att.getChild(new UiSelector().className("android.widget.CheckBox").resourceId("com.good.android.gfe:id/multi_attachment_checkbox").index(0));
                attCheckbox.waitForExists(5 * hearbeat);
                attDetail = att.getChild(new UiSelector().className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/attachment_detail_container"));
                attDetail.waitForExists(5 * hearbeat);
                attName[i] = attDetail.getChild(
                        new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/attachment_filename")).getText();
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        return attName;
    }

    /**
     * Returns a String for GFE build version, call this method in Preferences Activity.
     * @return A String for GFE build version.
     */
    public String getBuildNumber() {
        Tools.$Starts(pre, "getBuildNumber");
        UiObject appInfor = null;
        String vers = null;
        try {
            UiObject about = this.findItemInPreferences("About");
            about.waitForExists(5 * hearbeat);
            if (about != null) {
                about.clickAndWaitForNewWindow();
            }
            sleep(blink);
            appInfor = new UiObject(new UiSelector().resourceId("android:id/title").text(
                    "Application Information"));
            appInfor.waitForExists(5 * hearbeat);
            if (appInfor.exists() && appInfor.isEnabled()) {
                appInfor.clickAndWaitForNewWindow();
            }
            String version = new UiObject(new UiSelector().className("android.widget.TextView").resourceId(
                    "android:id/summary")).getText();
            vers = version.substring("Version: ".length()).trim();
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "getBuildNumber");
        return vers;
    }
    
    /**
     * Returns a String "day", "week" or "month" for current Calendar view, call this method in Calendar Activity.
     * @return A String for Calendar view.
     */
    public String getCurrentCalendarView() {
    	Tools.$Starts(pre, "getCurrentCalendarView");
    	UiObject dayObj = null;
    	UiObject weekObj = null;
    	String view = null;
    	dayObj = nc.getDayNumberAtTopLeftCorner();
    	weekObj = nc.getWeekNumberAtTopLeftCorner();
    	sleep(this.hearbeat);
    	if(dayObj.exists()) {
    		Tools.logInfor(pre + "Current Calendar view is [Day view]");
    		view = "day";
    	} else if(weekObj.exists()) {
    		Tools.logInfor(pre + "Current Calendar view is [Week view]");
    		view = "week";
    	} else {
    		Tools.logInfor(pre + "Current Calendar view is [Month view]");
    		view = "month";
    	}
    	Tools.$Ends(pre, "getCurrentCalendarView");
    	return view;
    }

    /**
     * Returns the String value for "Day", "Month" or "Day-Month" in opened meeting request, call this method in Meeting Request view.
     * @param arg String "day", "month" or "day-month".
     * @return String for "Day", "Month" or "Day-Month".
     */
    public String getDate(String arg) {
        Tools.$Starts(pre, "getDate");
        UiObject dayObj = null;
        UiObject monthObj = null;
        String date = null;
        try {
            UiObject dateObj = null;
            this.showInCalendar();
            if (arg.toLowerCase().equals("day")) {
                dateObj = new UiObject(new UiSelector().className("android.widget.TextView").resourceId(
                        "com.good.android.gfe:id/dayNumber"));
                dateObj.waitForExists(5 * hearbeat);
                date = dateObj.getText();
            } else if (arg.toLowerCase().equals("month")) {
                dateObj = new UiObject(new UiSelector().className("android.widget.TextView").resourceId(
                        "com.good.android.gfe:id/date2nd"));
                dateObj.waitForExists(5 * hearbeat);
                date = dateObj.getText();
            } else {
                dayObj = new UiObject(new UiSelector().className("android.widget.TextView").resourceId(
                        "com.good.android.gfe:id/dayNumber"));
                dayObj.waitForExists(5 * hearbeat);
                monthObj = new UiObject(new UiSelector().className("android.widget.TextView").resourceId(
                        "com.good.android.gfe:id/date2nd"));
                monthObj.waitForExists(5 * hearbeat);
                date = dayObj.getText() + ", " + monthObj.getText();
            }
            Tools.pressBack();
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        } catch (IOException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "getDate");
        return date;
    }

    /**
     * Navigates to the specified activity. 
     * @param actName the enum of Activity. 
     */
    public void gotoActivity(Activity actName) {
        Tools.$Starts(pre, "gotoActivity");
        UiObject actBtn = null;
        try {
            //Navigate to HomeActivity
            Runtime.getRuntime().exec("am start -n com.good.android.gfe/com.good.android.ui.LaunchHomeActivity");
            Tools.logInfor(pre + "gotoActivity() -> " + actName + " is selected");
            sleep(this.hearbeat);
            this.removeAlertMessage();
            sleep(this.hearbeat);
            //Jump to the selected screen
            UiScrollable navBar = new UiScrollable(new UiSelector().className("android.widget.HorizontalScrollView"));
            navBar.waitForExists(2 * tea);
            navBar.setAsHorizontalList();
            navBar.waitForExists(2 * tea);
            navBar.scrollToBeginning(1);
            navBar.waitForExists(2 * tea);
            navBar.scrollToBeginning(1);
            navBar.waitForExists(2 * tea);
            switch (actName) {
	            case Email: {
	                actBtn = navBar.getChildByText(new UiSelector().className(activityClName), "Email");
	                break;
	            }
	            case Calendar: {
	                actBtn = navBar.getChildByText(new UiSelector().className(activityClName), "Calendar");
	                break;
	            }
	            case Contacts: {
	                actBtn = navBar.getChildByText(new UiSelector().className(activityClName), "Contacts");
	                break;
	            }
	            case Tasks: {
	                actBtn = navBar.getChildByText(new UiSelector().className(activityClName), "Tasks");
	                break;
	            }
	            case ToDo: {
	                actBtn = navBar.getChildByText(new UiSelector().className(activityClName), "To Do");
	                break;
	            }
	            case Applications: {
	                actBtn = navBar.getChildByText(new UiSelector().className(activityClName), "Applications");
	                break;
	            }
	            case Documents: {
	                actBtn = navBar.getChildByText(new UiSelector().className(activityClName), "Documents");
	                break;
	            }
	            case Browser: {
	                actBtn = navBar.getChildByText(new UiSelector().className(activityClName), "Browser");
	                break;
	            }
	            case Preferences: {
	                actBtn = navBar.getChildByText(new UiSelector().className(activityClName), "Preferences");
	                break;
	            }
	            default: {
	                Tools.logInfor(pre + actName.toString());
	            }
            }
            actBtn.waitForExists(5 * hearbeat);
            if (actBtn.exists() && actBtn.isEnabled()) {
                actBtn.click();
                Tools.logInfor(pre + "gotoActivity() -> Navigate to " + actName + " screen!");
                this.removeAlertMessage();
                sleep(this.hearbeat);
            } else {
                Tools.logInfor(pre + ">>>>>>@Error: Fail to navigate to " + actName + " screen!");
            }
            Tools.$Ends(pre, "gotoActivity");
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        } catch (IOException e) {
            Tools.logInfor(e);
        }
    }

    /**
     * Returns true if and only if successfully access Email compose window, call this method in Email Activity.
     * @return true if successfully access Email compose window
     */
    public boolean gotoComposeWindow() {
        Tools.$Starts(pre, "gotoComposeWindow");
        try {
            UiObject composeBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                    "com.good.android.gfe:id/new_email"));
            composeBtn.waitForExists(5 * hearbeat);
            if (composeBtn.exists() && composeBtn.isEnabled()) {
                composeBtn.clickAndWaitForNewWindow();
            } else {
                gotoActivity(Activity.Email);
                composeBtn.clickAndWaitForNewWindow();
            }
            Tools.logInfor(pre + "Email compose screen");
            Tools.$Ends(pre, "gotoComposeWindow");
            return true;
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
            return false;
        }
    }

    /**
     * Navigates to Email subfolder, call this method in Email Activity.
     * @param actName the enum of Activity.
     * @param index the index of Email subfolder. 
     */
    public void gotoSubFolder(Activity actName, int index) {
        Tools.$Starts(pre, "gotoSubFolder");
        UiObject subMenu = null;
        UiObject emailFolder = null;
        gotoActivity(actName); //Go to Email tab;
        try {
            subMenu = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                    "com.good.android.gfe:id/submenu")); //Drawer button;
            if (subMenu.exists() && subMenu.isEnabled()) {
                subMenu.clickAndWaitForNewWindow(); //Tap on Drawer button;
                emailFolder = new UiObject(new UiSelector().className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/background_item").index(index));
                emailFolder.clickAndWaitForNewWindow();
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "gotoSubFolder");
    }

    /**
     * Navigates to Contacts subfolder, call this method in Contacts Activity.
     * @param cfName String for Contacts subfolder name, cannot be empty.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void gotoContactsFolder(String cfName) throws UiObjectNotFoundException {
    	Tools.$Starts(pre, "gotoContactsFolder");
        UiObject drawerInContact = null;
        UiObject cf1Item = null;
        drawerInContact = nco.getBtnInContactsView("submenu");
        drawerInContact.waitForExists(this.tea);
        drawerInContact.clickAndWaitForNewWindow();
        Tools.logInfor(pre + "Click on Submenu button in Contacts view");
        cf1Item = nco.getContactSubfolder(cfName);
        cf1Item.waitForExists(this.tea);
        cf1Item.clickAndWaitForNewWindow();
        Tools.logInfor(pre + "Navigate to contact subfolder [" + cfName + "]");
        Tools.$Ends(pre, "gotoContactsFolder");
    }

    /**
     * Navigates to the specified Email subfolder, call this method in Email Activity.
     * @param efName String for the name of Email subfolder.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void gotoEmailFolder(String efName) throws UiObjectNotFoundException {
    	Tools.$Starts(pre, "gotoEmailFolder");
    	UiObject obj =  null;
    	UiObject drawerList = null;
    	UiObject efolder = null;
    	UiObject subfolder = null;
    	UiObject inboxObj = null;
        this.openDrawer();
        sleep(this.hearbeat);
        obj = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/child_folders"));
        obj.waitForExists(tea);
        if (obj.exists()) {
            this.unfoldingEmailSub();
        }
        sleep(this.hearbeat);
        drawerList = new UiObject(new UiSelector().className("android.widget.ListView").resourceId("com.good.android.gfe:id/email_folders_list"));
        int fcount = 0;
        if (drawerList.waitForExists(3 * this.tea)) {
        	fcount = drawerList.getChildCount();
        } else {
        	drawerList = new UiObject(new UiSelector().className("android.widget.ListView").resourceId("com.good.android.gfe:id/email_folders_list"));
            if (drawerList.waitForExists(3 * this.tea)) {
            	fcount = drawerList.getChildCount();
            }  else {
            	Tools.logInfor("Do Not get the the count of email subfolder, set the number = 7");
            	fcount = 7;
            }
        }
        boolean flag = false;
        if (fcount > 0) { //If email subfolder exist;
            Tools.logInfor(pre + "There are (" + fcount + ") email folders exist!");
            for (int i = 0; i < fcount; i++) { //Locate the email folder one by one;
                efolder = new UiObject(new UiSelector().className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/background_item").index(i));
                efolder.waitForExists(3 * tea);
                subfolder = efolder.getChild(new UiSelector().className("android.widget.TextView"));
                subfolder.waitForExists(3 * tea);
                String fName = null;
                if (subfolder.exists()) {
                    fName = subfolder.getText().toLowerCase(); //Get the name of the subfolder;
                    if (fName.startsWith(efName.toLowerCase())) { //Find the specific subfolder;
                        subfolder.clickAndWaitForNewWindow();
                        sleep(this.blink); //Open the specific subfolder;
                        Tools.logInfor(pre + "Found the email subfolder [" + fName + "], click to Enter");
                        flag = true;
                        break;
                    } else {
                        Tools.logInfor(pre + "[" + fName + "]");
                    }
                }
            }
            inboxObj = new UiObject(new UiSelector().className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/background_item").index(0));
            inboxObj.waitForExists(5 * hearbeat);
            if (inboxObj.exists() && (flag == false)) {
                inboxObj.click();
                Tools.logInfor(pre + "Fail to find the email subfolder [" + efName + "], back to Inbox");
            }
        }
        Tools.$Ends(pre, "gotoEmailFolder");
    }

    
/* Start with "H" */
    /**
     * Taps Agree button to handle the disclaimer in Disclaimer screen.
     */
    public void handleDisclaimer() {
        Tools.$Starts(pre, "handleDisclaimer");
        try {
            UiScrollable disclaimerScreen = new UiScrollable(new UiSelector().className("android.widget.ScrollView"));
            UiObject disclaimer = disclaimerScreen.getChildByText(
                    new UiSelector().className("android.widget.TextView"), "Disclaimer");
            if (disclaimer.exists()) {
                Tools.logInfor(pre + "Disclaimer agree");
                new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/accept").text("Agree"))
                        .clickAndWaitForNewWindow();
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(pre + "No disclaimer");
        }
        Tools.$Ends(pre, "handleDisclaimer");
    }

    /**
     * Taps on "Accept", "Tentative" or "Decline" button with "No response", "Respond with comment" or "Send respond now" option in meeting request, call this method in opened Meeting Request view.
     * @param respond the enum of Respond.
     * @param respondMsg the enum of RespondMsg.
     */
    public void handleMeetingRequest(Respond respond, RespondMsg respondMsg) {
        Tools.$Starts(pre, "handleMeetingRequest");
        UiObject repText = null;
        UiObject sendBtn = null;
        String msg = null;
        try { //Tap on "xxx" button in meeting request(xxx-Accept/Tentative/Decline);
            UiObject acceptBtn = new UiObject(new UiSelector().resourceId(
                    "com.good.android.gfe:id/meeting_request_" + respond.toString().toLowerCase()).text(
                    respond.toString()));
            acceptBtn.waitForExists(5 * hearbeat);
            if (acceptBtn.exists() && acceptBtn.isEnabled()) {
                acceptBtn.clickAndWaitForNewWindow();
                Tools.logInfor("Button " + respond.toString() + " is clicked");
            }
            switch (respondMsg) {
            case None: {
                msg = "No response";
                break;
            }
            case Comment: {
                msg = "Respond with comment";
                break;
            }
            default: {
                msg = "Send response now";
            }
            }
            repText = new UiObject(new UiSelector().resourceId("android:id/text1").text(msg));
            repText.waitForExists(5 * hearbeat);
            if (repText.exists() && repText.isEnabled()) {
                repText.clickAndWaitForNewWindow(); //Select respond meeting with default/comment/none;
                Tools.logInfor("Meeting " + respond.toString() + " -> " + msg);
            }
            sleep(this.blink);
            this.tapButton("OK"); //Tap OK button;
            sleep(this.blink);
            if (msg.equals("Respond with comment")) {
                new UiObject(new UiSelector().className("android.widget.EditText").resourceId("com.good.android.gfe:id/body")).setText("With comments");
                sendBtn = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/send_button"));
                sendBtn.waitForExists(5 * hearbeat);
                if (sendBtn.exists() && sendBtn.isEnabled()) {
                    sendBtn.clickAndWaitForNewWindow();
                }
                Tools.logInfor("Edit comment and send");
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "handleMeetingRequest");
    }
    

/* Start with "I" */
    /**
     * Returns true if and only if the bottom action bar with 'Action/Edit', 'Delete' and 'Forward' buttons exist in Event detail view (As Organizer &Invitee).
     * @param person String "Organizer" or "Invitee", cannot be empty.
     * @return true if the bottom action bar with 'Action/Edit', 'Delete' and 'Forward' buttons exist in Event detail view.
     */
    public boolean isActionBarExistsInCalendarDetailView(String person) {
        boolean flag = false;
        UiObject actBtn = null;
        UiObject delBtn = null;
        UiObject respondBtn = null;
        if (person.toLowerCase().equals("organizer")) {
            actBtn = nc.getEditBtnInCalendarDetailView();
            actBtn.waitForExists(this.tea);
        } else {
            actBtn = nc.getActionBtnInCalendarDetailView();
            actBtn.waitForExists(this.tea);
        }
        delBtn = nc.getDeleteBtnInCalendarDetailView();
        delBtn.waitForExists(this.tea);
        respondBtn = nc.getRespondBtnInCalendarDetailView();
        respondBtn.waitForExists(this.tea);
        if (actBtn.exists() && delBtn.exists() && respondBtn.exists()) {
            flag = true;
            Tools.logInfor("Meeting detail view appears with bottom action bar which contains 'Action(response icon)', 'Delete' and 'Forward' buttons");
        }
        return flag;
    }

    /**
     * Returns true if and only if no contacts exist in current Contacts subfolder.
     * @return true if no contacts exist in current Contacts subfolder.
     */
    public boolean isContactSubfolderEmpty() {
    	Tools.$Starts(pre, "isContactSubfolderEmpty");
    	UiObject emptyText = nco.getEmptyTextInContactsView();
    	boolean flag = false;
		sleep(this.hearbeat);
		if(emptyText.exists()) {
			flag = true;
			Tools.logInfor(pre + "No contacts exist in current contact subfolder");
		} else {
			Tools.logInfor(pre + "There are some contacts exist in current contact subfolder");
		}
     	Tools.$Ends(pre, "isContactSubfolderEmpty");
    	return flag;
    }
    
    /**
     * Compares current Contacts subfolder to the specified Contacts subfolder. The result is true if and only if the argument is not null and is a String object that represents the same Contacts subfolder.
     * @param fName String for Contacts subfolder name.
     * @return true if the String object that represents the same Contacts subfolder.
     */
    public boolean isCurrentContactsFolderEquals(String fName) {
    	Tools.$Starts(pre, "isCurrentContactsFolderEquals");
    	UiObject cFolder = nco.getTitleInContactsView();
    	String name = null;
    	boolean flag = false;
    	try {
	    	cFolder.waitForExists(this.tea);
	    	name = cFolder.getText().toLowerCase();
	    	if(fName.toLowerCase().equals(name)) {
	    		flag = true;
	    		Tools.logInfor(pre + "Current contact subfolder is [" + fName + "]");
	    	} else {
	    		Tools.logInfor(pre + "Current contact subfolder is [" + name + "], rather than [" + fName + "]");
	    	}
    	} catch(UiObjectNotFoundException e) {
    		Tools.logInfor(e);
    		flag = false;
    	}
    	Tools.$Ends(pre, "isCurrentContactsFolderEquals");
    	return flag;
    }

    /**
     * Compares current Email subfolder to the specified Email subfolder. The result is true if and only if the argument is not null and is a String object that represents the same Email subfolder.
     * @param fName String for Email subfolder name.
     * @return true if the String object that represents the same Email subfolder.
     */
    public boolean isCurrentEmailFolderEquals(String fName) {
    	Tools.$Starts(pre, "isCurrentEamilFolderEquals");
    	UiObject eFolder = ne.getTitleInEmailView();
    	String name = null;
    	boolean flag = false;
    	try {
	    	eFolder.waitForExists(this.tea);
	    	name = eFolder.getText().toLowerCase();
	    	if(name.startsWith(fName.toLowerCase())) {
	    		flag = true;
	    		Tools.logInfor(pre + "Current email subfolder is [" + fName + "]");
	    	} else {
	    		Tools.logInfor(pre + "Current email subfolder is [" + name + "], rather than [" + fName + "]");
	    	}
    	} catch(UiObjectNotFoundException e) {
    		Tools.logInfor(e);
    		flag = false;
    	}
    	Tools.$Ends(pre, "isCurrentEamilFolderEquals");
    	return flag;
    }
    
    /**
     * Returns true if and only if Signature is enabled, call this methond in Preferences->Signature view.
     * @return true if Signature is enabled.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public boolean isSignatureEnabled() throws UiObjectNotFoundException {
    	Tools.$Starts(pre, "isSignatureEnabled");
    	UiObject useSignatureObj = null;
    	UiObject signatureTextObj = null;
    	UiObject title = null;
    	title = new UiObject(np.SignatureTitle());
    	if (title.waitForExists(3 * tea)) {
    		useSignatureObj = new UiObject(np.useSignatureOption());
        	useSignatureObj.waitForExists(3 * tea);
        	signatureTextObj = new UiObject(np.signatureTextOption());
        	signatureTextObj.waitForExists(3 * tea);
        	if (useSignatureObj.isEnabled() && signatureTextObj.isEnabled()) {
        		Tools.logInfor(pre + "Signature is enabled!!!");
        		Tools.$Ends(pre, "isSignatureEnabled");
        		return true;
        	} else {
        		Tools.logInfor(pre + "Signature IS NOT enabled!!!");
        		Tools.$Ends(pre, "isSignatureEnabled");
        		return false;
        	}
    	} else {
    		Tools.logInfor(pre + "Please enter Signature view first!!!");
    		Tools.$Ends(pre, "isSignatureEnabled");
    		return false;
    	}
     }
    
    /**
     * Check if current recurring event is a work day event, call this method in event detail view
     * @return boolean True if this event is a work day event, false otherwise
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public boolean isRecurringEventOccursOnWorkDay() throws UiObjectNotFoundException {
    	Tools.$Starts(pre, "isRecurringEventOccursOnWorkDay");
    	UiObject rp = nc.getRecurPattern();
    	if (!rp.waitForExists(3 * tea)) {
    		rp = nc.getRecurPattern();
    		rp.waitForExists(3 * tea);
    	}
    	String text = rp.getText().toLowerCase();
    	if(text.contains("monday") && text.contains("tuesday") && text.contains("wednesday") && text.contains("thursday") && text.contains("friday")) {
    		Tools.$Ends(pre, "isRecurringEventOccursOnWorkDay");
    		Tools.logInfor(pre + "The event occurs on work day!");
    		return true;
    	} else {
    		Tools.logInfor(pre + "Error: The event DOES NOT occurs on work day!!!");
    		Tools.$Ends(pre, "isRecurringEventOccursOnWorkDay");
    		return false;
    	}
    }
    
    /**
     * Returns true if and only if "Edit Event Occurrence" screen displayed.
     * @return true if "Edit Event Occurrence" screen displayed.
     */
    public boolean isEditEventOccurrenceScreenDisplay() {
    	Tools.$Starts(pre, "isEditEventOccurrenceScreenDisplay");
    	UiObject title = nc.getEditEventOccurrenceTitle();
    	Tools.$Ends(pre, "isEditEventOccurrenceScreenDisplay");
    	return title.exists();
    }
    
    /**
     * Returns true if and only if the specified event exists in Calendar Month view.
     * @param eventName String for specified event.
     * @return true if the specified event exists in Calendar Month view.
     */
    public boolean isEventExists(String eventName) {
        Tools.$Starts(pre, "isEventExists");
        boolean flag = false;
        UiObject eventObj = null;
        this.switchToCalenderMode(CalendarMode.Month); //Go to Month view;
        UiScrollable list = new UiScrollable(new UiSelector().className("android.widget.ListView").scrollable(true));
        list.waitForExists(3 * tea);
        try {
            eventObj = list.getChildByText(new UiSelector().className("android.widget.TextView"), eventName);
            eventObj.waitForExists(3 * tea);
            if (eventObj.isEnabled()) {
                Tools.logInfor("Found the event [" + eventName + "]");
                flag = true;
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor("@Error: Fail to found event [" + eventName + "]!");
            flag = false;
        }
        Tools.$Ends(pre, "isEventExists");
        return flag;
    }
    
    /**
     * Returns true if and only if Flag icon exists in Email List view.
     * @param sub String for email subject.
     * @return true if flag icon exists in Email List view.
     * @throws UiObjectNotFoundException If the UI element is not found. 
     */
    public boolean isFlagIconExists(String sub) throws UiObjectNotFoundException {
    	Tools.$Starts(pre, "isFlagIconExists");
    	UiScrollable emalListView = null;
    	UiObject flagObj = null;
    	UiObject emailSubObj = null;
    	UiObject iconsObj = null;
    	boolean f = false;
    	emalListView = ne.getEmailListView();
    	if (!emalListView.waitForExists(3 * tea)) {
    		emalListView = ne.getEmailListView();
    		emalListView.waitForExists(3 * tea);
    	}
    	emailSubObj = emalListView.getChildByText(ne.getEmailSubject(), sub);
    	if (emailSubObj.waitForExists(3 * tea)) {
    		Tools.logInfor(pre + "Found the specified email [" + sub + "]");
    		iconsObj = emailSubObj.getFromParent(ne.getIconsContainer());
    		if (iconsObj.waitForExists(3 * tea)) {
    			Tools.logInfor(pre + "Found the icons container");
    			flagObj = iconsObj.getChild(ne.getFlagIcon());
    			if (flagObj.waitForExists(3 * tea)) {
    				f = true;
            		flagObj.clickAndWaitForNewWindow();
              		Tools.logInfor(pre + "Found the Flag icon!!!");
            	} else {
            		Tools.logInfor(pre + "@Error: Do Not found the Flag icon!!!");
            	}
    		} else {
    			Tools.logInfor(pre + "@Error: Do Not found the icons container!!!");
    			Tools.$Ends(pre, "isFlagIconExists");
    	    	return f;
    		}
    	} else {
    		Tools.logInfor(pre + "@Error: Do Not found the specified email [" + sub + "]!!!");
    		Tools.$Ends(pre, "isFlagIconExists");
        	return f;
    	}
    	Tools.$Ends(pre, "isFlagIconExists");
    	return f;
    }
    
    /**
     * Returns true if and only if Flag context menu exists in Email Detail view.
     * @return true if Flag context menu exists in Email Detail view.
     */
    public boolean isFlagMenuExists() {
    	Tools.$Starts(pre, "isEventExists");
    	UiObject obj = ne.getFlagMenu();
    	if (obj.waitForExists(3 * tea)) {
    		Tools.logInfor("Found Flag context menu!!!");
    		Tools.$Ends(pre, "isEventExists");
    		return true;
    	} else {
    		Tools.logInfor("@Error: Fail to found the Flag context menu!!!");
    		Tools.$Ends(pre, "isEventExists");
    		return false;
    	}   	
    }

    /**
     * Returns true if and only if specified message exists in current Email subfolder, call this method in Email Activity.
     * @param msgSubject String for specified message.
     * @return true if specified message exists in current Email subfolder.
     * @throws UiObjectNotFoundException If the UI element is not found. 
     */
    public boolean isMsgExists(String msgSubject) throws UiObjectNotFoundException {
        Tools.$Starts(pre, "isMsgExists");
        UiObject recvEmail = null;
        UiObject recvEmailSubject = null;
        UiScrollable emailListView = new UiScrollable(new UiSelector().className("android.widget.ListView"));
        emailListView.waitForExists(5 * hearbeat);
        recvEmail = emailListView.getChild(new UiSelector().className("android.widget.LinearLayout"));
        recvEmail.waitForExists(5 * hearbeat);
        recvEmailSubject = recvEmail.getChild(new UiSelector().className("android.widget.TextView").textContains(msgSubject));
        recvEmailSubject.waitForExists(5 * hearbeat);
        if (recvEmailSubject.exists()) {
            recvEmailSubject.clickAndWaitForNewWindow();
            Tools.logInfor(pre + "The email can be found!");
            Tools.$Ends(pre, "isMsgExists");
            return true;
        } else {
        	this.errorCode = 1;
            Tools.logInfor(pre + "Fail to found the email");
            Tools.$Ends(pre, "isMsgExists");
            return false;
        }
    }

    /**
     * Returns true if and only if specified message exists in specified Email subfolder, call this method in Email Activity.
     * @param msgSubject String for specified message.
     * @param folderName String for specified email subfolder.
     * @return true if specified message exists in specified Email subfolder.
     * @throws UiObjectNotFoundException If the UI element is not found. 
     */ 
    public boolean isMsgExists(String msgSubject, String folderName) throws UiObjectNotFoundException {
        Tools.$Starts(pre, "isMsgExists");
        UiObject recvEmail = null;
        gotoEmailFolder(folderName);
        this.scrollToBegin(10);
        UiScrollable emailListView = new UiScrollable(new UiSelector());
        emailListView.waitForExists(5 * hearbeat);
        //Receive and find the email which subject equals msgSubject
        recvEmail = emailListView.getChildByText(new UiSelector().className("android.widget.TextView"), msgSubject);
        if (recvEmail.waitForExists(5 * hearbeat)) {
            Tools.logInfor(pre + "The email [" + msgSubject + "] can be found!");
            Tools.$Ends(pre, "isMsgExists");
            return true;
        } else {
            Tools.logInfor(pre + "Fail to found the email [" + msgSubject + "]");
            Tools.$Ends(pre, "isMsgExists");
            return false;
        }
    }

    /**
     * Returns true if and only if specified message was sent out, call this method in Email Activity.
     * @param msgSubject String for specified message.
     * @return true if specified message was sent out.
     * @throws UiObjectNotFoundException If the UI element is not found. 
     */
    public boolean isMsgSentOut(String msgSubject) throws UiObjectNotFoundException {
        Tools.$Starts(pre, "isMsgSentOut");
        UiObject sub2 = null;
        UiObject sentItem = null;
        UiObject subMenu = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/submenu"));
        subMenu.waitForExists(5 * hearbeat);
        if (subMenu.exists() && subMenu.isEnabled()) {
            subMenu.clickAndWaitForNewWindow(); //Open drawer
            sub2 = new UiObject(new UiSelector().className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/background_item").index(1));
            sub2.waitForExists(5 * hearbeat);
            String sub2Text = sub2.getChild(new UiSelector().className("android.widget.TextView")).getText(); //Location Outbox in drawer
            if (sub2Text.startsWith("Outbox") == false) { //Collapse email subfolders
                new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/child_folders")).click();
                Tools.logInfor(pre + "Collapsing");
            }
            //Check if sent email is stuck at outbox, wait for the email sent out
            long starts = new Date().getTime();
            while (!sub2.getChild(new UiSelector().className("android.widget.TextView")).getText().equals("Outbox")) {
            	this.errorCode = 1;
                Tools.logInfor(pre + "Waiting for message sent out...5s");
                sleep(10000);
                sub2 = new UiObject(new UiSelector().className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/background_item").index(1));
                sub2.waitForExists(5 * hearbeat);
                sub2Text = sub2.getChild(new UiSelector().className("android.widget.TextView")).getText();
                Tools.logInfor(pre + "There are" + sub2Text.substring(6) + " email stuck in outbox");
                if (sub2Text.equals("Outbox") || (!Tools.timer(starts, 5 * 60000, pre))) {
                	if (sub2Text.equals("Outbox")) {
                		this.errorCode = 0;
                	}
                    break;
                }
            }
            sentItem = new UiObject(new UiSelector().className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/background_item").index(2));
            sentItem.waitForExists(5 * hearbeat);
            sentItem.clickAndWaitForNewWindow(); //Go to SentItem
        } else {
            Tools.logInfor(pre + ">>>>>>@Error: Button SubMenu is not found!");
        }
        sleep(3000);
        if (this.isMsgExists(msgSubject)) { //check if sent email exists in sent item
            Tools.$Ends(pre, "isMsgSentOut");
            return true;
        } else {
            Tools.logInfor(pre + ">>>>>>@Error: Fail to found sent email in Sent Items!");
            Tools.$Ends(pre, "isMsgSentOut");
            return false;
        }
    }

    /**
     * Returns true if and only if there are "Flag" "Read/Unread" "Move" "Trash" "Reply" icons exist in the Tool bar, call this method in Email detail view.
     * @return true if there are "Flag" "Read/Unread" "Move" "Trash" "Reply" icons exist in the Tool bar.
     */
    public boolean isToolBarAvailable() {
        Tools.$Starts(pre, "isToolBarAvailable");
        UiObject flagBtn = null;
        UiObject readBtn = null;
        UiObject moveBtn = null;
        UiObject delBtn = null;
        UiObject replyBtn = null;
        boolean flag = false;
        //On Bottom bar are available 5 icons: Flag; Read/Unread; Move; Trash; Reply.
        flagBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                "com.good.android.gfe:id/tick_menu"));
        flagBtn.waitForExists(5 * hearbeat);
        readBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                "com.good.android.gfe:id/unread_menu"));
        readBtn.waitForExists(5 * hearbeat);
        moveBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                "com.good.android.gfe:id/move_to_folder"));
        moveBtn.waitForExists(5 * hearbeat);
        delBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                "com.good.android.gfe:id/delete"));
        delBtn.waitForExists(5 * hearbeat);
        replyBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                "com.good.android.gfe:id/reply_button"));
        replyBtn.waitForExists(5 * hearbeat);
        if (flagBtn.exists() && readBtn.exists() && moveBtn.exists() && delBtn.exists() && replyBtn.exists()) {
            Tools.logInfor(pre + "Tool bar with 5 icons are available");
            flag = true;
        } else {
            flag = false;
        }
        Tools.$Ends(pre, "isToolBarAvailable");
        return flag;
    }

    /**
     * Returns true if and only if the Tool bar is available, call this method in Email detail view.
     * @param i An int.
     * @return true if the Tool bar is available.
     */
    public boolean isToolBarAvailable(int i) {
    	UiObject flagBtn = null;
    	UiObject readBtn = null;
    	UiObject moveBtn = null;
    	UiObject delBtn = null;
        boolean flag = false;
        if (i > 1) {
            Tools.$Starts(pre, "isToolBarAvailable");
            //On Bottom bar are available 5 icons: Flag; Read/Unread; Move; Trash.
            flagBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                    "com.good.android.gfe:id/tick_menu"));
            flagBtn.waitForExists(5 * hearbeat);
            readBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                    "com.good.android.gfe:id/unread_menu"));
            readBtn.waitForExists(5 * hearbeat);
            moveBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                    "com.good.android.gfe:id/move_to_folder"));
            moveBtn.waitForExists(5 * hearbeat);
            delBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                    "com.good.android.gfe:id/delete"));
            delBtn.waitForExists(5 * hearbeat);
            if (flagBtn.exists() && readBtn.exists() && moveBtn.exists() && delBtn.exists()) {
                Tools.logInfor(pre + "Tool bar with 4 icons are available");
                flag = true;
            } else {
                flag = false;
            }
            Tools.$Ends(pre, "isToolBarAvailable");
        } else {
            flag = this.isToolBarAvailable();
        }
        return flag;
    }

    
/* Start with "J" */
/* Start with "K" */

    
/* Start with "L" */
    /**
     * Launches an application by name.
     * @param nameOfAppToLaunch String for application name.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void launchAppCalled(String nameOfAppToLaunch) throws UiObjectNotFoundException {
        getUiDevice().pressHome();
        UiObject allAppsButton = new UiObject(new UiSelector().description("Apps"));
        UiObject allAppsButton1 = new UiObject(new UiSelector().text("Apps"));
        if (allAppsButton.exists()) {
            allAppsButton.clickAndWaitForNewWindow();
        } else if (allAppsButton1.exists()) {
            allAppsButton1.clickAndWaitForNewWindow();
        }
        UiScrollable appViews = new UiScrollable(new UiSelector().scrollable(true));
        //Set the swiping mode to horizontal (the default is vertical)
        appViews.setAsHorizontalList();
        appViews.scrollToBeginning(10); //Otherwise the Apps may be on a later page of apps.
        int maxSearchSwipes = appViews.getMaxSearchSwipes();

        UiSelector selector;
        selector = new UiSelector().className(android.widget.TextView.class.getName());
        UiObject appToLaunch;
        //The following loop is to workaround a bug in Android 4.2.2 which fails to scroll more than once into view.
        for (int i = 0; i < maxSearchSwipes; i++) {

            try {
                appToLaunch = appViews.getChildByText(selector, nameOfAppToLaunch);
                if (appToLaunch != null) {
                    //Create a UiSelector to find the Settings app and simulate a user click to launch the app.
                    appToLaunch.clickAndWaitForNewWindow();
                    break;
                }
            } catch (UiObjectNotFoundException e) {
                System.out.println("Did not find match for " + e.getLocalizedMessage());
            }
            for (int j = 0; j < i; j++) {
                appViews.scrollForward();
                System.out.println("scrolling forward 1 page of apps.");
            }
        }
    }
    
    /**
     * Launches the application GFE.
     * @param device A singleton instance of UiDevice.
     */
    public void launchGFE(UiDevice device) {
        Tools.$Starts(pre, "launchGFE");
        deviceInitial(device);
        device.pressHome();
        try {
            Runtime.getRuntime().exec("am start -n com.good.android.gfe/com.good.android.ui.LaunchHomeActivity");
        } catch (IOException e) {
            Tools.logInfor(e);
        }
        handleDisclaimer();
        unlockGFE(password);
        this.removeAlertMessage();
        sleep(this.breath);
        device.takeScreenshot(Tools.setFile(new Date().getTime())); //Take a screenshot
        Tools.$Ends(pre, "launchGFE");
    }
    
    /**
     * set Lock out timeout in GFE.
     * @param timeout String "number of minuses", "such as 1 minute", "2 minuses", "10 minuses", "1 hour", "2 hours".
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void LockOutTimeOut(String timeout) throws UiObjectNotFoundException {
        //Lock timeout field in Password view.
        UiObject LockTimeoutField = new UiObject(new UiSelector().text("Lock Timeout"));
        //select timeout dialog listview
        UiScrollable listViews = new UiScrollable(new UiSelector().resourceId("android:id/select_dialog_listview"));
        //Folder name in GoodShare.
        UiObject Timeout = new UiObject(new UiSelector().resourceId("android:id/text1").text(timeout));
        //Set timeout in GFE
        LockTimeoutField.clickAndWaitForNewWindow();
        listViews.scrollTextIntoView(timeout);
        Timeout.click();
    }

    
/* Start with "M" */
    /**
     * Moves forward to next day in Calendar day view, call this method in Calendar Activity.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void moveToNextDay() throws UiObjectNotFoundException {
    	UiScrollable day = nc.getEventDayView();
    	day.setAsHorizontalList();
    	day.scrollForward();
    }
    
    /**
     * Moves back to previous day in Calendar day view, call this method in Calendar Activity.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void moveToPreviousDay() throws UiObjectNotFoundException {
    	UiScrollable day = nc.getEventDayView();
    	day.setAsHorizontalList();
    	day.scrollBackward();
    }

    
/* Start with "N" */
    

/* Start with "O" */
    /**
     * Taps on the Drawer button at the upper left corner, call this method in Email Activity.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void openDrawer() throws UiObjectNotFoundException {
        UiObject subMenu = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                "com.good.android.gfe:id/submenu")); //Locate Drawer button;
        if (subMenu.exists() && subMenu.isEnabled()) {
            subMenu.clickAndWaitForNewWindow(); //Click Drawer button to open drawer;
        }
    }

    /**
     * Returns true if and only if successfully opened the specified Email subfolder from Drawer list, call this method in Email Activity.
     * @param folderName String for the name of Email subfolder.
     * @return true if successfully opened the specified Email subfolder.
     */
    public boolean openEmailSub(String folderName) {
        Tools.$Starts(pre, "openEmailSub");
        UiObject sub = null;
        UiObject subfolder = null;
        try {
            boolean flag = false;
            int id = 0;
            int subCount = countEmailSubfolder();
            if (subCount > 0) { //If email subfolder exist;
                Tools.logInfor(pre + "There are (" + subCount + ") email subfolders exist!");
                for (int i = 0; i < subCount; i++) {
                    sub = new UiObject(new UiSelector().className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/background_item").index(i + 1)); //Locate the subfolder one by one;
                    subfolder = sub.getChild(new UiSelector().className("android.widget.TextView"));
                    String subName = null;
                    if (subfolder.exists()) {
                        subName = subfolder.getText().toLowerCase(); //Get the name of the subfolder;
                        if (subName.startsWith(folderName.toLowerCase())) { //Find the specific subfolder;
                            subfolder.clickAndWaitForNewWindow();
                            sleep(this.blink); //Open the specific subfolder;
                            Tools.logInfor(pre + "Found the email subfolder [" + subName + "]");
                            flag = true;
                        } else {
                            Tools.logInfor(pre + "Fail to find the email subfolder");
                        }
                    }
                }
            } else if (subCount == 0) {
                if (folderName.toLowerCase() == "inbox") {
                    id = 0;
                    flag = true;
                } else if (folderName.toLowerCase() == "outbox") {
                    id = 1;
                    flag = true;
                } else if (folderName.toLowerCase() == "sent") {
                    id = 2;
                    flag = true;
                } else if (folderName.toLowerCase() == "draft") {
                    id = 3;
                    flag = true;
                } else if (folderName.toLowerCase() == "delete") {
                    id = 4;
                    flag = true;
                } else {
                    Tools.logInfor(pre + "Error!");
                    return false;
                }
                this.gotoSubFolder(Activity.Email, id);
            } else {
                Tools.logInfor(pre + "Error!");
                return false;
            }
            Tools.$Ends(pre, "openEmailSub");
            return flag;
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
            return false;
        }
    }

    /**
     * Returns true if and only if successfully opened the document, image and email in Good Built-in viewer, call this method in Email/Calendar Attachments view.
     * @param attachment A UI element for attachment.
     * @param device A singleton instance of UiDevice.
     * @param flag A boolean.
     * @return true if successfully opened the document, image and email in Good Built-in viewer.
     */
    public boolean openAttachments(UiObject attachment, UiDevice device, boolean flag) {
        Tools.$Starts(pre, "openAttachments");
        UiObject from = null;
        UiObject to = null;
        UiObject subject = null;
        try {
            attachment.click();
            //Open file in different built-in viewer
            if (new UiObject(new UiSelector().resourceId("android:id/alertTitle").text("Complete action using"))
                    .exists()) {
                if (new UiObject(new UiSelector().resourceId("android:id/text1").text("Built-in Smart Office"))
                        .exists()) {
                    new UiObject(new UiSelector().resourceId("android:id/text1").text("Built-in Smart Office"))
                            .clickAndWaitForNewWindow();
                } else if (new UiObject(new UiSelector().resourceId("android:id/text1").text("Built-in image viewer"))
                        .exists()) {
                    new UiObject(new UiSelector().resourceId("android:id/text1").text("Built-in image viewer"))
                            .clickAndWaitForNewWindow();
                } else if (new UiObject(new UiSelector().resourceId("android:id/text1").text("Built-in EML viewer"))
                        .exists()) {
                    new UiObject(new UiSelector().resourceId("android:id/text1").text("Built-in EML viewer"))
                            .clickAndWaitForNewWindow();
                }
            }
            sleep(this.tea);
            if (new UiObject(new UiSelector().className("android.widget.TextView").resourceId("android:id/message")
                    .text("Unable to open file. Please try opening on your desktop.")).exists()) {
                Tools.logInfor(pre + "Unable to open email due to alert, skip the [file name]");
                new UiObject(new UiSelector().className("android.widget.Button").resourceId("android:id/button1")
                        .text("OK")).click();
                return true;
            }
            String fileName = new UiObject(new UiSelector().className("android.widget.TextView")).getText();
            Tools.logInfor(pre + "Open file [" + fileName + "]");
            if (fileName.endsWith(".eml")) {
                from = new UiObject(new UiSelector().className("android.widget.TextView").resourceId(
                        "com.good.android.gfe:id/from"));
                from.waitForExists(5 * hearbeat);
                Tools.logInfor(pre + from.getText());
                to = new UiObject(new UiSelector().className("android.widget.TextView").resourceId(
                        "com.good.android.gfe:id/to"));
                to.waitForExists(5 * hearbeat);
                Tools.logInfor(pre + to.getText());
                subject = new UiObject(new UiSelector().className("android.widget.TextView").resourceId(
                        "com.good.android.gfe:id/subject"));
                subject.waitForExists(5 * hearbeat);
                Tools.logInfor(pre + subject.getText());
                screenCapture(device);
                flag = true;
            }
            //Back to Attachment Manager view
            while (new UiObject(new UiSelector().className("android.view.View").index(0)).exists()
                    || new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                            "com.good.android.gfe:id/image")).exists()) {
                Runtime.getRuntime().exec("input keyevent 4");
                sleep(1000);
            }
            Tools.$Ends(pre, "openAttachments");
            return flag;
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
            return false;
        } catch (IOException e1) {
            Tools.logInfor(e1);
            return false;
        }
    }

    /**
     * Returns true if and only if successfully opened the document, image and email in Good Built-in viewer, call this method in Email/Calendar Attachments view.
     * @param attachment A UI element for attachment.
     * @param device A singleton instance of UiDevice.
     * @return true if successfully opened the document, image and email in Good Built-in viewer.
     */
    public boolean openAttachments(UiObject attachment, UiDevice device) {
        Tools.$Starts(pre, "openAttachments");
        try {
            attachment.click();
            //Open file in different built-in viewer
            if (new UiObject(new UiSelector().resourceId("android:id/alertTitle").text("Complete action using")).exists()) {
                if (new UiObject(new UiSelector().resourceId("android:id/text1").text("Built-in Smart Office")).exists()) {
                    new UiObject(new UiSelector().resourceId("android:id/text1").text("Built-in Smart Office")).clickAndWaitForNewWindow();
                } else if (new UiObject(new UiSelector().resourceId("android:id/text1").text("Built-in image viewer")).exists()) {
                    new UiObject(new UiSelector().resourceId("android:id/text1").text("Built-in image viewer")).clickAndWaitForNewWindow();
                } else if (new UiObject(new UiSelector().resourceId("android:id/text1").text("Built-in EML viewer")).exists()) {
                    new UiObject(new UiSelector().resourceId("android:id/text1").text("Built-in EML viewer")).clickAndWaitForNewWindow();
                }
            }
            sleep(this.tea);
            if (new UiObject(new UiSelector().className("android.widget.TextView").resourceId("android:id/message").text("Unable to open file. Please try opening on your desktop.")).exists()) {
                Tools.logInfor(pre + "Unable to open email due to alert, skip the [file name]");
                new UiObject(new UiSelector().className("android.widget.Button").resourceId("android:id/button1").text("OK")).click();
            } else {
                Tools.logInfor(pre + "Open file [" + new UiObject(new UiSelector().className("android.widget.TextView")).getText() + "]");
                deviceRotation(device);
                //Back to Attachment Manager view
                while (new UiObject(new UiSelector().className("android.view.View").index(0)).exists()
                        || new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                                "com.good.android.gfe:id/image")).exists()) {
                    device.pressBack();
                    sleep(1000);
                }
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
            return false;
        }
        Tools.$Ends(pre, "openAttachments");
        return true;
    }

    /**
     * Taps to open the specified Event in Calendar Month view, call this method in Calendar Month view.
     * @param eventName String for specified Event name.
     */
    public void openEventInCalendarMonthView(String eventName) {
    	UiObject event = null;
        Tools.$Starts(pre, "openEventInCalendarMonthView");
        try {
            event = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/subject").text(eventName));
            if (event.exists() && event.isEnabled()) {
                event.clickAndWaitForNewWindow();
                Tools.logInfor(pre + "Open event [" + eventName + "]");
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "openEventInCalendarMonthView");
    }

    /**
     * Returns true if and only if successfully opened the speicified message in Email list view, call this method in Email Activity.
     * @param msgSubject String for the specified message's subject.
     * @return true if successfully opened the speicified message in Email list view.
     */
    public boolean openMsg(String msgSubject) {
        Tools.$Starts(pre, "openMsg");
        UiObject recvEmail = null;
        UiObject clipIcon = null;
        boolean flag = false;
        try {
            UiScrollable emailListView = new UiScrollable(new UiSelector().scrollable(true));
            emailListView.waitForExists(5 * hearbeat);
            //Receive and find the email which subject equals msgSubject
            recvEmail = emailListView.getChildByText(new UiSelector().className("android.widget.TextView"), msgSubject);
            //Open the email
            clipIcon = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/read_unread").index(1));
            recvEmail.waitForExists(5 * hearbeat);
            clipIcon.waitForExists(5 * hearbeat);
            if (recvEmail.exists() && clipIcon.exists()) {
                Tools.logInfor(pre + "openMsg() -> Email list view");
                recvEmail.clickAndWaitForNewWindow();
                flag = true;
            }
        } catch (UiObjectNotFoundException e) {
        	errorCode = 3;
        	Tools.logInfor("ErrorCode=3");
            Tools.logInfor("Fail to found [" + msgSubject + "]");
            Tools.$Ends(pre, "openMsg");
            return false;
        }
        Tools.$Ends(pre, "openMsg");
        return flag;
    }

    /**
     * Finds and opens the speicified message with attachments, call this method in Email Activity.
     * @param msgSubject String for the specified message's subject.
     * @param attachment The number of attachments.
     */
    public void openMsg(String msgSubject, int attachment) {
        Tools.$Starts(pre, "openMsg");
        UiObject recvEmail = null;
        UiObject clipIcon = null;
        UiObject clipBtn = null;
        try {
            UiScrollable emailListView = new UiScrollable(new UiSelector().scrollable(true));
            emailListView.waitForExists(5 * this.hearbeat);
            //Receive and fine the email which subject is "msgSubject"
            recvEmail = emailListView.getChildByText(new UiSelector().className("android.widget.TextView"), msgSubject);
            recvEmail.waitForExists(5 * hearbeat);
            clipIcon = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/read_unread").index(1));
            clipIcon.waitForExists(5 * hearbeat);
            if (recvEmail.exists() && clipIcon.exists()) {
                Tools.logInfor(pre + "openMsg() -> Email list view");
                recvEmail.clickAndWaitForNewWindow();
            }
            //Open the email
            clipBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/subject_attachments_button").index(1));
            clipBtn.waitForExists(5 * hearbeat);
            int attachNum = Integer.parseInt(clipBtn.getText());
            if (attachNum == attachment) {
                Tools.logInfor(pre + "Receive an email with " + attachNum + " attachments, the number is correct");
            } else {
                Tools.logInfor(pre + "Receive an email with " + attachNum + " attachments");
            }
            if (clipBtn.exists() && clipBtn.isEnabled()) {
                Tools.logInfor(pre + "openMsg() -> Email detail view");
                clipBtn.clickAndWaitForNewWindow(); //Open Attachments Manager
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "openMsg");
    }

    /**
     * Returns true if and only if the speicified message is opened with correct attachments number, call this method in Email Activity.
     * @param msgSubject String for the specified message's subject.
     * @param attachment The number of attachments.
     * @param flag A boolean.
     * @return true if the speicified message is opened with correct attachments number.
     */
    public boolean openMsg(String msgSubject, int attachment, boolean flag) {
        Tools.$Starts(pre, "openMsg");
        UiObject recvEmail = null;
        UiObject clipIcon = null;
        UiObject clipBtn = null;
        try {
            UiScrollable emailListView = new UiScrollable(new UiSelector().scrollable(true));
            emailListView.waitForExists(5 * this.hearbeat);
            //Receive and fine the email which subject equals msgSubject
            recvEmail = emailListView.getChildByText(new UiSelector().className("android.widget.TextView"), msgSubject);
            recvEmail.waitForExists(5 * hearbeat);
            clipIcon = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/read_unread").index(1));
            clipIcon.waitForExists(5 * hearbeat);
            if (recvEmail.exists() && clipIcon.exists()) {
                Tools.logInfor(pre + "openMsg() -> Email list view");
                recvEmail.clickAndWaitForNewWindow();
            }
            //Open the email
            clipBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/subject_attachments_button").index(1));
            clipBtn.waitForExists(5 * hearbeat);
            int attachNum = Integer.parseInt(clipBtn.getText());
            if (attachNum == attachment) {
                flag = true;
                Tools.logInfor(pre + "Receive an email with " + attachNum + " attachments, the number is correct");
            } else {
                flag = false;
                Tools.logInfor(pre + "Receive an email with " + attachNum + " attachments");
            }
            if (clipBtn.exists() && clipBtn.isEnabled()) {
                Tools.logInfor(pre + "openMsg() -> Email detail view");
                clipBtn.clickAndWaitForNewWindow(); //Open Attachments Manager
            }
            Tools.$Ends(pre, "openMsg");
            return flag;
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
            return flag;
        }
    }

    /**
     * Open the Zip file in Email/Calendar Attachments view.
     */
    public void openZipFile() {
        Tools.$Starts(pre, "openZipFile");

        Tools.$Ends(pre, "openZipFile");
    }

    
/* Start with "P" */
    /**
     * Prints event Starts and Ends time, call this method in Calendar Detail view. 
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void printEventStartAndEndTime() throws UiObjectNotFoundException {
    	Tools.$Starts(pre, "printEventStartAndEndTime");
    	UiObject time = nc.getEventStartAndEndTime();
    	time.waitForExists(tea);
    	Tools.logInfor(pre + time.getText());
    	Tools.$Ends(pre, "printEventStartAndEndTime");
    }
    
    
/* Start with "Q" */
    

/* Start with "R" */
    
    /** 
     * Create a random number for Email subject
     */
    static Random random1 = new Random();
    public static String random2 = Long.toString(random1.nextLong(), 36);
    
    
    
    /**
     * Remove the alert message in "Email", "Calendar" or "Task" Activity.
     */
    public void removeAlertMessage() {
        Tools.$Starts(pre, "removeAlertMessage");
        try {
            UiObject alinC = new UiObject(new UiSelector().resourceId("android:id/alertTitle").text("Switch Views"));
            UiObject alinT = new UiObject(new UiSelector().resourceId("android:id/alertTitle").text("Switch Filters"));
            UiObject alinE = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("android:id/message"));
            if (alinC.exists()) {
                Tools.logInfor(pre + "Alert message displayed in [Calendar] view!");
                this.tapButton("OK");
                Tools.logInfor(pre + "Remove the alert message dialog!");
                new UiObject(new UiSelector().className("android.widget.LinearLayout").index(0)).click();
            } else if (alinT.exists()) {
                Tools.logInfor(pre + "Alert message displayed in [Task] view!");
                this.tapButton("OK");
                Tools.logInfor(pre + "Remove the alert message dialog!");
                new UiObject(new UiSelector().className("android.widget.LinearLayout").index(0)).click();
            } else if (alinE.exists()) {
                Tools.logInfor(pre + "Alert message for OS>4.4 displayed in [Email] view!");
                this.tapButton("OK");
                Tools.logInfor(pre + "Remove the alert message dialog!");
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "removeAlertMessage");
    }

    /**
     * Remove all running applications from Recent App screen, call this method in device Recent App screen.
     * @param device A singleton instance of UiDevice.
     */
    public void removeAppsFromRecent(UiDevice device) {
        Tools.$Starts(pre, "removeAppsFromRecent");
        UiObject removeBtn = null;
        try {
            device.pressRecentApps(); //Go to Recent App screen;
            removeBtn = new UiObject(new UiSelector().className("android.widget.ImageButton").resourceId("com.android.systemui:id/recents_RemoveAll_button_kk"));
            removeBtn.waitForExists(5 * hearbeat);
            if (removeBtn.isEnabled()) {
                Tools.logInfor("Remove all apps in Recent");
                removeBtn.click();
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        } catch (RemoteException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "removeAppsFromRecent");
    }

    /**
     * Taps on Reply button in Email detail view and select "Reply", "Reply all" or "Forward" option, call this method in Email Activity.
     * @param option String "Reply", "Reply all" or "Forward".
     */
    public void respondMsgWithOption(String option) {
    	UiObject replyBtn = null;
    	UiObject optionBtn = null;
        try {
            replyBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/reply_button"));
            replyBtn.waitForExists(5 * this.hearbeat);
            replyBtn.clickAndWaitForNewWindow();
            optionBtn = new UiObject(new UiSelector().resourceId("android:id/text1").text(option));
            optionBtn.waitForExists(5 * this.hearbeat);
            if (optionBtn.exists() && optionBtn.isEnabled()) {
                optionBtn.clickAndWaitForNewWindow();
                Tools.logInfor("Tap to " + option + " the message");
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
    }

    
/* Start with "S" */
    /**
     * Take a screenshot of current window and store it as PNG in default folder /data/local/tmp.
     * @param device A singleton instance of UiDevice.
     */
    public void screenCapture(UiDevice device) {
        Tools.$Starts(pre, "screenCapture");
        device.takeScreenshot(Tools.setFile(new Date().getTime()));
        Tools.$Ends(pre, "screenCapture");
    }
    
    /**
     * Scroll event list in Calendar Month view.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void scrollEventListInMonthView() throws UiObjectNotFoundException {
    	Tools.$Starts(pre, "scrollEventListInMonthView");
    	UiScrollable list = nc.getEventListInMonthView();
    	list.waitForExists(tea);
    	list.scrollForward();
    	Tools.$Ends(pre, "scrollEventListInMonthView");
    }

    /**
     * Scrolls to the beginning of a scrollable UI element. 
     */
    public void scrollToBegin() {
        Tools.$Starts(pre, "scrollToBegin");
        try {
            UiScrollable listView = new UiScrollable(new UiSelector().className("android.widget.ListView").scrollable(
                    true));
            listView.waitForExists(5 * hearbeat);
            for (int i = 0; i < 11; i++) {
                listView.scrollToBeginning(1);
                Tools.logInfor(pre + "Scroll to the top of email list view(" + i + ")");
                sleep(this.hearbeat);
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "scrollToBegin");
    }

    /**
     * Scrolls to the beginning of a scrollable UI element.
     * @param count use count to control the speed.
     */
    public void scrollToBegin(int count) {
        Tools.$Starts(pre, "scrollToBegin");
        try {
            UiScrollable listView = new UiScrollable(new UiSelector().className("android.widget.ListView"));
            listView.waitForExists(hearbeat);
            for (int i = 0; i < count; i++) {
                listView.scrollToBeginning(1);
                Tools.logInfor(pre + "Scroll to the top of email list view(" + i + ")");
                sleep(this.hearbeat);
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "scrollToBegin");
    }
    
    /**
     * Select folder in GoodShare.
     * @param folderName String for folder full name.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void selectFolderInGoodShare(String folderName) throws UiObjectNotFoundException {
        //Folder name in GoodShare.
        UiObject folderNameGS = new UiObject(new UiSelector().resourceId("com.good.goodshare:id/title").text(folderName));
        UiScrollable listViews = new UiScrollable(new UiSelector().className("android.widget.FrameLayout").index(1));
        UiObject content = new UiObject(new UiSelector().resourceId("android:id/content"));
        //validat credential alert
        UiObject validcredential = new UiObject(new UiSelector().resourceId("android:id/alertTitle").textContains("Last validation failed"));
        UiObject validcredentialOKbtn = new UiObject(new UiSelector().resourceId("android:id/button1").text("OK"));
        try {
            //Scroll to the folder
            listViews.scrollIntoView(content);
            listViews.scrollTextIntoView(folderName);
            //Open folder
            folderNameGS.clickAndWaitForNewWindow(60000);
            //Incase need to log in GoodShare again
            if (validcredential.exists()) {
                validcredentialOKbtn.clickAndWaitForNewWindow();
            }

        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
    }

    /**
     * Select message according to the specified message name in Email Activity.
     * @param msgSub String for specified message name.
     */
    public void selectMsg(String msgSub) {
        Tools.$Starts(pre, "selectMsg");
        UiObject select1 = null;
        UiObject checkbox1 = null;
        try {
            UiScrollable emailList = new UiScrollable(new UiSelector().className("android.widget.ListView"));
            emailList.waitForExists(hearbeat);
            emailList.scrollToBeginning(1);
            sleep(this.blink);
            select1 = emailList.getChildByText(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/subject"), msgSub);
            select1.waitForExists(5 * hearbeat);
            checkbox1 = select1.getFromParent(new UiSelector().className("android.widget.CheckBox").resourceId("com.good.android.gfe:id/multi_check"));
            checkbox1.waitForExists(5 * hearbeat);
            if (checkbox1.exists() && (!checkbox1.isChecked())) {
                checkbox1.click();
                Tools.logInfor(pre + "Email [" + msgSub + "] is checked!");
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "selectMsg");
    }

    /**
     * Select message according to the specified message name in Email Activity.
     * @param emailListItem An UI element for specified message. 
     */
    public void selectMsg(UiObject emailListItem) {
        Tools.$Starts(pre, "selectMsg");
        try {
            UiObject emailCheckbox = emailListItem.getChild(new UiSelector().className("android.widget.CheckBox")
                    .resourceId("com.good.android.gfe:id/multi_check"));
            String emailSubject = emailListItem.getChild(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/subject")).getText();
            if (emailCheckbox.exists() && emailCheckbox.isEnabled()) {
                emailCheckbox.click();
                Tools.logInfor(pre + "Email [" + emailSubject + "] is checked!");
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "selectMsg");
    }

    /**
     * Returns true if and only if tap Send button to send message in Email compose window, call this method in Email Activity.
     * @return true if successfully sent out the message in Email compose window.
     */
    public boolean sendMsg() {
        Tools.$Starts(pre, "sendMsg");
        UiObject sendBtn = null;
        UiObject smtpAlert = null;
        try {
            sendBtn = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/send_button"));
            if(!sendBtn.waitForExists(3 * tea)) {
            	sendBtn = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/send_button"));
            	sendBtn.waitForExists(3 * tea);
            }
            if (sendBtn.exists()) {
            	sendBtn.clickAndWaitForNewWindow();
            	Tools.logInfor("Tap on Send button");
            }    
            smtpAlert = new UiObject(new UiSelector().className("android.widget.TextView").text(
            		"This message contains external email addresses.  Deselect recipients who should not receive the message."));
            if (!smtpAlert.waitForExists(3 * tea)) {
            	smtpAlert = new UiObject(new UiSelector().className("android.widget.TextView").text(
                		"This message contains external email addresses.  Deselect recipients who should not receive the message."));
            	smtpAlert.waitForExists(3 * tea);
            }
            if (smtpAlert.exists()) {
                new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/send_button")).clickAndWaitForNewWindow();
            }
            Tools.logInfor(pre + "Message is sending...");
            Tools.$Ends(pre, "sendMsg");
            return true;
        } catch (UiObjectNotFoundException e) {
        	this.errorCode = 400;
            Tools.logInfor(e);
            return false;
        }
    }

    /**
     * Sets password for GFE, call this method in Password Required screen.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void setPassword() throws UiObjectNotFoundException {
    	UiObject prDialog = null;
    	UiObject setPWText = null;
    	UiObject enterPWField = null;
    	UiObject reenterPWField = null;
    	UiObject nextBtn = null;
        Tools.$Starts(pre, "setPassword");
        prDialog = new UiObject(new UiSelector().resourceId("android:id/alertTitle").text("Password Required"));
        prDialog.waitForExists(5 * this.hearbeat);
        if (prDialog.exists() && prDialog.isEnabled()) {
            Tools.logInfor("Password Required screen displayed!");
            this.tapButton("OK");
            Tools.logInfor("Tap OK button to remove Password Required screen!");
        }
        setPWText = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/tvChangePasswordTitle").text("Set password"));
        setPWText.waitForExists(5 * this.hearbeat);
        enterPWField = new UiObject(new UiSelector().className("android.widget.EditText").resourceId("com.good.android.gfe:id/new_password"));
        enterPWField.waitForExists(5 * this.hearbeat);
        reenterPWField = new UiObject(new UiSelector().className("android.widget.EditText").resourceId("com.good.android.gfe:id/re_password"));
        reenterPWField.waitForExists(5 * this.hearbeat);
        if (setPWText.exists() && enterPWField.exists() && reenterPWField.exists()) {
            Tools.logInfor("Set Password screen displayed");
            enterPWField.setText(this.password);
            Tools.logInfor("Enter password \"" + this.password + "\"");
            sleep(this.breath);
            reenterPWField.setText(this.password);
            Tools.logInfor("Re-enter password \"" + this.password + "\"");
            sleep(this.breath);
            nextBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/save").text("Next"));
            nextBtn.waitForExists(5 * this.hearbeat);
            if (nextBtn.exists() && nextBtn.isEnabled()) {
                nextBtn.clickAndWaitForNewWindow();
                Tools.logInfor("Tap Next button to leave Set Password screen");
            }
        }
        Tools.$Ends(pre, "setPassword");
    }

    /**
     * Sets password for GFE.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void setPasswordNew() throws UiObjectNotFoundException {
        UiObject passwordInput = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/new_password"));
        UiObject passwordReInput = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/re_password"));
        UiObject passwordSaveBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/save"));

        try{
            passwordInput.setText("a");
            passwordReInput.setText("a");
            passwordSaveBtn.clickAndWaitForNewWindow();
            Tools.logInfor("Password is: a \n\n");
//            Tools.logProvisionResult("ProvisionSuccessfully");

        }catch  (UiObjectNotFoundException e) {
            Tools.logInfor(e);
//            Tools.logProvisionResult("ProvisionFailed");
        }
            
    }
        


    /**
     * Sets default value to an array for step results, the array returned by this method.  
     * @param stepResult An array.
     * @return An array for initial step results.
     */
    public boolean[] setStepResult(boolean[] stepResult) {
        Tools.$Starts(pre, "setStepResult");
        for (int i = 0; i < stepResult.length; i++) {
            stepResult[i] = false;
        }
        Tools.$Ends(pre, "setStepResult");
        return stepResult;
    }
    
    /**
     * The way used for sort files in GoodShare, call this method in GoodShare.
     * @param string1 String "Name", "Date", "Size" or "Type".
     * @param string2 String "Ascending" or "Descending".
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void sortInGoodShare(String string1, String string2) throws UiObjectNotFoundException {
        // Overflow button in GoodShare
//        UiObject overFlowbuttonGoodShare = new UiObject(new UiSelector().className("android.widget.ImageButton").index(
//                2));
//        UiObject sortbuttonGoodShare = new UiObject(new UiSelector().resourceId("android:id/title").text("Sort"));
        UiObject sortraidobuttonGoodShare = new UiObject(new UiSelector().className("android.widget.RadioButton").text(
                string1));
        UiObject sortraidobuttonGoodShare1 = new UiObject(new UiSelector().className("android.widget.RadioButton")
                .text(string2));
        UiObject okbuttonGoodShare = new UiObject(new UiSelector().className("android.widget.Button").text("OK"));

        try {
            // overFlowbuttonGoodShare.click();
            // sortbuttonGoodShare.click();
            sortraidobuttonGoodShare.click();
            sortraidobuttonGoodShare1.click();
            okbuttonGoodShare.click();

        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
    }

    /**
     * Taps on Show in Calendar button in meeting request screen, call this method in Email Activity.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void showInCalendar() throws UiObjectNotFoundException {
        Tools.$Starts(pre, "showInCalendar");
        UiObject show = new UiObject(new UiSelector().className("android.widget.Button").resourceId(
                "com.good.android.gfe:id/show_in_calendar_button"));
        show.waitForExists(5 * hearbeat);
        if (show.exists() && show.isEnabled()) {
            show.clickAndWaitForNewWindow(); // Click on Show in Calendar button;
        }
        Tools.$Ends(pre, "showInCalendar");
    }

    /**
     * Taps to subscribe the specified Email subfolder in Preferences -> Folder Sync view.
     * @param folderName String for specified Email subfolder.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void subscribeEmailFolder(String folderName) throws UiObjectNotFoundException {
        Tools.$Starts(pre, "subscribeEmailFolder");
        UiScrollable fsList = new UiScrollable(new UiSelector().className("android.widget.ListView"));
        UiObject secLineObj = null;
        UiObject secBoxObj = null;
        UiObject secTextObj = null;
        fsList.waitForExists(this.tea);
        for (int i = 1; i < fsList.getChildCount(); i++) {
            secLineObj = fsList.getChild(new UiSelector().className("android.widget.LinearLayout").index(i));
            secLineObj.waitForExists(this.tea);
            secBoxObj = secLineObj.getChild(new UiSelector().className("android.widget.CheckBox"));
            secBoxObj.waitForExists(this.tea);
            secTextObj = secBoxObj.getFromParent(new UiSelector().className("android.widget.TextView"));
            String strLi = secTextObj.getText().trim().toLowerCase();
            if (i == 1 && strLi.equals("sent items")) {
                Tools.logInfor(pre + "Email subfolders are hiding");
                clickChildFolderButton();
                i--;
                continue;
            }
            if (strLi.startsWith(folderName.toLowerCase())) {
                if (!secBoxObj.isChecked()) {
                    Tools.logInfor(pre + "Email subfolder [" + folderName + "] isn't subscribed");
                    secBoxObj.clickAndWaitForNewWindow();
                    Tools.logInfor(pre + "Check subfolder [" + folderName + "] to subscribe");
                } else {
                    Tools.logInfor(pre + "Email subfolder [" + folderName + "] has been subscribed, Skip");
                }
                break;
            }
        }
        Tools.$Ends(pre, "subscribeEmailFolder");
    }

    /**
     * Returns true if and only if successful subscribed the specified Contacts subfolder in Preferences -> Subfolder Sync view.
     * @param folderName String for specified Contacts subfolder.
     * @return true if successful subscribed the given Contacts subfolder.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public boolean subscribeContactFolder(String folderName) throws UiObjectNotFoundException {
        UiObject conFolderObj1 = null;
        UiObject cf1Checkbox = null;
        UiObject actionBarHomeBtn = null;

        conFolderObj1 = np.getSyncSubfolderItem(folderName);
        conFolderObj1.waitForExists(this.tea);
        cf1Checkbox = np.getSyncSubfolderCheckbox(folderName);
        cf1Checkbox.waitForExists(this.tea);
        if (!cf1Checkbox.isChecked()) {
            conFolderObj1.clickAndWaitForNewWindow();
            Tools.logInfor("Contact Subfolder [" + folderName + "] hasn't been Subscribed, tap to subscribe!");
        }
        boolean flag = cf1Checkbox.isChecked();
        if (flag) {
            Tools.logInfor("Contact Subfolder [" + folderName + "] is Subscribed");
        } else {
            Tools.logInfor("Contact Subfolder [" + folderName + "] is NOT been Subscribed!!!");
        }
        actionBarHomeBtn = np.getActionBarHomeBtn();
        actionBarHomeBtn.waitForExists(this.tea);
        actionBarHomeBtn.clickAndWaitForNewWindow();
        return flag;
    }

    /**
     * This method only used in package com.android.settings. Tested on Nexus 5 OS 4.4.4 and Samsung Note 4 OS 5.0.2, working well.
     * @param statusOfWifi String "ON" or "OFF".
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void switchWiFi(String statusOfWifi) throws UiObjectNotFoundException {
        // Wi-Fi page under settings.
        UiObject WiFi = new UiObject(new UiSelector().resourceId("android:id/title").textContains("Wi-Fi"));
        // Currently Wi-Fi status
        UiObject statusWifi = new UiObject(new UiSelector().className("android.widget.Switch").textContains(
                statusOfWifi));
        // Switch widget
        UiObject switchWidgetNexus5 = new UiObject(new UiSelector().className("android.widget.Switch").resourceId(
                "com.android.settings:id/switchWidget"));
        UiObject switchWidgetSamsungNote4 = new UiObject(new UiSelector().className("android.widget.Switch"));

        try {
            // Open Wi-Fi configuration page under Settings
            WiFi.click();
            // Switch Wi-Fi status.
            if (statusWifi.exists()) {
                Tools.logInfor(pre + "Wi-Fi status already was [" + statusOfWifi + "] ");

            } else {
                // Nexus 5 Switch
                if (switchWidgetNexus5.exists()) {
                    switchWidgetNexus5.click();
                }
                // SamsungNote4 Switch
                else if (switchWidgetSamsungNote4.exists()) {
                    switchWidgetSamsungNote4.click();
                }

                Tools.logInfor(pre + "Wi-Fi status Changed to [" + statusOfWifi + "] ");
            }

        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
    }


    /**
     * Switch from Calendar Day view or Week view to specified view, call this method in Calendar Day/Week view. 
     * @param mode the enum of CalendarMode.
     */
    public void switchToCalenderMode(CalendarMode mode) {
        Tools.$Starts(pre, "switchToCalenderMode");
        String modeStr = mode.toString();
        try { //In Calendar view
            UiObject modeObj = new UiObject(new UiSelector().className("android.widget.RelativeLayout").resourceId("com.good.android.gfe:id/date"));
            modeObj.waitForExists(2 * tea);
            if (modeObj.exists()) { //Click Day/Week/Month navigate button;
                modeObj.clickAndWaitForNewWindow();
                UiObject modeItem = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/action_text").text(modeStr));
                modeItem.waitForExists(2 * tea);
                modeItem.clickAndWaitForNewWindow(); 
                //Click \mode\ button, go to \mode\ view;
                Tools.logInfor("Navigate to " + modeStr + " view");
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "switchToCalenderMode");
    }
    
    /**
     * Switch from Calendar Month view to specified Day or Week view, call this method in Calendar Month view.  
     * @param mode the enum of CalendarMode.  
     */
    public void switchFromMonthToCalenderMode(CalendarMode mode) {
        Tools.$Starts(pre, "switchToCalenderMode");
        String modeStr = mode.toString();
        try { //In Calendar view
            UiObject modeObj = new UiObject(new UiSelector().className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/date"));
            modeObj.waitForExists(5 * hearbeat);
            if (modeObj.exists()) { //Click Day/Week/Month navigate button;
                modeObj.clickAndWaitForNewWindow();
                new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/action_text").text(modeStr)).clickAndWaitForNewWindow(); 
                //Click \mode\ button, go to \mode\ view;
                Tools.logInfor("Navigate to " + modeStr + " view");
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "switchToCalenderMode");
    }
    

/* Start with "T" */
    /**
     * Taps screen to take a photo, call this method in Email Activity or Docs Activity.
     */
    public void takePhoto() {
        Tools.$Starts(pre, "takePhoto");
        try {
            UiObject capBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/tap_prompt").text("Tap the screen to take a photo"));
            if (capBtn.exists() && capBtn.isEnabled()) {
                capBtn.click(); //Tap the screen to take a photo;
            }
            sleep(this.breath);
            new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/accept_button").text("Accept")).clickAndWaitForNewWindow(); //Click Accept button;
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "takePhoto");
    }
    
    
    /**
     * Taps Drawer button in Email and Contact view
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    
    public void tapDrawer() throws UiObjectNotFoundException {
        UiObject drawerButton = new UiObject(
                new UiSelector().resourceId("com.good.android.gfe:id/submenu"));
        drawerButton.click();
    }
    
    
    /**
     * Taps Done button in Time and Date field in Calendar Activity.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void tapDoneBtn() throws UiObjectNotFoundException {
    	Tools.$Starts(pre, "tapDoneBtn");
    	UiObject doneBtn = nc.getDoneBtn();
    	doneBtn.waitForExists(tea);
    	doneBtn.clickAndWaitForNewWindow();
    	Tools.logInfor(pre + "Tap Done button");
    	Tools.$Ends(pre, "tapDoneBtn");
    }
    
    /**
     * Taps on the specified button in Calendar Activity.
     * @param btn String for text on the specified button.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void tapButton(String btn) throws UiObjectNotFoundException {
        Tools.$Starts(pre, "tapButton");
        UiObject btnObj = new UiObject(new UiSelector().resourceId("android:id/button1").text(btn));
        btnObj.waitForExists(3 * tea);
        if (btnObj.exists() && btnObj.isEnabled()) {
            btnObj.clickAndWaitForNewWindow();
            Tools.logInfor(pre + "Tap button " + btn);
        }
        Tools.$Ends(pre, "tapButton");
    }
    
    /**
     * Taps on the specified button in any Activity.
     * @param btn a UI element for the specified button.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void tapButton(UiObject btn) throws UiObjectNotFoundException {
    	Tools.$Starts(pre, "tapButton");
    	if (btn.clickAndWaitForNewWindow()) {
    		Tools.logInfor("Button has been clicked!!!");;
    	}
    	Tools.$Ends(pre, "tapButton");
    }
    
    /**
     * Taps on the specified button "Flag", "Unread", "Move", "Delete" or "Respond" button in Email Detail View.
     * @param btn String for button name "Flag", "Unread", "Move", "Delete" or "Respond".
     * @return true if successful taps on the specified button.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public boolean tapBtnInEmailDetailView(String btn) throws UiObjectNotFoundException {
    	Tools.$Starts(pre, "tapBtnInEmailDetailView");
    	UiObject btnObj = null;
    	if (btn.toLowerCase().equals("flag")) {
    		btnObj = ne.getFlagBtn();
    		if (!btnObj.waitForExists(3 * tea)) {
    			btnObj = ne.getFlagBtn();
    			btnObj.waitForExists(3 * tea);
    		}
    	} else if (btn.toLowerCase().equals("unread")) {
    		btnObj = ne.getReadUnreadBtn();
    		if (!btnObj.waitForExists(3 * tea)) {
    			btnObj = ne.getReadUnreadBtn();
    			btnObj.waitForExists(3 * tea);
    		}
    	} else if (btn.toLowerCase().equals("move")) {
    		btnObj = ne.getMovetoFolderBtn();
    		if (!btnObj.waitForExists(3 * tea)) {
    			btnObj = ne.getMovetoFolderBtn();
    			btnObj.waitForExists(3 * tea);
    		}
    	} else if (btn.toLowerCase().equals("delete")) {
    		btnObj = ne.getDeleteBtn();
    		if (!btnObj.waitForExists(3 * tea)) {
    			btnObj = ne.getDeleteBtn();
    			btnObj.waitForExists(3 * tea);
    		}
    	} else if (btn.toLowerCase().equals("respond")) {
    		btnObj = ne.getRespondBtn();
    		if (!btnObj.waitForExists(3 * tea)) {
    			btnObj = ne.getRespondBtn();
    			btnObj.waitForExists(3 * tea);
    		}
    	} else {
    		Tools.logInfor("@Error: The error button information!!!");
    		return false;
    	}
    	if (btnObj.clickAndWaitForNewWindow()) {
    		Tools.logInfor("Tap on " + btn + "button!!!");
    		Tools.$Ends(pre, "tapBtnInEmailDetailView");
    		return true;
    	} else {
    		Tools.logInfor("@Error: Fail to found " + btn + "button!!!");
    		Tools.$Ends(pre, "tapBtnInEmailDetailView");
    		return false;
    	}
    }
    
    /**
     * Returns true if and only if successfully click on Hour Increment button in Time and Date field in Calendar Activity.
     * @param num The number of click on Hour Increment button. 
     * @return true if successfully click on Hour Increment button.
     */
    public boolean tapIncrementBtn(int num) {
    	Tools.$Starts(pre, "tapIncrementBtn");
    	try {
	    	UiObject incrementBtn = nc.getIncrementBtn();
	    	incrementBtn.waitForExists(this.tea);
	    	for(int i=0; i<num; i++) {
	    		incrementBtn.click();
	    	}
	    	Tools.$Ends(pre, "tapIncrementBtn");
	    	return true;
    	} catch(UiObjectNotFoundException e) {
    		Tools.logInfor(e);
    		return false;
    	}
    }
    
    /**
     * Taps on action Overflow button, call this method in Email -> Compose Window.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void tapOverflow() throws UiObjectNotFoundException {

        try {
            Tools.$Starts(pre, "tapOverflow");
            UiObject overflowObj = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/more"));
            if (overflowObj.exists() && overflowObj.isEnabled()) {
                overflowObj.clickAndWaitForNewWindow();
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }

        Tools.$Ends(pre, "tapOverflow");
    }
    
    /**
     * Taps on the overflow button in GoodShare then select on operation, such as "Sort", "Email links" and "Email Files".
     * @param option String "Sort", "Email links" and "Email Files".
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void tapOverflowSubItemInGoodShare(String option) throws UiObjectNotFoundException {
    	UiObject overflowObjGoodShare = null;
    	UiObject subItemoverflowObjGoodShare = null;
        try {
            Tools.$Starts(pre, "tapoverflowSubItemInGoodShare");
            overflowObjGoodShare = new UiObject(new UiSelector().className("android.widget.ImageButton").index(2));
            overflowObjGoodShare.click();

            Tools.$Starts(pre, "tap" + option + "in overflow in GoodShare");
            subItemoverflowObjGoodShare = new UiObject(new UiSelector().resourceId("android:id/title").text(option));
            subItemoverflowObjGoodShare.clickAndWaitForNewWindow();
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }

        Tools.$Ends(pre, "tapoverflowSubItemInGoodShare");
    }

    /**
     * Returns true if and only if successfully click on Save button in Calendar -> Edit Event Occurrence view.
     * @return true if successfully click on Save button.
     */
    public boolean tapSaveBtn() {
    	Tools.$Starts(pre, "tapSaveBtn");
    	UiObject editOccurrenceTitleObj = null;
    	UiObject save = null;
    	boolean flag = false;
    	try {
	    	editOccurrenceTitleObj = nc.getEditEventOccurrenceTitle();
	    	editOccurrenceTitleObj.waitForExists(tea);
	    	if(editOccurrenceTitleObj.exists()) {
	    		save = nc.getBtnInEditEventView("save");
	        	save.waitForExists(tea);
	        	save.clickAndWaitForNewWindow();
	        	Tools.logInfor(pre + "Tap Save button");
	        	flag = true;
			}
	    	Tools.$Ends(pre, "tapSaveBtn");
	    	return flag;
    	} catch(UiObjectNotFoundException e) {
    		Tools.logInfor(e);
    		return false;
    	}
    }

    
/* Start with "U" */
    /**
     * Unfold the tree of Email folders when the Drawer is displayed, call this method in Email Activity.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public void unfoldingEmailSub() throws UiObjectNotFoundException {
        Tools.$Starts(pre, "unfoldingEmailSub");
        //Locate the folder under the Inbox (@Line2);
        UiObject subLine2 = new UiObject(new UiSelector().className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/background_item").index(1));
        String sub2Text = subLine2.getChild(new UiSelector().className("android.widget.TextView")).getText(); //Get the name of the subfolder at line2;
        if (sub2Text.startsWith("Outbox")) { //Judge if email subfolder is folding;
            new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/child_folders")).click();
            sleep(this.blink);
            Tools.logInfor(pre + "Email subfolder is folding, tap to Unfolding"); //If folding, click to unfolding all subfolders;
        } else {
            Tools.logInfor(pre + "Email subfolder is unfolding, return");
            return;
        }
        Tools.$Ends(pre, "unfoldingEmailSub");
    }

    /**
     * Input password to unlock GFE, call this method in GFE Lock screen.
     * @param password String for unlock password.
     */
    public void unlockGFE(String password) {
        Tools.$Starts(pre, "unlockGFE");
        try {
            UiObject passwordField = new UiObject(new UiSelector().className("android.widget.EditText").resourceId("com.good.android.gfe:id/lockpassword"));
            //passwordField.waitForExists(5*hearbeat);
            if (passwordField.exists()) {
                Tools.logInfor(pre + "Unlock GFE");
                passwordField.setText(password);
                sleep(this.blink);
                new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/btn_unlock").text("GO")).clickAndWaitForNewWindow();
            } else {
                Tools.logInfor(pre + "No password");
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        }
        Tools.$Ends(pre, "unlockGFE");
    }
    
    /**
     * Returns true if and only if successful un-subscribed the specified Contacts subfolder in Preferences -> Subfolder Sync view.
     * @param folderName String for specified Contacts subfolder.
     * @return true if successful un-subscribed the given Contacts subfolder.
     * @throws UiObjectNotFoundException If the UI element is not found.
     */
    public boolean unSubscribeContactFolder(String folderName) throws UiObjectNotFoundException {
        UiObject conFolderObj1 = null;
        UiObject cf1Checkbox = null;
        UiObject actionBarHomeBtn = null;

        conFolderObj1 = np.getSyncSubfolderItem(folderName);
        conFolderObj1.waitForExists(this.tea);
        cf1Checkbox = np.getSyncSubfolderCheckbox(folderName);
        cf1Checkbox.waitForExists(this.tea);
        if (cf1Checkbox.isChecked()) {
            conFolderObj1.clickAndWaitForNewWindow();
            Tools.logInfor("Contact Subfolder [" + folderName + "] has been Subscribed, tap to unsubscribe!");
        }
        boolean flag = !cf1Checkbox.isChecked();
        if (flag) {
            Tools.logInfor("Contact Subfolder [" + folderName + "] is Unsubscribed");
        } 
        actionBarHomeBtn = np.getActionBarHomeBtn();
        actionBarHomeBtn.waitForExists(this.tea);
        actionBarHomeBtn.clickAndWaitForNewWindow();
        return flag;
    }
    

/* Start with "V" */
    

/* Start with "W" */
    /**
     * Returns true if and only if the OTA progress is completed, call this method in Provision or Resync process. 
     * @return true if the OTA progress is completed.
     */
    public boolean waitForOtaCompleted() {
        Tools.$Starts(pre, "waitForOtaCompleted");
        UiObject doneBtn = null;
        boolean flag = true;
        try {
            long starts = new Date().getTime();
            while (!new UiObject(new UiSelector().className("android.widget.TextView").text("Congratulations!")).exists()) {
                sleep(10000);
                UiObject pwdObj = np.getEnterPasswordTextField();
                if (pwdObj.exists()) {
                	this.unlockGFE(password);
                	sleep(3 * this.hearbeat);
                	continue;
                }
                if (!Tools.timer(starts, 5 * this.read, "")) {
                	flag = false;
                	this.errorCode = 1;
                	Tools.logInfor(pre + "errorCode=1");
                    break;
                }
            }
            doneBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/doneBtn").text("Done"));
            doneBtn.waitForExists(5 * hearbeat);
            if (doneBtn.exists() && doneBtn.isEnabled()) {
                Tools.logInfor("Good for Enterprise is set up on device and is ready to use");
                doneBtn.clickAndWaitForNewWindow();
            }
        } catch (UiObjectNotFoundException e) {
        	flag = false;
            Tools.logInfor(e);
            return flag;
        }
        Tools.$Ends(pre, "waitForOtaCompleted");
        return flag;
    }
    

/* Start with "X" */
/* Start with "Y" */
/* Start with "Z" */
}
