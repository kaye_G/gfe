package com.good.android.gfetest;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;

public class Nodes_Preferences extends UiAutomatorTestCase {
/* Nodes in Preferences view */
	/**
	 * Returns a UI element for the title of Preferences in Contact Activity.
	 * @return A UI element for the title of Preferences in Contact Activity. 
	 */
	public UiObject getTitleInPreferencesView() {
		UiObject titleObj = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/title_bar"));
		return titleObj;
	}
	

/* Nodes in Signature view */
	/**
	 * Set the search criteria to match the title in Signature View.
	 * @return UiSelector with the specified search criteria.
	 */
	public UiSelector SignatureTitle() {
		UiSelector sTitle = new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/title").text("Signature");
		return sTitle;
	}
	
	/**
	 * Set the search criteria to match the "Enable" option in Signature View.
	 * @return UiSelector with the specified search criteria.
	 */
	public UiSelector EnableSignatureOption() {
		UiSelector enable = new UiSelector().className("android.widget.TextView").resourceId("android:id/title").text("Enable");
		return enable;
	}
	
	/**
	 * Set the search criteria to match the option "Use Signature" in Signature View.
	 * @return UiSelector with the specified search criteria.
	 */
	public UiSelector useSignatureOption() {
		UiSelector us = new UiSelector().className("android.widget.TextView").resourceId("android:id/title").text("Use Signature");
		return us;
	}
	
	/**
	 * Set the search criteria to match the option "Signature Text" in Signature View.
	 * @return UiSelector with the specified search criteria.
	 */
	public UiSelector signatureTextOption() {
		UiSelector si = new UiSelector().className("android.widget.TextView").resourceId("android:id/title").text("Signature Text");
		return si;
	}

	
/* Nodes in Subfolder Sync view (Open Preferences tab -> Click Subfolder Sync item) */
	/**
	 * Returns a UI element for Contacts Subfolder in Subfolder Sync view.
	 * @param conFolder String for the name of Contacts Subfolder, cannot be empty.
	 * @return A UI element for Contacts Subfolder in Subfolder Sync view.
	 */
	public UiObject getSyncSubfolderItem(String conFolder) {
		UiObject cfObj = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/name").text(conFolder));
		return cfObj;
	}
	
	/**
	 * Returns a UI element for Sync_Check_Box in Subfolder Sync view.
	 * @param conFolder String for the name of Contacts Subfolder, cannot be empty.
	 * @return A UI element for Sync_Check_Box in Subfolder Sync view.
	 * @throws UiObjectNotFoundException If the UI element is not found.
	 */
	public UiObject getSyncSubfolderCheckbox(String conFolder) throws UiObjectNotFoundException {
		UiObject conFolderObj = this.getSyncSubfolderItem(conFolder);
		UiObject cfCheckbox = conFolderObj.getFromParent(
				new UiSelector().className("android.widget.CheckBox").resourceId("com.good.android.gfe:id/sync_check_box"));
		return cfCheckbox;
	}
	
	/**
	 * Returns a UI element for the Back button "Action_Bar_Home" in Subfolder Sync view.
	 * @return A UI element for the Back button "Action_Bar_Home" in Subfolder Sync view.
	 */
	public UiObject getActionBarHomeBtn() {
		UiObject actionBarHomeObj = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/action_bar_home"));
		return actionBarHomeObj;
	}
	
	/**
	 * Returns a UI element for text field "Enter Password" in Lock screen.
	 * @return A UI element for text field "Enter Password" in Lock screen.
	 */
	public UiObject getEnterPasswordTextField() {
		UiObject pwdTextField = new UiObject(new UiSelector().className("android.widget.EditText").resourceId("com.good.android.gfe:id/lockpassword"));
		return pwdTextField;
	}
}
