package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.core.*;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.good.android.gfetest.Frequency.Activity;
import com.good.android.gfetest.Frequency.CalendarMode;

public class ID03CancelMeeting extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
	private String eventSub = "S03";
	private int errorCode = 0;
	private final String pre = "Canceled: ";
	boolean[] stepResult = {false, false};
	String starts, ends;
	
	public void testID03() {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testID03()");
		UiDevice device = getUiDevice();
		freq.launchGFE(device);
		freq.gotoActivity(Activity.Calendar);
/**
 * Step1. On the organizer's Outlook, create a new meeting request then save;
 * Step2. On the organizer's GFE, delete/cancel the meeting. Choose the option "Sending a cancellation and delete meeting".
 */
		Tools.logInfor("Start Step1 ...");
		Tools.logInfor("Start Step2 ...");
		try {
			String ts = Tools.readData();
			eventSub += ts;
			freq.backToToday();
			UiObject eventObj = freq.findEventInCalendar(eventSub, CalendarMode.Month); //Find the meeting in Month view
			errorCode = freq.errorCode;
			freq.screenCapture(device);
			if(eventObj.waitForExists(freq.tea)) {
				this.stepResult[0] = true;
				eventObj.clickAndWaitForNewWindow(); //Open the meeting;
				Tools.logInfor("Open meeting [" + eventSub + "] in Monthly view");
			}
			UiObject delBtn = new UiObject(new UiSelector().
					className("android.widget.ImageView").resourceId("com.good.android.gfe:id/delete"));
			delBtn.waitForExists(freq.tea);
			if(delBtn.isEnabled()) {
				delBtn.clickAndWaitForNewWindow(); //Click on Delete button;
				Tools.logInfor("Click on Delete button in event detail view");
			}
			UiObject delConfirmObj = new UiObject(new UiSelector().resourceId("android:id/alertTitle").text("Confirm Delete"));
			delConfirmObj.waitForExists(freq.tea);
			if(delConfirmObj.isEnabled()) {
				freq.tapButton("OK");
				UiObject cancelObj = new UiObject(new UiSelector().resourceId("android:id/alertTitle").text("Cancellation Needed"));
				cancelObj.waitForExists(freq.tea);
				if(cancelObj.isEnabled()) {
					UiObject sCTDel = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/buttonLabel").text("Send cancellation then delete event"));
					sCTDel.waitForExists(3 * freq.tea);
					sCTDel.clickAndWaitForNewWindow();
					Tools.logInfor("Send cancellation then delete event");
					freq.tapButton("OK");
				}
			}
			sleep(freq.tea * 2);
			stepResult[1] = !freq.isEventExists(eventSub); //The meeting is removed from the Organizer's GMM/Outlook calendars
			if (stepResult[1]) {
				Tools.logInfor("The meeting is removed from the Organizer's calendar!!!");
			} else {
				Tools.logInfor("@Error: The meeting isn't removed from Organizer's calendar!!!");
			}
			freq.gotoActivity(Activity.Email);
			if (freq.isMsgExists(pre + eventSub, "Sent Items")) { //The cancellation notice is sent;
				stepResult[1] &= true;
				Tools.logInfor("The cancellation notice is sent");
			} else {
				stepResult[1] &= false;
				Tools.logInfor("@Error: The cancellation notice isn't sent!!!");
			}
			freq.gotoActivity(Activity.Email);
		} catch(UiObjectNotFoundException e) {
			if (errorCode == 0) {
				errorCode = 400;
				Tools.logInfor("ErrorCode=400");
			}
			Tools.logInfor(e);
		} catch(Exception e) {
			Tools.logInfor(e);
		} finally {
			ends = date.format(new Date());
			Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
			Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
			Tools.doubleLog("@@Smoke ID03 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
			Tools.writeData("03," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
System.out.println(errorCode);
		}
	}
}
