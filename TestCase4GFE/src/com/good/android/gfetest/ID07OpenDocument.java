/**
 * Precondition:
 * 1. Receive a mail with multiple attachments with different size and type, includes below file types: .doc, .docx, .xls, .xlsx, .ppt, .pptx, .pdf, .txt;
 * Step:
 * 1. Select attachment to download, open the attachment in Good SmartOffice.
 * 2. Rotate the screen, check result.
 * 3. Select another attachment to repeat Step#2.
 * Expected Result:
 * 1.1 No abnormal behavior occurs (e.g. crash).
 * 1.2 File displayed normally.
 * 1.3 Make the page fit for screen.
 * ----Completed on Feb 11, 2015
 */

package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;
import com.good.android.gfetest.Frequency.Activity;

import android.os.*;

public class ID07OpenDocument extends UiAutomatorTestCase {
	private final int attachment = 8;
	private final String msgSubject = "SmokeID07";
	private int errorCode = 0;
	Frequency freq = new Frequency();
	boolean[] stepResult = {false, false, false};
	String starts, ends;
	
	public void testID07() throws UiObjectNotFoundException, RemoteException {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testID07()");
		UiDevice device = getUiDevice();
		
		try {
		/**Step 1. Select attachment to download, open the attachment in Good SmartOffice.*/
			Tools.logInfor("Start Step1 ...");
			//Go to Email tab
			freq.launchGFE(device);
			freq.gotoActivity(Activity.Email);
			freq.openMsg(msgSubject, attachment);
			sleep(freq.blink);
			
			/**Step 2. Rotate the screen.
			  *Step 3. Select another attachment to repeat*/
			//Tap to download eight attachments in Attachment Manager screen
			Tools.logInfor("Start Step2&3 ...");
			this.stepResult[0] = freq.downloadAttachments(device, attachment);
			if (!this.stepResult[0]) {
				errorCode = freq.errorCode;
			}
			sleep(freq.blink);
			stepResult[1] = stepResult[0];
			stepResult[2] = stepResult[0];
			freq.gotoActivity(Activity.Email);
		} catch(Exception e) {
			Tools.logInfor(e);
		}
		ends = date.format(new Date());
		Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
		Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
		Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));
		Tools.doubleLog("@@Smoke ID07 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
		Tools.writeData("07," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
//System.out.println(errorCode);
	}
}
