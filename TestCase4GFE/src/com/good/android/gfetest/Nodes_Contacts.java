package com.good.android.gfetest;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;

public class Nodes_Contacts extends UiAutomatorTestCase {
	/**
	 * Returns a UI element for "submenu", "search_button" or "new_contact" button in Contacts Activity.
	 * @param button String "submenu", "search_button" or "new_contact", cannot be empty.
	 * @return A UI element for "submenu", "search_button" or "new_contact" button in Contacts Activity.
	 */
	public UiObject getBtnInContactsView(String button) {
		UiObject btnObj = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/" + button));
		return btnObj;
	}
	
	/**
	 * Returns a UI element for Contacts Subfolder when open Drawer List in Contacts Activity.
	 * @return A UI element for Contacts Subfolder when open Drawer List in Contacts Activity.
	 */
	public UiObject getContactSubfolder(String fName) {
		UiObject cfObj = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/name").text(fName));
		return cfObj;
	}
	
	/**
	 * Returns a UI element for text "No contacts" in Contacts Activity.
	 * @return A UI element for text "No contacts" in Contacts Activity.
	 */
	public UiObject getEmptyTextInContactsView() {
		UiObject emptyTextObj = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/emptyText").text("No contacts"));
		return emptyTextObj;
	}
	
	/**
	 * Returns a UI element for the title of Contacts folder in Contacts Activity.
	 * @return A UI element for the title of Contacts folder in Contacts Activity.
	 */
	public UiObject getTitleInContactsView() {
		UiObject titleObj = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/title"));
		return titleObj;
	}
}
