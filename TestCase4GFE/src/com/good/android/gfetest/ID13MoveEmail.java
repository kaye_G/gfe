package com.good.android.gfetest;


import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.core.*;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.good.android.gfetest.Frequency.Activity;

public class ID13MoveEmail extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
	private int errorCode = 0;
	boolean[] stepResult = new boolean[4];
	private final String emailSub = "SmokeID13";
	private final String subfolder = "SUB1";
	
	public void testID13() {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testID13()");
		freq.setStepResult(stepResult);
		UiDevice device = getUiDevice();
		freq.launchGFE(device);
		freq.gotoActivity(Activity.Email);
		sleep(freq.blink);
/**
 * Step1. Press and hold one of the email.		
 */
		Tools.logInfor("Start Step1 ...");
		try {
			freq.gotoEmailFolder("Inbox");
			sleep(freq.blink);
			freq.selectMsg(this.emailSub); //Select email which subject is "Smoke13";
			sleep(freq.blink);
			this.stepResult[0] = freq.isToolBarAvailable(); //Expected result: On Bottom bar are avilable 5 icons: Flag; Read/Unread; Move; Trash; Reply.
			sleep(freq.blink);
			UiObject selectNum = new UiObject(new UiSelector().
					className("android.widget.TextView").resourceId("com.good.android.gfe:id/selection_counter_text")); 
			selectNum.waitForExists(freq.hearbeat);
			if(selectNum.exists()) {
				if(selectNum.getText().trim().equals("1 selected")) {
					this.stepResult[0] &= true; //Expected result: string 1 selected is displayed;
					Tools.logInfor("Text [1 selected] displayed");
				} else {
					this.stepResult[0] = false;
				}
			} else {
				this.stepResult[0] = false;
			}
			freq.screenCapture(device);
			sleep(freq.blink);
/**
 * Step2. Choose move icon.
 * Step3. Choose folder and tap on it.
 */
			Tools.logInfor("Start Step2 ...");
			UiObject moveBtn = new UiObject(new UiSelector().
					className("android.widget.ImageView").resourceId("com.good.android.gfe:id/move_to_folder"));
			moveBtn.waitForExists(freq.hearbeat);
			if(moveBtn.exists()) {
				moveBtn.clickAndWaitForNewWindow(); 
				Tools.logInfor("Tap to move selected email");
			}
			Tools.logInfor("Start Step3 ...");
			UiObject mvFolder = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/title").text("Move to folder:"));
			mvFolder.waitForExists(freq.hearbeat);
			if(mvFolder.exists()) {
				this.stepResult[1] = true; //ER: Move to folder screen is displayed.
				Tools.logInfor("Move to folder screen is displayed");
				freq.screenCapture(device);
				sleep(freq.blink);
				
				UiObject fList = new UiObject(new UiSelector().className("android.widget.ListView").resourceId("android:id/list"));
				fList.waitForExists(freq.hearbeat);
				int subCount = fList.getChildCount();
				for(int i=0; i<subCount; i++) {
					UiObject sub = new UiObject(new UiSelector(). //Locate the subfolder one by one;
							className("android.widget.LinearLayout").index(i+1));
					sub.waitForExists(freq.hearbeat);
					UiObject subfolder = sub.getChild(new UiSelector().className("android.widget.TextView"));
					subfolder.waitForExists(freq.hearbeat);
					String subName = null;
					if(subfolder.exists()) {
						subName = subfolder.getText().toLowerCase(); //Get the name of the subfolder;
						if(subName.startsWith(this.subfolder.toLowerCase())) { //Find the specific subfolder;
							subfolder.clickAndWaitForNewWindow();
							sleep(freq.blink); //Open the specific subfolder;
							Tools.logInfor("Found the email subfolder [" + subName + "]"); 
						} else {
							continue;
						}
						freq.screenCapture(device);
					}
				}
			}
			freq.scrollToBegin(10);
/**
 * Step4. Go to Chosen folder.
 */
			Tools.logInfor("Start Step4 ...");
			if(!freq.isMsgExists(emailSub)) {
				freq.screenCapture(device);
				sleep(5*freq.hearbeat);
				freq.screenCapture(device);
				
				freq.gotoEmailFolder(subfolder);
				sleep(freq.blink);
				freq.scrollToBegin(5);
				this.stepResult[3] = freq.isMsgExists(emailSub); //ER: Email is moved to that folder.
				freq.screenCapture(device);
			}
			this.stepResult[2] = this.stepResult[3];
/**
 * Step5. Go to OWA.		
 */		
			Tools.logInfor("Start Step5 ...");
		} catch(UiObjectNotFoundException e) {
			if (errorCode == 0) {
				errorCode = 400;
				Tools.logInfor("ErrorCode=400");
			}
			Tools.logInfor(e);
		} catch(Exception e) {
			Tools.logInfor(e);
		} finally {
			String ends = date.format(new Date());
			freq.gotoActivity(Activity.Email);
			Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
			Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
			Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));
			Tools.doubleLog("@@Step4 result -> " + Tools.getResult(stepResult[3]));
			Tools.doubleLog("@@Step5 result -> Need to check this step on OWA...");
			Tools.doubleLog("@@Smoke ID13 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
			Tools.writeData("13," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
		}
	}
}
