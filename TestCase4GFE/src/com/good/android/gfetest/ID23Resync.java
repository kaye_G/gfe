package com.good.android.gfetest;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;
import com.good.android.gfetest.Frequency.Activity;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ID23Resync extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
    private int errorCode = 0;
	boolean[] stepResult = new boolean[2];
	private static final String item = "About";
	private static final String debug = "input text debug";
	private static final String command = "resync now";
	
	public void testID23() {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testID23()");
		freq.setStepResult(stepResult);
		UiDevice device = getUiDevice();
		freq.launchGFE(device);

		try {
/** 
 * Step 1. Go to GFE->Preferences->about->tap menu key several times, input "debug" in pop up dialog;
 */
			Tools.logInfor("Start Step1 ...");
			UiObject about = freq.findItemInPreferences(item);
			about.waitForExists(freq.hearbeat);
			if(about != null) {
				about.clickAndWaitForNewWindow(); //Go to Preferences->About;
			}
			Tools.logInfor("Go to About Item screen");
			Runtime.getRuntime().exec(debug);
			Tools.logInfor("Go to Debug screen");
			sleep(freq.blink);
/**
 * Step 2. Input "resync now" and press carriage return, click hard back key to go out of currently screen, then press ok button to trigger resync;
 */
			Tools.logInfor("Start Step2 ...");
			UiObject dbgInput = new UiObject(new UiSelector().
					className("android.widget.EditText").resourceId("com.good.android.gfe:id/GFEDbgInput"));
			dbgInput.waitForExists(freq.hearbeat);
			if(dbgInput.exists() && dbgInput.isEnabled()) {
				dbgInput.setText(command);
				sleep(freq.blink);
				Tools.logInfor("Input debug command [resync now]");
				device.pressEnter();
			}
			sleep(freq.blink);
			UiObject alertTitle = new UiObject(new UiSelector().resourceId("android:id/alertTitle").text("Resynchronize"));
			alertTitle.waitForExists(freq.hearbeat);
			if(alertTitle.isEnabled()) {
				Tools.logInfor("Alert message for Resync is displayed");
				new UiObject(new UiSelector().resourceId("android:id/button1").text("OK")).clickAndWaitForNewWindow();
				Tools.logInfor("Click OK button to trigger Resync...");
				this.stepResult[0] = true; //The re-sync message should pop up;
			}
			sleep(freq.blink);
			//this.stepResult[1] = freq.waitForOtaCompleted(); //The re-sync complete without exception;
			if (!freq.waitForOtaCompleted()) {
				errorCode = freq.errorCode;
				Tools.doubleLog("Stuck at starting severice 5mins will presshome as workaround");
	            device.pressHome();
	            Runtime.getRuntime().exec("am start -n com.good.android.gfe/com.good.android.ui.LaunchHomeActivity");
	            this.stepResult[1] = freq.waitForOtaCompleted();
			} else {
				this.stepResult[1] = true;
			}
			sleep(freq.blink);
		} catch(UiObjectNotFoundException e) {
			Tools.logInfor(e);
			if (errorCode == 0) {
				errorCode = 400;
				Tools.logInfor("ErrorCode=400");
			}
		} catch(IOException e) {
			if (errorCode == 0) {
				errorCode = 500;
				Tools.logInfor("ErrorCode=500");
			}
			Tools.logInfor(e);
		} catch(Exception e) {
			Tools.logInfor(e);
		} finally {
			String ends = date.format(new Date());
			if(this.stepResult[1]) {
				freq.gotoActivity(Activity.Email);
			}
			Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
			Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
			Tools.doubleLog("@@Smoke ID23 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
			Tools.writeData("23," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
		}	
	}
}
