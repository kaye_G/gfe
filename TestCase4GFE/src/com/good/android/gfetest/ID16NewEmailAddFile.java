package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.good.android.gfetest.Frequency.Activity;

public class ID16NewEmailAddFile extends UiAutomatorTestCase {
    private int errorCode = 0;
    public void testID16NewEmailAddFile() throws UiObjectNotFoundException {
        boolean[] stepResult = new boolean[]{false,false,false,false};
        Frequency freq = new Frequency();

        Tools.doubleLog("==========================================================================================");
        Tools.doubleLog("Start to execute method testID16()");
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String starts = date.format(new Date());

        try {
            UiDevice device = getUiDevice();
            freq.launchGFE(device);
            freq.gotoActivity(Activity.Email);
            // Create a random number for Email subject
            Random random1 = new Random();
            String RandomEmailSubject = Long.toString(random1.nextLong(), 36);

            /**
             * // **********Precondition********** Precondition for this
             * testcase: 1. The 'Sending attachment' policy is enabled from GMC
             * 2. "File Repository" is enabled from GMC
             * 3."Allow access to camera & device photo gallery" is enabled on
             * GMC 4.
             * "Enable email recipient warning for unauthorized email domains"
             * UNCHECK on GMC 5. Configure input method and set to Google input,
             * don't use other IME 6. Make a file in GFR
             * */
            // Below is for Precondition 6, Make a file in GFR
            UiObject GoodTabScroll = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/goodTabScroll"));
            GoodTabScroll.swipeRight(10);
            UiObject GFRTab = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/tab_framer")
                    .childSelector(new UiSelector().resourceId("com.good.android.gfe:id/tab_text").text("Documents")));
            GFRTab.click();

            // Capture Picture
            // Tap add photo button.
            UiObject AddPhoto = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/add_photo"));
            AddPhoto.click();

            // Then add photo by Take Photo
            UiObject TakePhoto = new UiObject(new UiSelector().text("Take photo"));
            TakePhoto.click();

            // Tap screen and accept the photo
            UiObject CapturePictureTakePhoto = new UiObject(
                    new UiSelector().resourceId("com.good.android.gfe:id/camera_view"));
            CapturePictureTakePhoto.clickAndWaitForNewWindow();
            UiObject CapturePictureTakePhotoAccept = new UiObject(
                    new UiSelector().resourceId("com.good.android.gfe:id/accept_button"));
            CapturePictureTakePhotoAccept.click();

            // Save photo in GFR
            UiObject SaveButton = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/save_button"));
            SaveButton.click();

            // Write precondition results to file
            // PublicVoid.log("    Precondition Step6 DONE  \n");
            Tools.doubleLog("Precondition Step6 DONE");

            /**
             * // **********Step1********** Step1: Go to Email tab, tap to
             * compose a new email. Tab on Email tab page Scroll tab to left
             */

            GoodTabScroll.swipeRight(2);
            sleep(1000);
            UiObject EmailTab = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/tab_framer").index(0));
            EmailTab.click();

            UiScrollable EmailListViewScroll = new UiScrollable(
                    new UiSelector().resourceId("com.good.android.gfe:id/emailList"));
            EmailListViewScroll.scrollToBeginning(2, 2);

            // Compose a new email
            UiObject ComposeEmail = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/new_email"));
            stepResult[0] = ComposeEmail.click();

            // Write testing results to file
            // PublicVoid.log("    Step1 PASS  \n");
            Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));

            /**
             * // **********Step2********** // Step2: Click Paperclip icon,
             * select a file to attach. Check result 2. Tap attachment button
             */
            UiObject AttachmentButton = new UiObject(
                    new UiSelector().resourceId("com.good.android.gfe:id/subject_attachments_button"));
            AttachmentButton.click();

            // Try to attach file from attachment management list view
            UiObject AddAttachment = new UiObject(
                    new UiSelector().resourceId("com.good.android.gfe:id/add_attachments"));
            AddAttachment.click();

            // Tap "File From Documents"
            UiObject FileFromDocuments = new UiObject(new UiSelector().text("File from Documents"));
            FileFromDocuments.click();

            // Check attachment from GFR then attach
            UiObject MultiCheck = new UiObject(new UiSelector().className("android.widget.LinearLayout").index(0)
                    .childSelector(new UiSelector().resourceId("com.good.android.gfe:id/multi_check")));
            MultiCheck.click();
            UiObject AttachFile = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/attach_button"));
            AttachFile.click();

            // Go back to Email compose view
            UiObject ActiionBarHome = new UiObject(
                    new UiSelector().resourceId("com.good.android.gfe:id/action_bar_home"));
            stepResult[1] = ActiionBarHome.click();

            // Write testing results to file
            Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
            /**
             * // **********Step3********** Step3:Add recipients and sent it,
             * check result 3. Input email address then send it out. In this
             * case, send the email to itself asia1new@asia.qagood.com
             */
            UiObject EmailAddressInput = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/to"));
            EmailAddressInput.click();
            EmailAddressInput.setText("asia1new@asia.qagood.com");
            getUiDevice().pressEnter();

            // Input email subject
            UiObject EmailSubjectInput = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/subject"));
            EmailSubjectInput.setText(RandomEmailSubject);
            getUiDevice().pressEnter();

            // Tap send button.
            UiObject EmailSendButton = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/send_button"));
            EmailSendButton.click();

            // Waiting for max 300s for new email arriving then open this email.
            UiObject EmailSubjectContent = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/subject")
                    .textContains(RandomEmailSubject));
            EmailSubjectContent.waitForExists(300000);
            EmailSubjectContent.click();

            // Check the attachment in this email if existed and should be
            // opened by Good Image Viewer
            AttachmentButton.click();
            UiObject tapToDownload = new UiObject(new UiSelector().className("android.widget.LinearLayout").index(0));

            // Waiting for max 180s for downloading attachment.
            tapToDownload.click();
            UiObject attachmentDetailContainer = new UiObject(
                    new UiSelector().resourceId("com.good.android.gfe:id/attachment_detail_container"));
            attachmentDetailContainer.click();
            UiObject attachmentDownloadStatus = new UiObject(new UiSelector().resourceId(
                    "com.good.android.gfe:id/attachment_download_status").text("Downloaded"));
            attachmentDownloadStatus.waitForExists(180000);

            // Open the attachment by Good Image Viewer
            attachmentDetailContainer.click();
            UiObject buildinImageViewer = new UiObject(new UiSelector().resourceId("android:id/text1").text(
                    "Built-in image viewer"));
            buildinImageViewer.click();

            UiObject imageExist = new UiObject(new UiSelector().resourceId("android:id/title").textContains("IMG"));
            stepResult[2] = imageExist.exists();

            // Write testing results to file
            Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));

            getUiDevice().pressBack();
            getUiDevice().pressBack();
            getUiDevice().pressBack();

            /**
             * // **********Step4********** // Step4:Repeat steps 1 thru 2.
             * Check the Save Draft button in email compose window and then
             * save the email with the selected attachment and check
             * result 4
             */
            GoodTabScroll.swipeRight(2);
            EmailTab.click();
            ComposeEmail.click();

            // Step2: Click Paperclip icon, select a file to attach. Check
            // result 2.
            // Tap attachment button
            AttachmentButton.click();
            // Try to attach file from attachment management list view
            AddAttachment.click();
            // Tap "File From Documents"
            FileFromDocuments.click();
            // Check attachment from GFR then attach
            MultiCheck.click();
            AttachFile.click();
            // Go back to Email compose view
            ActiionBarHome.click();
            EmailSubjectInput.setText(RandomEmailSubject + "-Step 4");

            // Tap overflow button then tap "Save Draft"
            UiObject overflowButton = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/more"));
            overflowButton.click();

            UiObject saveDraft = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/action_text").text(
                    "Save Draft"));
            saveDraft.click();

            // Tap Drawerbutton then open Drafts folder
            UiObject drawerButton = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/submenu"));
            drawerButton.click();
            UiObject gotoDraft = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/folder_name")
                    .textContains("Drafts"));
            gotoDraft.click();

            // Waiting for max 60s .
            EmailSubjectContent.waitForExists(60000);
            EmailSubjectContent.click();

            // Open the attachment by Good Image Viewer
            AttachmentButton.click();
            attachmentDetailContainer.click();
            buildinImageViewer.click();
            stepResult[3] = imageExist.exists();

            // Write testing results to file
            Tools.doubleLog("@@Step4 result -> " + Tools.getResult(stepResult[3]));

            // Go to Inbox view as initial step
            getUiDevice().pressBack();
            getUiDevice().pressBack();
            getUiDevice().pressBack();
            getUiDevice().pressBack();
            drawerButton.click();
            UiObject gotoInbox = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/folder_name")
                    .textContains("Inbox"));
            gotoInbox.click();

        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        } catch (Exception e) {
            Tools.logInfor(e);
        }

        String ends = date.format(new Date());
        freq.gotoActivity(Activity.Email);
        Tools.doubleLog("@@Smoke ID16 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
        Tools.writeData("16," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
    }
}