package com.good.android.gfetest;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;
import com.good.android.gfetest.Frequency.Activity;
import com.good.android.gfetest.Frequency.CalendarItem;

public class ID24GFEUpgrade extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
	boolean[] stepResult = new boolean[4];
	private int errorCode = 0;
	private final String emailSubject = "SmokeID24 Email Subject";
	private final String eventSub = "SmokeID24 Event Subject";
	private final String eventLoc = "SmokeID24 Event Location";
	private final String eventInv = "Katharine4@asia.qagood.com";
	private final String personName = "Katharine 4";
	private final String phoneNumber = "13800138000";
	private final String emailAddress = "Katharine4@asia.qagood.com";
	private final String taskTitle = "SmokeID24 Task Title";
	private final String appName1 = "Good";
	private final String upgrade = "Up";

	public void testID24() {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testID24()");
		freq.setStepResult(stepResult);
		String provisionResult = null;
		String provisionResultFailed = "ProvisionFailed";
		//String provisionResultSuccessfully = "ProvisionSuccessfully";

		FileReader fr = null;
		BufferedReader br = null;

		try {
			fr = new FileReader("/data/local/tmp/ProvisionResult.txt");
			br = new BufferedReader(fr);
			provisionResult = br.readLine();
			Tools.logInfor("provisionResult=" + provisionResult);
			br.close();
			fr.close();
		} catch (IOException e) {
				Tools.logInfor(e);
		}
		try {
			if (provisionResult.equals(provisionResultFailed)) {
				Tools.logInfor("Upgrade testcase didn't run as the the upgrade from build not Provision successfuly.");
				this.errorCode = 1;
				Tools.logInfor("ErrorCode=1");
			} else {
				UiDevice device = getUiDevice();
				freq.launchGFE(device);
/**
 * Step 1. Upgrade client to latest build;
 */
				Tools.logInfor("Start Step1 ...");

				String ver = freq.getBuildNumber();
				if (!ver.equals(null)) {
					stepResult[0] = true;
					Tools.logInfor("GFE is upgrade to [" + ver + "]");
				} else {
					Tools.logInfor(">>>>>>@Error:Fail to upgrade GFE!!!");
				}
/**
 * Step 2. Perform smoke test to ensure upgrade works as expected and all features work;
 */
				Tools.logInfor("Start Step2 ...");
				freq.gotoActivity(Activity.Email); //Compose an email;
				stepResult[1] = freq.gotoComposeWindow();
				sleep(freq.blink);
				stepResult[1] &= freq.enterMsgRecipent("To", emailAddress);
				sleep(freq.blink);
				stepResult[1] &= freq.enterMsgText("Subject", emailSubject);
				sleep(freq.blink);
				stepResult[1] &= freq.enterMsgText("Body", appName1);
				sleep(freq.blink);
				freq.screenCapture(device);
				stepResult[1] &= freq.sendMsg();
				sleep(freq.blink);

				freq.gotoActivity(Activity.Calendar); //Create an event;
				stepResult[1] &= freq.createEvent(CalendarItem.Subject, eventSub, CalendarItem.Location, eventLoc, CalendarItem.Required, eventInv);
				freq.screenCapture(device);
				sleep(freq.blink);

				freq.gotoActivity(Activity.Contacts); //Create a contact;
				stepResult[1] &= freq.addContact(personName, phoneNumber,
						emailAddress);
				freq.screenCapture(device);
				sleep(freq.blink);

				freq.gotoActivity(Activity.Tasks);
				stepResult[1] &= freq.createTask(taskTitle); //Create a task;
				freq.screenCapture(device);
				sleep(freq.blink);

				freq.gotoActivity(Activity.Documents); //Add photo to file repository;
				stepResult[1] &= freq.addPhotoToDocs();
				freq.screenCapture(device);
				sleep(freq.blink);

/**
 * Step 3. Force stop client;
 */
				Tools.logInfor("Start Step3 ...");
				device.pressHome();
				stepResult[2] = freq.forceStopApp(freq.findAppFromAppManager(appName1));
				sleep(freq.blink);
/**
 * Step 4. Restart client and perform smoke test;
 */
				Tools.logInfor("Start Step4 ...");
				freq.launchGFE(device);
				freq.gotoActivity(Activity.Email); //Compose an email;
				stepResult[3] = freq.gotoComposeWindow();
				sleep(freq.blink);
				stepResult[3] &= freq.enterMsgRecipent("To", emailAddress);
				sleep(freq.blink);
				stepResult[3] &= freq.enterMsgText("Subject", emailSubject
						+ upgrade);
				sleep(freq.blink);
				stepResult[3] &= freq.enterMsgText("Body", appName1 + upgrade);
				freq.screenCapture(device);
				sleep(freq.blink);
				stepResult[3] &= freq.sendMsg();
				sleep(freq.blink);

				freq.gotoActivity(Activity.Calendar); //Create an event;
				stepResult[3] &= freq.createEvent(CalendarItem.Subject, eventSub + upgrade, CalendarItem.Location, eventLoc + upgrade, CalendarItem.Required, eventInv);
				freq.screenCapture(device);
				sleep(freq.blink);

				freq.gotoActivity(Activity.Contacts); //Create a contact;
				stepResult[3] &= freq.addContact(personName + upgrade,
						phoneNumber, emailAddress);
				freq.screenCapture(device);
				sleep(freq.blink);

				freq.gotoActivity(Activity.Tasks);
				stepResult[3] &= freq.createTask(taskTitle + upgrade); //Create a task;
				freq.screenCapture(device);
				sleep(freq.blink);

				freq.gotoActivity(Activity.Documents); //Add photo to file repository;
				stepResult[3] &= freq.addPhotoToDocs();
				freq.screenCapture(device);
				sleep(freq.blink);
				freq.gotoActivity(Activity.Email);

			}
		} catch (Exception e) {
			Tools.logInfor(e);
		} finally {
			String ends = date.format(new Date());
			Tools.doubleLog("@@Step1 result -> -> Need to check currently GFE version...");
			Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
			Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));
			Tools.doubleLog("@@Step4 result -> " + Tools.getResult(stepResult[3]));
			Tools.doubleLog("@@Smoke ID24 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
			Tools.writeData("24," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends  + "," + errorCode);
		}
		
	}
}
