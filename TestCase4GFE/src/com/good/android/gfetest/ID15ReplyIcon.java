package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;
import com.good.android.gfetest.Frequency.Activity;


public class ID15ReplyIcon extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
	private int errorCode = 0;
	boolean[] stepResult = new boolean[8];
	private final String emailSubject = "SmokeID15"; //Prepare an email with multiple recipients;
	private static int emailNum = 0;
	private final String folderName = "SUB1";
	
	public void testID15() {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testID15()");
		freq.setStepResult(stepResult);
		Tools.logInfor("Start Step1-7 ...");
		Tools.logInfor("testID15() -> Go to Inbox");
		checkReplyIcon("Inbox", 1);
		sleep(freq.hearbeat);
		
		Tools.logInfor("testID15() -> Go to Subfolder");
		emailNum = 0;
		Tools.logInfor("Start Step8 ...");
		stepResult[7] = checkReplyIcon(folderName, 5);
		String ends = date.format(new Date());
		freq.gotoActivity(Activity.Email);
		
		Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
		Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
		Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));
		Tools.doubleLog("@@Step4 result -> " + Tools.getResult(stepResult[3]));
		Tools.doubleLog("@@Step5 result -> " + Tools.getResult(stepResult[4]));
		Tools.doubleLog("@@Step6 result -> " + Tools.getResult(stepResult[5]));
		Tools.doubleLog("@@Step7 result -> " + Tools.getResult(stepResult[6]));
		Tools.doubleLog("@@Step8 result -> " + Tools.getResult(stepResult[7]));
		Tools.doubleLog("@@Smoke ID15 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
		Tools.writeData("15," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
System.out.println(errorCode);
	}
	
	public boolean checkReplyIcon(String fName, int count) {
		UiDevice device = getUiDevice();
		freq.deviceInitial(device); //Device initial;
		freq.gotoActivity(Activity.Email);
		try {
			freq.gotoEmailFolder(fName);
			Tools.logInfor(fName + " is selected");
			sleep(freq.blink);
			freq.scrollToBegin(count);
			sleep(3*freq.hearbeat);

		/**
		 * Step 1. Press and hold one of the email;
		 */
			UiObject selectEmail = new UiObject(new UiSelector().
					className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/email_list_item").index(0));
			selectEmail.waitForExists(3*freq.tea);
			UiObject checkbox = selectEmail.getChild(new UiSelector().
					className("android.widget.CheckBox").resourceId("com.good.android.gfe:id/multi_check"));
			checkbox.waitForExists(3*freq.tea);
			String subject = selectEmail.getChild(new UiSelector().
					className("android.widget.TextView").resourceId("com.good.android.gfe:id/subject")).getText();
			if(checkbox.exists() && checkbox.isEnabled()) {
				Tools.logInfor("Tap to select an email");
				checkbox.click(); //Check the first email;
				sleep(freq.blink);
				emailNum++;
				Tools.logInfor("Email [" + subject + "] is checked!");
			} else {
				selectEmail = new UiObject(new UiSelector().
						className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/email_list_item").index(0));
				selectEmail.waitForExists(3*freq.tea);
				checkbox = selectEmail.getChild(new UiSelector().
						className("android.widget.CheckBox").resourceId("com.good.android.gfe:id/multi_check"));
				checkbox.waitForExists(3*freq.tea);
				subject = selectEmail.getChild(new UiSelector().
						className("android.widget.TextView").resourceId("com.good.android.gfe:id/subject")).getText();
				Tools.logInfor("Tap to select an email");
				checkbox.click(); //Check the first email;
				sleep(freq.blink);
				emailNum++;
				Tools.logInfor("Email [" + subject + "] is checked!");
			}
			UiObject selectCounter = new UiObject(new UiSelector().
					className("android.widget.TextView").resourceId("com.good.android.gfe:id/selection_counter_text"));
			selectCounter.waitForExists(3 * freq.tea);
			int selectCN = freq.emailSelectCounter(selectCounter);
//System.out.println("Line95");
			if(selectCN == emailNum) {
				stepResult[0] = true; //Expected result: Email is select. Up-bar is changed. Thick icon is displayed and string 1 selected is displayed.
				Tools.logInfor("There are (" + selectCN + ") emails selected!");
			} else {
				Tools.logInfor("Select (" + selectCN + ") emails"); 
				Tools.logInfor(">>>>>>@Error: Please check the number of email select!"); 
			}
			sleep(freq.blink);
			UiObject flagBtn = new UiObject(new UiSelector().
					className("android.widget.ImageView").resourceId("com.good.android.gfe:id/tick_menu"));
			flagBtn.waitForExists(3*freq.tea);
			UiObject readBtn = new UiObject(new UiSelector().
					className("android.widget.ImageView").resourceId("com.good.android.gfe:id/unread_menu"));
			readBtn.waitForExists(3*freq.tea);
			UiObject moveBtn = new UiObject(new UiSelector().
					className("android.widget.ImageView").resourceId("com.good.android.gfe:id/move_to_folder"));
			moveBtn.waitForExists(3*freq.tea);
			UiObject delBtn = new UiObject(new UiSelector().
					className("android.widget.ImageView").resourceId("com.good.android.gfe:id/delete"));
			delBtn.waitForExists(3*freq.tea);
			UiObject replyBtn = new UiObject(new UiSelector().
					className("android.widget.ImageView").resourceId("com.good.android.gfe:id/reply_button"));
			replyBtn.waitForExists(3*freq.tea);
			if(flagBtn.exists() && readBtn.exists() && moveBtn.exists() && delBtn.exists() && replyBtn.exists()) {
				stepResult[0] &= true; //Expected result: On Bottom bar are available 5 icons: Flag; Read/Unread; Move; Trash; Reply.
				freq.screenCapture(device);
				Tools.logInfor("Tool bar with 5 icons are available");
			} else {
				stepResult[0] = false;
			}
			sleep(freq.blink);
		/**
		 * Step 2. Tap on Reply button;
		 */
			if(replyBtn.exists() && replyBtn.isEnabled()) {
				replyBtn.click();
				Tools.logInfor("Respond dialog is displayed");
				stepResult[1] = true; //Expected result: Box with options "Reply and Forward" is displayed;
				freq.screenCapture(device);
			}
			sleep(freq.blink);
		/**
		 * Step 3. Tap on Cancel button;
		 */
			UiObject respondCxt = new UiObject(new UiSelector().resourceId("android:id/alertTitle").text("Respond"));
			respondCxt.waitForExists(3*freq.tea);
			if(respondCxt.exists()) {
				UiObject cancelBtn = new UiObject(new UiSelector().resourceId("android:id/button2").text("Cancel"));
				cancelBtn.waitForExists(3*freq.tea);
				cancelBtn.click();
				Tools.logInfor("Respond dialog is cancelled");
			}
			sleep(freq.blink);
			if(checkbox.isChecked()) {
				stepResult[2] = true; //Expected result: User back to the inbox. Previously checked email is still marked.
				freq.screenCapture(device);
				checkbox.click(); //Uncheck the first email
				emailNum--;
				Tools.logInfor("Email [" + subject + "] is unchecked!");
			}
			sleep(freq.blink);
		/**
		 * Step 4. Check several other email. At least one email has few recipients;
		 */
			UiScrollable emailList = new UiScrollable(new UiSelector().className("android.widget.ListView"));
			if (!emailList.waitForExists(3*freq.tea)) {
				emailList = new UiScrollable(new UiSelector().className("android.widget.ListView"));
				emailList.waitForExists(3*freq.tea);
			}
			emailList.scrollToBeginning(1);
			UiObject select1 = emailList.getChildByText(new UiSelector().
					className("android.widget.TextView").resourceId("com.good.android.gfe:id/subject"), emailSubject);
			if(!select1.waitForExists(3*freq.tea)) {
				select1 = emailList.getChildByText(new UiSelector().
						className("android.widget.TextView").resourceId("com.good.android.gfe:id/subject"), emailSubject);
				select1.waitForExists(3*freq.tea);
			}
			UiObject checkbox1 = select1.getFromParent(new UiSelector().
					className("android.widget.CheckBox").resourceId("com.good.android.gfe:id/multi_check"));
			if (!checkbox1.waitForExists(3*freq.tea)) {
				checkbox1 = select1.getFromParent(new UiSelector().
						className("android.widget.CheckBox").resourceId("com.good.android.gfe:id/multi_check"));
				checkbox1.waitForExists(3*freq.tea);
			}
			if(checkbox1.exists() && (!checkbox1.isChecked())) {
				checkbox1.click(); //Find and check the email with multiple recipients
				emailNum++;
				Tools.logInfor("Email [" + emailSubject + "] is checked!");
				stepResult[3] = true;
			}
			emailList.scrollForward();
			sleep(freq.blink);
			for(int i=1; i<4; i++) {
				UiObject selectNext = new UiObject(new UiSelector().
						className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/email_list_item").index(i));
				if (!selectNext.waitForExists(3*freq.tea)) {
					selectNext = new UiObject(new UiSelector().
							className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/email_list_item").index(i));
					selectNext.waitForExists(3*freq.tea);
				}
				String nextSub = selectNext.getChild(new UiSelector().
						className("android.widget.TextView").resourceId("com.good.android.gfe:id/subject")).getText();
				UiObject nextBox = selectNext.getChild(new UiSelector().
						className("android.widget.CheckBox").resourceId("com.good.android.gfe:id/multi_check"));
				if (!nextBox.waitForExists(3*freq.tea)) {
					nextBox = selectNext.getChild(new UiSelector().
							className("android.widget.CheckBox").resourceId("com.good.android.gfe:id/multi_check"));
					nextBox.waitForExists(3*freq.tea);
				}
				if(nextSub.equals(emailSubject)) {
					continue;
				} 
				nextBox.click();
				emailNum++;
				Tools.logInfor("Email [" + nextSub + "] is checked!");
				sleep(freq.blink);
			}
			
			selectCounter = new UiObject(new UiSelector().
					className("android.widget.TextView").resourceId("com.good.android.gfe:id/selection_counter_text"));
			if (!selectCounter.waitForExists(3 * freq.tea)) {
				selectCounter = new UiObject(new UiSelector().
						className("android.widget.TextView").resourceId("com.good.android.gfe:id/selection_counter_text"));
				selectCounter.waitForExists(3 * freq.tea);
			}
			selectCN = freq.emailSelectCounter(selectCounter);
//System.out.println("Line222");
			sleep(freq.blink);
			if(selectCN == emailNum) {
				Tools.logInfor("There are (" + selectCN + ") emails selected!");
				stepResult[3] &= true; //Expected result: Emails are select. Up-bar is changed. Thick icon is displayed and string "N selected" is displayed.
			} else {
				Tools.logInfor("Select (" + selectCN + ") emails"); 
				Tools.logInfor(">>>>>>@Error: Please check the number of email select!");
			}
			if(flagBtn.exists() && readBtn.exists() && moveBtn.exists() && delBtn.exists() && (!replyBtn.exists())) {
				stepResult[3] &= true; //Expected result: On Bottom bar are available 4 icons: Flag; Read/Unread; Move; Trash
				freq.screenCapture(device);
				Tools.logInfor("Tool bar with 4 icons displayed, Reply button is not available");
			} else {
				stepResult[3] = false;
			}
			sleep(freq.blink);
		/**
		 * Step 5. Uncheck emails. Leave only one message which has several recipients;
		 */
			for(int i=1; i<4; i++) {
				UiObject selectNext = new UiObject(new UiSelector().
						className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/email_list_item").index(i));
				if (!selectNext.waitForExists(3 * freq.tea)) {
					selectNext = new UiObject(new UiSelector().
							className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/email_list_item").index(i));
					selectNext.waitForExists(3 * freq.tea);
				}
				String nextSub = selectNext.getChild(new UiSelector().
						className("android.widget.TextView").resourceId("com.good.android.gfe:id/subject")).getText();
				UiObject nextBox = selectNext.getChild(new UiSelector().
						className("android.widget.CheckBox").resourceId("com.good.android.gfe:id/multi_check"));
				if (!nextBox.waitForExists(3 * freq.tea)) {
					nextBox = selectNext.getChild(new UiSelector().
							className("android.widget.CheckBox").resourceId("com.good.android.gfe:id/multi_check"));
					nextBox.waitForExists(3 * freq.tea);
				}
				if(nextBox.isChecked()) {
					if(nextSub.equals(emailSubject)) {
						continue;
					} 
					nextBox.click();
					emailNum--;
					Tools.logInfor("Email [" + nextSub + "] is unchecked!");
					stepResult[4] = true;
				}
				sleep(freq.blink);
			}
			
			selectCounter = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/selection_counter_text"));
			if(!selectCounter.waitForExists(3 * freq.tea)) {
				selectCounter = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/selection_counter_text"));
				selectCounter.waitForExists(3 * freq.tea);
			}
			selectCN = freq.emailSelectCounter(selectCounter);
//System.out.println("Line279");
			if(selectCN == 1) {
				Tools.logInfor("There are (" + selectCN + ") emails selected!");
			} else {
				Tools.logInfor("Select (" + selectCN + ") emails"); 
				Tools.logInfor(">>>>>>@Error: Please check the number of email select!");
			}
			sleep(freq.blink);
			if(flagBtn.exists() && readBtn.exists() && moveBtn.exists() && delBtn.exists() && replyBtn.exists()) {
				stepResult[4] &= true; //Expected result: On Bottom bar are available 5 icons: Flag; Read/Unread; Move; Trash; Reply.
				freq.screenCapture(device);
				Tools.logInfor("Tool bar with 5 icons are available");
			} else {
				stepResult[4] = false;
			}
			sleep(freq.blink);
		/**
		 * Step 6. Tap on Reply icon;
		 */
			if(replyBtn.exists() && replyBtn.isEnabled()) {
				replyBtn.click();
				Tools.logInfor("Respond dialog is displayed");
				sleep(freq.blink);
				UiObject reply = new UiObject(new UiSelector().resourceId("android:id/text1").text("Reply"));
				if (!reply.waitForExists(3 * freq.tea)) {
					reply = new UiObject(new UiSelector().resourceId("android:id/text1").text("Reply"));
					reply.waitForExists(3 * freq.tea);
				}
				UiObject replyAll = new UiObject(new UiSelector().resourceId("android:id/text1").text("Reply all"));
				if (!replyAll.waitForExists(3 * freq.tea)) {
					replyAll = new UiObject(new UiSelector().resourceId("android:id/text1").text("Reply all"));
					replyAll.waitForExists(3 * freq.tea);
				}
				UiObject forward = new UiObject(new UiSelector().resourceId("android:id/text1").text("Forward"));
				if (!forward.waitForExists(3 * freq.tea)) {
					forward = new UiObject(new UiSelector().resourceId("android:id/text1").text("Forward"));
					forward.waitForExists(3 * freq.tea);
				}
				if(reply.exists() && replyAll.exists() && forward.exists()) {
					stepResult[5] = true; //Expected result: Box with options: "Reply, Reply All and Forward" is displayed.
					freq.screenCapture(device);
					Tools.logInfor("Reply, Reply All and Forward buttons are displayed."); 
				}
			}
			sleep(freq.hearbeat);
		/**
		 * Step 7. Tap back button;
		 */
			UiObject cancelBtn = new UiObject(new UiSelector().resourceId("android:id/button2").text("Cancel"));
			if (cancelBtn.waitForExists(3 * freq.tea)) {
				cancelBtn.clickAndWaitForNewWindow();
				Tools.logInfor("Respond dialog is cancelled");
			}
			//if(device.pressBack()) {
				//Tools.logInfor("Respond dialog is cancelled");
			//}
			sleep(freq.blink);
			selectCounter = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/selection_counter_text"));
			if (!selectCounter.waitForExists(3 * freq.tea)) {
				selectCounter = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/selection_counter_text"));
				selectCounter.waitForExists(3 * freq.tea);
			}
			selectCN = freq.emailSelectCounter(selectCounter);
System.out.println("Line337");
			if ((freq.errorCode!=0) && (errorCode == 0)) {
				errorCode = freq.errorCode;
				Tools.logInfor("ErrorCode=" + this.errorCode);
			}
			sleep(freq.blink);
			if(selectCN == emailNum) {
				stepResult[6] = true; //Expected result: User back to the inbox. Previously checked email is still marked.
				freq.screenCapture(device);
				Tools.logInfor("There are (" + selectCN + ") emails selected!");
			} else {
				Tools.logInfor("Select (" + selectCN + ") emails"); 
				Tools.logInfor(">>>>>>@Error: Please check the number of email select!");
			}
			sleep(freq.hearbeat);
		/**
		 * Step 8. Repeat above steps in subscribed subfolder;
		 */
			device.pressBack(); //Back to the start status;
			sleep(freq.blink);
			freq.scrollToBegin(10);
			return true;	
		} catch(UiObjectNotFoundException e) {
			if (errorCode == 0) {
				errorCode = 400;
				Tools.logInfor("ErrorCode=400");
			}
			Tools.logInfor(e);
			return false;
		} catch(Exception e) {
			Tools.logInfor(e);
			return false;
		} finally {
			if (errorCode == 0) {
				errorCode = freq.errorCode;
				Tools.logInfor("ErrorCode=" + errorCode);
			}
		}
	}
}

