package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;
import com.good.android.gfetest.Frequency.Activity;
import com.good.android.gfetest.Frequency.Respond;
import com.good.android.gfetest.Frequency.RespondMsg;

//Please send a new meeting request which subject equals "SmokeID01";
public class ID01AcceptSingleMR extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
	boolean[] stepResult = new boolean[2];
	private static String meetingSubject = "S01";
	private int errorCode = 0;
	//private static final String pre = "Invitation: ";
	private static final String pre = "";
	String starts, ends;
	
	public void testID01() {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testID01()");
		UiDevice device = getUiDevice();
		freq.setStepResult(stepResult);
		freq.launchGFE(device);
		freq.gotoActivity(Activity.Email); //Go to Email tab;
		sleep(freq.breath);
		try {
			String ts = Tools.readData();
			meetingSubject += ts;
/**
 * Step 1. On the organizer's Outlook create and invite a GMM user to a single meeting;
 * Expected Result 1. The GMM user receives the meeting request;
 */	
			Tools.logInfor("Start Step1 ...");
			this.stepResult[0] = freq.openMsg(pre + meetingSubject); //Open the meeting request in email list view;
			errorCode = freq.errorCode;
			freq.screenCapture(device);
			sleep(freq.breath);
/**
 * Step 2. GMM user accepts the meeting request;
 * Expected Result 2. Response sent to organizer, and all the calendars are updated with the scheduled meeting;
 */
			if(this.stepResult[0]) {
				Tools.logInfor("Start Step2 ...");
				String month = freq.getDate("Month").toLowerCase(); //Get the month of the meeting;
				sleep(freq.breath);
				String day = freq.getDate("Day").toLowerCase();
				sleep(freq.breath);
				Tools.logInfor("Date: " + day + ", " + month);
				freq.handleMeetingRequest(Respond.Accept, RespondMsg.DefaultMsg); //Tap on Accept button in email detail view, then send default message;
				sleep(2 * freq.tea);
				freq.gotoActivity(Activity.Calendar);
				sleep(2 * freq.tea);
				freq.backToToday(); //Back to Today;
				sleep(3 * freq.tea);
				this.stepResult[1] = freq.findEventInCalendar(meetingSubject, device); //Need to change according to the backend;
				if (errorCode == 0) {
					errorCode = freq.errorCode;
					Tools.logInfor("ErrorCode=" + this.errorCode);
				}
				sleep(freq.breath);
				freq.screenCapture(device);
				freq.gotoActivity(Activity.Email);
			}			
		} catch(Exception e) {
			if (errorCode == 0) {
				errorCode = 400;
				Tools.logInfor("ErrorCode=400");
			}
			Tools.logInfor(e);
		} finally {
			ends = date.format(new Date());
			Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
			Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
			Tools.doubleLog("@@Smoke ID01 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
			Tools.writeData("01," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
System.out.println(errorCode);
		}
	}
}
