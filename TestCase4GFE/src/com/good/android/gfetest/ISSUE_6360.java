package com.good.android.gfetest;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;
import com.good.android.gfetest.Frequency.Activity;

public class ISSUE_6360 extends UiAutomatorTestCase {
	String msgSubject = "[JIRA] Subscription: GFE Android Bug Triage";
	int num = 4;
	Frequency freq = new Frequency();
	
	public void test6360() {
		try {
			UiDevice device = getUiDevice();
			freq.launchGFE(device);
			freq.gotoActivity(Activity.Email);
			while(num > 0) {
				int i = 5 - num;
				if(freq.isMsgExists(msgSubject)) {
					--num;
					Tools.logInfor("@The [" + i + "] time, find and open the specified email");
					sleep(freq.breath);
					freq.backToEmailList();
					sleep(freq.breath);
					Tools.logInfor("Back to Email List View");
				} else {
					Tools.logInfor("The [" + i + "] time, unable to find the specified email ---> Break!!!");
					break;
				}
			}
		} catch(UiObjectNotFoundException e) {
			Tools.logInfor(e);
		}
	}
}
