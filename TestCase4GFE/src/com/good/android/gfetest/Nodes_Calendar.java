package com.good.android.gfetest;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;

public class Nodes_Calendar extends UiAutomatorTestCase {
/* Nodes in Day View */
	/**
	 * Returns a scrollable UI container for Calendar Day view.
	 * @return A scrollable UI container for Calendar Day view. 
	 */
	public UiScrollable getEventDayView() {
		UiScrollable dayView = new UiScrollable(new UiSelector().resourceId("com.good.android.gfe:id/switcher"));
		return dayView;
	}
	
	/**
	 * Returns a UI element for DayNumber at the top left corner in Calendar Day view.
	 * @return A UI element for DayNumber in Calendar Day view.
	 */
	public UiObject getDayNumberAtTopLeftCorner() {
		UiObject dayNum = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/dayNumber"));
		return dayNum;
	}

	
/* Nodes in Week View */
	/**
	 * Returns a UI element for WeekNumber at the top left corner in Calendar Week view.
	 * @return A UI element for WeekNumber in Calendar Week view.
	 */
	public UiObject getWeekNumberAtTopLeftCorner() {
		UiObject weekNum = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/date2nd"));
		return weekNum;
	}

	
/* Nodes in Month View */
	/**
	 * Returns a scrollable UI container for Calendar Month view.
	 * @return A scrollable UI container for Calendar Month view. 
	 */
	public UiScrollable getEventListInMonthView() {
		UiScrollable list = new UiScrollable(new UiSelector().className("android.widget.ListView").scrollable(true));
		return list;
	}
	
	/**
	 * Returns a UI element for the number of Month and Year at the top left corner in Calendar Week view.
	 * @return A UI element for the number of Month and Year in Calendar Week view.
	 */
	public UiObject getMonthYearAtTopLeftCorner() {
		UiObject monthYear = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/date1st"));
		return monthYear;
	}

	
/* Nodes in CLDATT View */
	/**
	 * Returns a UI element for Back_To_Message_Button at the top left corner in Calendar Attachments view.
	 * @return A UI element for Back_To_Message_Button at the top left corner in Calendar Attachments view.
	 */
	public UiObject getBackToEventBtn() {
		UiObject backBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId(
                "com.good.android.gfe:id/back_to_message_button"));
		return backBtn;
	}
	
	
/* Nodes in Event Detail View */
	/**
	 * Returns a UI element for Event Subject in Calendar Detail view.
	 * @return A UI element for Event Subject in Calendar Detail view.
	 */
	public UiObject getEventTitle(String title) {
		UiObject eventTitle = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/subject").text(title));
		return eventTitle;
	}
	
	/**
	 * Returns a UI element for Event/Occurrence Starts and Ends Time in Calendar Detail view.
	 * @return A UI element for Event/Occurrence Starts and Ends Time in Calendar Detail view.
	 */
	public UiObject getEventStartAndEndTime() {
		UiObject time = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/date"));
		return time;
	}
	
	/**
	 * Returns a UI element for Respond Button at the bottom bar in Calendar Detail view (As Invitee).
	 * @return A UI element for Respond Button in Calendar Detail view.
	 */
	public UiObject getActionBtnInCalendarDetailView() {
		UiObject actionBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/respond"));
		return actionBtn;
	}
	
	/**
	 * Returns a UI element for Edit Button at the bottom bar in Calendar Detail view (As Organizer).
	 * @return A UI element for Edit Button in Calendar Detail view.
	 */
	public UiObject getEditBtnInCalendarDetailView() {
		UiObject editBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/edit"));
		return editBtn;
	}
	
	/**
	 * Returns a UI element for Delete Button at the bottom bar in Calendar Detail view.
	 * @return A UI element for Delete Button in Calendar Detail view.
	 */
	public UiObject getDeleteBtnInCalendarDetailView() {
		UiObject delBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/delete"));
		return delBtn;
	}
	
	/**
	 * Returns a UI element for Calendar_Attachments_Button in Calendar Detail view.
	 * @return A UI element for Calendar_Attachments_Button in Calendar Detail view.
	 */
	public UiObject getPaperClipIconInEventView() {
		UiObject clip = new UiObject(new UiSelector().
				className("android.widget.Button").resourceId("com.good.android.gfe:id/calendar_attachments_button"));
		return clip;
	}
	
	/**
	 * Returns a UI element for Recurpattern of a recurring event in Calendar Detail view.
	 * @return A UI element for Recurpattern of a recurring event.
	 */
	public UiObject getRecurPattern() {
		UiObject recPat = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/recurpattern"));
		return recPat;
	}
	
	/**
	 * Returns a UI element for Reply/Forward Button at the bottom bar in Calendar Detail view.
	 * @return A UI element for Reply/Forward Button in Calendar Detail view.
	 */
	public UiObject getRespondBtnInCalendarDetailView() {
		UiObject respondBtn = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/reply"));
		return respondBtn;
	}
	
	/**
	 * Returns a UI element for Show As status in Calendar Detail view.
	 * @return A UI element for Show As status in Calendar Detail view.
	 */
	public UiObject getShowAsStatus() {
		UiObject showAs = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/show_as_text"));
		return showAs;
	}
	
	
/* Nodes in "Edit Event" view (Open any Event in Organizer's device -> Click Edit button) */
	/**
	 * Returns a UI element for Hour Increment button in Time and Date field (As Organizer).
	 * @return A UI element for Hour Increment button in Time and Date field.
	 */
	public UiObject getIncrementBtn() {
		UiObject incrementBtn = new UiObject(new UiSelector().className("android.widget.ImageButton").resourceId("com.good.android.gfe:id/increment"));
		return incrementBtn;
	}
	
	/**
	 * Returns a UI element for Done button in in Time and Date field (As Organizer).
	 * @return A UI element for Done button in in Time and Date field.
	 */
	public UiObject getDoneBtn() {
		UiObject doneBtn = new UiObject(new UiSelector().className("android.widget.Button").resourceId("com.good.android.gfe:id/done"));
		return doneBtn;
	}
	
	
/* Nodes in "Edit Recurring Event" dialog (Open any Event in Organizer's device -> Click Edit button) */
	/**
	 * Returns a UI element for the title of Edit Recurring Event dialog (As Organizer).
	 * @return A UI element for the title of Edit Recurring Event dialog.
	 */
	public UiObject getEditRecurringEventTitle() {
		UiObject titleObj = new UiObject(new UiSelector().resourceId("android:id/alertTitle").text("Edit Recurring Event"));
		return titleObj;
	}
	
	/**
	 * Returns a UI element for the Message of Edit Recurring Event dialog (As Organizer).
	 * @return A UI element for the Message of Edit Recurring Event dialog.
	 */
	public UiObject getEditRecurringEventMessage() {
		UiObject msgObj = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/message").text("This is a recurring event. You can:"));
		return msgObj;
	}
	
	/**
	 * Returns a UI element for the Option of Edit Recurring Event dialog (As Organizer).
	 * @param type String "occurrence" or "series", cannot be empty.
	 * @return A UI element for the Option of Edit Recurring Event dialog.
	 */
	public UiObject getEditRecurringEventOption(String type) {
		String text = null;
		if(type.toLowerCase().equals("occurrence")) {
			text = "Edit this occurrence";
		} else if(type.toLowerCase().equals("series")) {
			text = "Edit the series";
		}
		UiObject eventTypeObj = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/buttonLabel").text(text));
		return eventTypeObj;
	}
	
	
/* Nodes in "Edit Event Occurrence" view (Open any Event in Organizer's device -> Click Edit button -> Click Edit this occurrence option) */
	/**
	 * Returns a UI element for the title of Edit Event Occurrence view (As Organizer).
	 * @return A UI element for the title of Edit Event Occurrence view.
	 */
	public UiObject getEditEventOccurrenceTitle() {
		UiObject title = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/title").text("Edit Event Occurrence"));
		return title;
	}
	
	/**
	 * Returns a UI element for text of Event Starts or Ends DateTime in Edit Event Occurrence view (As Organizer).
	 * @param dateString String "starts" or "ends", cannot be empty.
	 * @return A UI element for text of Event Starts or Ends DateTime.
	 */
	public UiObject getTimeAndDateOption(String dateString) {
		UiObject timeDateObj = null; 
		String resourceId = null;
		if(dateString.toLowerCase().equals("starts")) {
			resourceId = "com.good.android.gfe:id/start_date";
		} else if(dateString.toLowerCase().equals("ends")) {
			resourceId = "com.good.android.gfe:id/end_date";
		}
		timeDateObj = new UiObject(new UiSelector().className("android.widget.TextView").resourceId(resourceId));
		return timeDateObj;
	}
	
	/**
	 * Returns a UI element for Save or Discard Button in Edit Event Occurrence view (As Organizer).
	 * @param btn String "save" or "discard", cannot be empty.
	 * @return A UI element for Save or Discard Button.
	 */
	public UiObject getBtnInEditEventView(String btn) {
		UiObject btnObj = null;
		if(!(btn.toLowerCase().equals("save") || btn.toLowerCase().equals("discard"))) {
			return btnObj;
		} else if(btn.toLowerCase().equals("save")) {
			btnObj = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/save").text("Save"));
		} else if(btn.toLowerCase().equals("discard")) {
			btnObj = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/discard").text("Discard"));
		} 
		return btnObj;
	}
	

/* Nodes in "Respond to Event" dialog ((Open any Event in Invitee's device -> Click Respond to Event button) */
	/**
	 * Returns a UI element for the title of Respond to Event in Respond to Event dialog (As Invitee).
	 * @return A UI element for the title of Respond to Event in Respond to Event dialog.
	 */
	public UiObject getRespondtoEventText() {
		UiObject rteText = new UiObject(new UiSelector().resourceId("android:id/alertTitle").text("Respond to Event"));
		return rteText;
	}
	
	/**
	 * Returns a UI element for "Accept", "Tentative" or "Decline" button in Respond to Event dialog (As Invitee).
	 * @param respond String "Accept", "Tentative" or "Decline", cannot be empty.
	 * @return A UI element for "Accept", "Tentative" or "Decline" button in Respond to Event dialog.
	 */
	public UiObject getActionBtn(String respond) {
		UiObject actionBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/buttonLabel").text(respond));
		return actionBtn;
	}
	
	
/* Nodes in "Meeting Accepted" dialog (Open any Event in Invitee's device -> Click Respond to Event button -> Click Accept option)*/
	/**
	 * Returns a UI element for alertTitle "Meeting Accepted" in Meeting Accepted dialog (As Invitee).
	 * @return A UI element for alertTitle "Meeting Accepted" in Meeting Accepted dialog.
	 */
	public UiObject getMeetingAcceptedText() {
		UiObject maText = new UiObject(new UiSelector().resourceId("android:id/alertTitle").text("Meeting Accepted"));
		return maText;
	}
	
	/**
	 * Returns a UI element for option "Send response now", "Respond with comment" or "No response" in Meeting Accepted dialog (As Invitee).
	 * @param option String "Send response now", "Respond with comment" or "No response", cannot be empty.
	 * @return A UI element for option "Send response now", "Respond with comment" or "No response" in Meeting Accepted dialog.
	 */
	public UiObject getMeetingAcceptedOption(String option) {
		UiObject optionObj = new UiObject(new UiSelector().resourceId("android:id/text1").text(option));
		return optionObj;
	}
}
