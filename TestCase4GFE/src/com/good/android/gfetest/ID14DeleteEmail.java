package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.core.*;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.good.android.gfetest.Frequency.Activity;

public class ID14DeleteEmail extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
	private int errorCode = 0;
	boolean[] stepResult = new boolean[4];
	private final String deleteFolder = "Deleted";
	//private final String deleteFolder = "Trash";
	
	public void testID14() {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testID14()");
		freq.setStepResult(stepResult);
		UiDevice device = getUiDevice();
		freq.launchGFE(device);
		freq.gotoActivity(Activity.Email);
		sleep(freq.breath);
/**
 * Step1. Select several emails.
 */		
		Tools.logInfor("Start Step1 ...");
		try {
			freq.gotoEmailFolder("Inbox");
			sleep(freq.blink);
			
			String[] email = new String[3];
			for(int i=1; i<4; i++) {
				UiObject selectNext = new UiObject(new UiSelector().
						className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/email_list_item").index(i));
				selectNext.waitForExists(freq.hearbeat);
				email[i-1] = selectNext.getChild(new UiSelector().
						className("android.widget.TextView").resourceId("com.good.android.gfe:id/subject")).getText().trim();
				UiObject nextBox = selectNext.getChild(new UiSelector().
						className("android.widget.CheckBox").resourceId("com.good.android.gfe:id/multi_check"));
				nextBox.waitForExists(freq.hearbeat);
				nextBox.click();
				Tools.logInfor("Email [" + email[i-1] + "] is checked!");
				sleep(freq.blink);
			}
			freq.screenCapture(device);
			this.stepResult[0] = freq.isToolBarAvailable(email.length);
			UiObject selectNum = new UiObject(new UiSelector().
					className("android.widget.TextView").resourceId("com.good.android.gfe:id/selection_counter_text")); 
			selectNum.waitForExists(freq.hearbeat);
			if(selectNum.exists()) {
				if(selectNum.getText().trim().equals("3 selected")) {
					this.stepResult[0] &= true; //Expected result: string 3 selected is displayed;
					Tools.logInfor("Text [" + email.length + " selected] displayed");
				} else {
					this.stepResult[0] = false;
				}
			} else {
				this.stepResult[0] = false;
			}
/**
 * Step2. Choose delete icon.
 * Step3. Choose Cancel.
 */
			Tools.logInfor("Start Step2 ...");
			UiObject delBtn = new UiObject(new UiSelector().
					className("android.widget.ImageView").resourceId("com.good.android.gfe:id/delete"));
			delBtn.waitForExists(freq.hearbeat);
			if(delBtn.exists()) {
				delBtn.clickAndWaitForNewWindow(); 
				Tools.logInfor("Tap on Delete button");
				sleep(freq.blink);
				UiObject delMsg = new UiObject(new UiSelector().resourceId("android:id/alertTitle").text("Delete Emails"));
				delMsg.waitForExists(freq.hearbeat);
				Tools.logInfor("Start Step3 ...");
				UiObject cancelBtn = new UiObject(new UiSelector().resourceId("android:id/button2").text("Cancel"));
				cancelBtn.waitForExists(freq.hearbeat);
				if(delMsg.exists() && cancelBtn.exists()) {
					cancelBtn.clickAndWaitForNewWindow();
					Tools.logInfor("Tap to cancel delete emails");
					this.stepResult[1] = true;
				}
			}
			sleep(freq.blink);
/**
 * Step4. Choose delete icon and choose Ok on next screen.			
 */
			Tools.logInfor("Start Step4 ...");
			if(delBtn.exists()) {
				this.stepResult[1] = true;
				delBtn.clickAndWaitForNewWindow(); 
				Tools.logInfor("Tap on Delete button");
				sleep(freq.blink);
				UiObject delMsg = new UiObject(new UiSelector().resourceId("android:id/alertTitle").text("Delete Emails"));
				delMsg.waitForExists(freq.hearbeat);
				if(delMsg.exists()) {
					freq.tapButton("OK");
					this.stepResult[1] &= true;
					Tools.logInfor("Tap OK to delete emails");
					sleep(5*freq.hearbeat);
				}
			}
			this.stepResult[2] = true;
			for(int i=0; i<3; i++) {
				int j = i+1;
				Tools.logInfor("Scroll the screen to find the [" + j + "] deleted email in Inbox ->");
				this.stepResult[2] &= !freq.isMsgExists(email[i]);
				if(this.stepResult[2]) {
					Tools.logInfor("Found!!!");
				} else {
					Tools.logInfor("Fail to found");
				}
				sleep(freq.blink);
			}
/**
 * Step5. Go to Delete items.
 */
			Tools.logInfor("Start Step5 ...");
			this.stepResult[3] = true;
			for(int i=0; i<3; i++) {
				int j = i+1;
				Tools.logInfor("Scroll the screen to find the [" + j + "] deleted email in Deleted folder ->");
				this.stepResult[3] &= freq.isMsgExists(email[i], deleteFolder);
				if(this.stepResult[3]) {
					Tools.logInfor("Found!!!");
				} else {
					Tools.logInfor("Fail to found");
				}
				sleep(freq.breath);
			}
 		} catch(UiObjectNotFoundException e) {
 			if (errorCode == 0) {
				errorCode = 400;
				Tools.logInfor("ErrorCode=400");
			}
 			Tools.logInfor(e);
 		} catch(Exception e) {
			Tools.logInfor(e);
		} finally {
			String ends = date.format(new Date());
			freq.gotoActivity(Activity.Email);
			Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
			Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
			Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));
			Tools.doubleLog("@@Step4 result -> " + Tools.getResult(stepResult[3]));
			Tools.doubleLog("@@Step5 result -> Need to check this step on OWA...");
			Tools.doubleLog("@@Smoke ID14 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
			Tools.writeData("14," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
		}
	}
}
