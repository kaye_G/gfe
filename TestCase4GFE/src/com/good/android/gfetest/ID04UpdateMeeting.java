package com.good.android.gfetest;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.good.android.gfetest.Frequency.*;


public class ID04UpdateMeeting extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
	String starts, ends;
	boolean[] stepResult = new boolean[3];
	private final String pre = "Reschedule:";
	private String eventSub = "S04";
	private int errorCode = 0;
	
	public void testID04() {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testID04()");
		UiDevice device = getUiDevice();
		freq.setStepResult(stepResult);
		try {
			String ts = Tools.readData();
			eventSub += ts;
		} catch (IOException e) {
			Tools.logInfor(e);
		}
		freq.launchGFE(device);
		freq.gotoActivity(Activity.Email); //Go to Email tab;
		sleep(2 * freq.breath);
/**
 * Step 1. On the organizer's Outlook, change a recurring meeting from "Daily", "Every 1 day", ends after 4 days, To "Daily", "Every Weekday", ends after 4 days, send the update;
 */
		try {
			Tools.logInfor("Start Step1 ...");
			this.stepResult[0] = freq.openMsg(pre + eventSub); //Expected Result: GFE invitee receives the update;
			errorCode = freq.errorCode;
			freq.screenCapture(device);
			sleep(freq.breath);
/**		
 * Step 2. On the invitee's GFE, accept the update;
 */
			if(!this.stepResult[0]) {
				freq.screenCapture(device);
				Tools.logInfor("Fail to found event [" + pre + eventSub + "]");
			} else {
				freq.screenCapture(device);
				Tools.logInfor("Found meeting request [" + pre + eventSub + "]");
				Tools.logInfor("Start Step2 ...");
				freq.handleMeetingRequest(Respond.Accept, RespondMsg.DefaultMsg);
				this.stepResult[1] = true;
/**
 * step 3. Open invitee's Outlook and GFE calendars.		
 */
				freq.gotoActivity(Activity.Calendar);
				sleep(freq.breath);
				String view = freq.getCurrentCalendarView();
				if(!view.equals("month")) {
					freq.switchToCalenderMode(CalendarMode.Month);
				}
				freq.backToToday(); //Back to Today;
				sleep(5*freq.hearbeat);
				this.stepResult[2] = freq.findEventInCalendar(pre + eventSub, device);
				if (errorCode == 0) {
					errorCode = freq.errorCode;
					Tools.logInfor("ErrorCode=" + this.errorCode);
				}
				freq.screenCapture(device);
				sleep(freq.breath);
				freq.openEventInCalendarMonthView(pre + eventSub);
				freq.screenCapture(device);
				sleep(freq.breath);
				boolean flag = freq.isRecurringEventOccursOnWorkDay();
				if(flag) {
					Tools.logInfor("The recurring meeting has been updated!!!");
				} else {
					if (errorCode == 0) {
						errorCode = 1;
						Tools.logInfor("ErrorCode=1");
					}
					Tools.logInfor("Error: The recurring meeting HAS NOT been updated!!!");
				}
				freq.screenCapture(device);
				this.stepResult[2] &= flag;
			}
		} catch(UiObjectNotFoundException e) {
			if (errorCode == 0) {
				errorCode = 400;
				Tools.logInfor("ErrorCode=400");
			}
			Tools.logInfor(e);
		} catch(Exception e) {
			Tools.logInfor(e);
		} finally {
			freq.gotoActivity(Activity.Email);
			ends = date.format(new Date());
			Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
			Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
			Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));
			Tools.doubleLog("@@Smoke ID04 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
			Tools.writeData("04," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
System.out.println(errorCode);
		}
	}
}
