package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiSelector;

public class ID29CopyPasteSamsungS5Note4 extends Frequency {

    public void testID29CopyPaste() throws UiObjectNotFoundException {
        boolean[] stepResult = new boolean[]{false,false};
        Frequency freq = new Frequency();

        Tools.doubleLog("==========================================================================================");
        Tools.doubleLog("Start to execute method testID29()");
        /**
         * // **********Precondition********** // Set following the option as
         * desired in the policy of Messaging on the GMC: 1. Do not allow data
         * to be copied from the Good application -->UnChecked 2. Do not allow
         * data to be copied into the Good application --->Checked Notes: If the
         * device has OS < 3 honeycomb and copy into or out of GFE is disallowed
         * by policy, then copy/paste is disallowed within Good for webview
         * 
         */

        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String starts = date.format(new Date());
        try {
            UiDevice device = getUiDevice();
            freq.launchGFE(device);
            gotoActivity(Activity.Email);

            // Copy text in Clipboard inside GFE
            freq.gotoComposeWindow();
            // PublicVoidBen.composeEmail();
            sleep(1000);
            UiObject EmailSubjectInput = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/subject"));
            EmailSubjectInput.setText("GOODLUCK INTERNAL");
            sleep(1000);
            UiObject Emailbcc = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/bcc"));
            Emailbcc.click();
            sleep(1000);
            EmailSubjectInput.click();
            EmailSubjectInput.longClick();
            UiObject CopyButton = new UiObject(new UiSelector().resourceId("android:id/copy"));
            CopyButton.click();
            UiObject backtolist = new UiObject(
                    new UiSelector().resourceId("com.good.android.gfe:id/back_to_list_button"));
            backtolist.click();

            /**
             * // **********Step1********** // Step1. Copy some text from other
             * app. Please install Clipper and Chrome app. Must clean up
             * clipboard by Clipper app
             * 
             */
            launchAppCalled("Clipper");
            freq.clearclipboard();

            // Launch Chrome then input text and copy
            launchAppCalled("Chrome");
            UiObject url = new UiObject(new UiSelector().resourceId("com.android.chrome:id/url_bar"));
            url.click();
            url.clearTextField();
            url.setText("EXTERNALINPUT");

            // Long press to copy text
            url.longClickTopLeft();
            stepResult[0] = CopyButton.click();

            // PublicVoidBen.log("    Step1 PASS  \n");

            Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));

            /**
             * // **********Step2********** // Step2. Launch Good client and
             * compose one email, then long click the email body. Launch GFE by
             * recent button.
             * 
             */
            launchAppCalled("Good");

            // Compose an email then long press Subject field
            freq.gotoComposeWindow();

            EmailSubjectInput.click();
            EmailSubjectInput.longClick();

            // Tap paste in pop up dialog
            UiObject Paste = new UiObject(new UiSelector().resourceId("android:id/title").text("Paste"));
            Paste.click();

            // Compare if External copied text can be paste in GFE
            String unexpectedMessage = "EXTERNALINPUT";

            if (unexpectedMessage.equals(EmailSubjectInput.getText()) == true) {
                stepResult[1] = false;
                Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));

            } else {
                // PublicVoidBen.log("    Step2 PASS  \n");
                stepResult[1] = true;
                Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));

            }
            getUiDevice().pressBack();
            getUiDevice().pressBack();

        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        } catch (Exception e) {
            Tools.logInfor(e);
        }

        String ends = date.format(new Date());
        Tools.doubleLog("@@Smoke ID29 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
        Tools.writeData("29," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends);
    }

}
