package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.core.*;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.good.android.gfetest.Frequency.Activity;

public class VAT_001 extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
	boolean[] stepResult = new boolean[1];
	private final String emailSubject = "VATID001";
	String starts, ends;
	
	public void testVAT001() {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testVAT001()");
/**
 * Step1. From desktop mail client go to Inbox and mark an email with Flag.	
 * Step2. Waiting for the update sync to device.	
 */
		freq.setStepResult(stepResult);
		UiDevice device = getUiDevice();
		freq.launchGFE(device);
		freq.gotoActivity(Activity.Email); //Go to Email tab;
		sleep(freq.breath);
		try {
			if (freq.isFlagIconExists(emailSubject)) { //Expected Result: Verify the that Flag status syncs to device inbox for the corresponding email.
				freq.screenCapture(device);
				Tools.logInfor("Flag icon -> Found!");
				if (freq.tapBtnInEmailDetailView("Flag")) {
					sleep(freq.hearbeat);
					this.stepResult[0] = freq.isFlagMenuExists();
					freq.screenCapture(device);
				}
			} else {
				Tools.logInfor("@Error: Fail to found the Flag icon for email [" + emailSubject + "]!!!");
				this.stepResult[0] =  false;
				freq.screenCapture(device);
			}
		} catch (UiObjectNotFoundException e) {
			Tools.logInfor(e);
		}
		ends = date.format(new Date());
		Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
		Tools.doubleLog("@@VAT_001 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
		Tools.writeData("VAT_001," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends);
	}
}
