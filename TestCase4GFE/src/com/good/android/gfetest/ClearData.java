/**
 * The class is suit for Samsung device, e.g. Samsung Note 4;
 */

package com.good.android.gfetest;
import java.util.Date;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;

public class ClearData extends UiAutomatorTestCase {
	public void testClearData() throws UiObjectNotFoundException {
		UiDevice device = getUiDevice();
		Tools.logInfor("==========================================================================================");
		Tools.logInfor("Start to execute method testClearData()");
		device.pressHome();
		//Find Apps button
		UiObject apps = new UiObject(new UiSelector().resourceId("com.sec.android.app.launcher:id/home_allAppsIcon").text("Apps"));
		if(apps.exists() && apps.isEnabled()) {
			apps.clickAndWaitForNewWindow();
			Tools.logInfor("testClearData() -> Apps screen");
		}
		//Scroll screen to find Settings icon
		UiScrollable appsList = new UiScrollable(new UiSelector());
		appsList.setAsHorizontalList();
		appsList.scrollToBeginning(1);
		UiObject settings = appsList.getChildByText(new UiSelector().className(android.widget.TextView.class.getName()), "Settings");
		
		//Click on Settings icon
		if(settings.exists() && settings.isEnabled()) {
			settings.clickAndWaitForNewWindow();
			Tools.logInfor("testClearData() -> System settings screen");
		}
		
		//Scroll to find Application Manager
		Tools.logInfor("testClearData() -> Scroll the screen...to find application manager");
		UiScrollable settingsListView = new UiScrollable(new UiSelector().scrollable(true)); 
		UiObject application = settingsListView.getChildByText(new UiSelector().className("android.widget.TextView"), "Application manager");
		//Click on Application Manager item
		if(application.exists() && application.isEnabled()) {
			application.clickAndWaitForNewWindow();
			Tools.logInfor("testClearData() -> System application manager screen");
		}
		
		//Scroll to find GFE
		Tools.logInfor("testClearData() -> Scroll the screen...to find application GFE");
		UiScrollable appListView = new UiScrollable(new UiSelector().scrollable(true));
		UiObject gfe = appListView.getChildByText(new UiSelector().className("android.widget.TextView"), "Good");
		if(gfe.exists() && gfe.isEnabled()) {
			gfe.clickAndWaitForNewWindow();
			Tools.logInfor("testClearData() -> App info screen for Good for Enterprise");
		}
		//Click Clear Data button
		UiObject clearBtn = new UiObject(new UiSelector().resourceId("com.android.settings:id/right_button").text("Clear data"));
		if(clearBtn.exists() && clearBtn.isEnabled()) {
			clearBtn.click();
			UiObject comfirmDialog = new UiObject(new UiSelector().resourceId("android:id/alertTitle").text("Delete app data?"));
			UiObject comfirmBtn = new UiObject(new UiSelector().resourceId("android:id/button1").text("OK"));
			if(comfirmDialog.exists()) {
				comfirmBtn.click();
			} 
		}
		device.takeScreenshot(Tools.setFile(new Date().getTime()));
		Tools.logInfor("testClearData() -> Clear Data is done");
		
		
		device.pressHome();
		Tools.logInfor("Completed to execute method testClearData()");
	}
}
