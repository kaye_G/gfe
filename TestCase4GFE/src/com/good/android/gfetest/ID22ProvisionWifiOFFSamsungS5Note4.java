package com.good.android.gfetest;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiSelector;

public class ID22ProvisionWifiOFFSamsungS5Note4 extends Frequency {

    public void testID22ProvisionWifiOFF() throws UiObjectNotFoundException {
        boolean[] stepResult = new boolean[]{false,false};
        Frequency freq = new Frequency();
        UiDevice device = getUiDevice();
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String starts = date.format(new Date());

        // Get EmailAddress&PinCode from file
        String emailAddress = null;
        String[] pinCode = new String[3];
        FileReader fr = null;
        BufferedReader br = null;

        // Start button on Welcome page
        UiObject startBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/startBtn").text("Start"));
        // Agree button on License agreement page
        UiObject acceptBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/acceptBtn")
                .text("Agree"));
        // Email address and pincode page
        UiObject provEmail = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/prov_email").text(
                "Enter Email Address"));
        // Next button on start OTA process page
        UiObject goBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/goBtn").text("Next"));
        // okBtn is for setting password after provision finished.
        UiObject okBtn = new UiObject(new UiSelector().resourceId("android:id/button1"));
        // doneBtn is is on congratulation screen
        UiObject doneBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/doneBtn"));

        Tools.doubleLog("==========================================================================================");
        Tools.doubleLog("Start to execute method testID22()");

        // **********Precondition**********
        // None

        /**
         * **********Step1********** Step1. Insert the device sim card. Step1
         * also be mentioned in Readme.txt. Insert SIM card need to be done
         * before the whole automation testing start.
         * 
         */

        try {
            stepResult[0] = true;
            Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));

            /**
             * **********Step2********** Step2: Turn off the wifi in device and
             * start the handheld provision. Step2 Expected Results: The
             * provision can be done successfully.
             */

            // Turn OFF Wi-Fi
            launchAppCalled("Settings");
            freq.switchWiFi("OFF");
            getUiDevice().pressHome();

            // Clear GFE data then provision ;
            launchAppCalled("Settings");
            clearAppDataSamsung("Good");

            launchAppCalled("Good");

            /**
             * Start provision process
             */
            // welcome page
            if (startBtn.exists() && startBtn.isEnabled()) {
                Tools.logInfor("testProvision() -> Welcome Page");
                startBtn.clickAndWaitForNewWindow();
            }

            // License agrement page
            if (acceptBtn.exists() && acceptBtn.isEnabled()) {
                Tools.logInfor("testProvision() -> License Page");
                acceptBtn.clickAndWaitForNewWindow();
            }

            try {
                // Input email address
                fr = new FileReader("/data/local/tmp/Regist.txt");
                br = new BufferedReader(fr);
                emailAddress = br.readLine();
                if (provEmail.exists() && provEmail.isEnabled()) {
                    Tools.logInfor("testProvision() -> Input email address");
                    provEmail.setText(emailAddress);
                }
                sleep(1000);
                // Input Pin Code
                pinCode = br.readLine().split("-");
                UiObject provPin = null;
                for (int i = 0; i < pinCode.length; i++) {
                    int flag = i + 1;
                    provPin = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/prov_pin" + flag));
                    if (provPin.exists() && provPin.isEnabled()) {
                        Tools.logInfor("testProvision() -> Input pin code" + flag);
                        provPin.setText(pinCode[i]);
                    }
                }
            } catch (FileNotFoundException e) {
                Tools.logInfor(">>>>>>@Error: Regist file doesn't exist!");
                Tools.logInfor(e);
            } catch (IOException e) {
                Tools.logInfor(">>>>>>@Error: Fail to read Regist file!");
                Tools.logInfor(e);
            }
            sleep(1000);

            // Click Next to start OTA process
            if (goBtn.exists() && goBtn.isEnabled()) {
                goBtn.clickAndWaitForNewWindow();
                Tools.logInfor("testProvision() -> Start OTA process");
            }

            /**
             * Set password after provision successfully. This method handle the
             * scenario which Password setting screen will overlap the
             * congratulations screen issue.
             */

            /**
             * Waiting for 5mins for provision by check if set password dialog
             * will be pop up and set password
             */

            if (okBtn.waitForExists(300000)) {
                okBtn.click();
                freq.setPasswordNew();
                /**
                 * if password setting screen is not existed. then check if
                 * congratulation screen exist and set password after tap done
                 * button on congratulation screen.
                 */
            } else if (doneBtn.exists()) {
                doneBtn.clickAndWaitForNewWindow();
                okBtn.click();
                freq.setPasswordNew();
            }
            /**
             * Below is workaround for GFEANDROID-6345 [Provisioning]: GFE is
             * getting stuck in starting services stage.
             */
            else {
                // Go to home screen then launch GFE again as workaround for
                // GFEANDROID-6345
                device.pressHome();
                try {
                    Runtime.getRuntime()
                            .exec("am start -n com.good.android.gfe/com.good.android.ui.LaunchHomeActivity");
                } catch (IOException e) {
                    Tools.logInfor(e);
                }
                okBtn.click();
                freq.setPasswordNew();
            }

            // Sleep 60s for initial sync.
            sleep(60000);

            try {
                br.close();
                fr.close();
            } catch (IOException e) {
                Tools.logInfor(e);
            }

            UiObject inboxexist = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/title")
                    .textContains("Inbox"));
            stepResult[1] = inboxexist.waitForExists(1000);

            Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[1]));

            // Turn on Wifi for continue testcase execution.
            launchAppCalled("Settings");
            freq.switchWiFi("ON");
            getUiDevice().pressHome();

        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        } catch (Exception e) {
            Tools.logInfor(e);
        }

        String ends = date.format(new Date());
        Tools.doubleLog("@@Smoke ID22 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
        Tools.writeData("22," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends);
    }

}
