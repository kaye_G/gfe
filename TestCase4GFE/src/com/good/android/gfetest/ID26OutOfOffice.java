package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiSelector;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.good.android.gfetest.Frequency.Activity;

public class ID26OutOfOffice extends UiAutomatorTestCase {
    private int errorCode = 0;
    public void testID26OutOfOffice() throws UiObjectNotFoundException {
        boolean[] stepResult = new boolean[]{false,false,false,false};
        Frequency freq = new Frequency();

        Tools.doubleLog("==========================================================================================");
        Tools.doubleLog("Start to execute method testID26()");
        /**
         * **********Precondition********** Don't need specific precondition for
         * this testcase.
         */

        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String starts = date.format(new Date());
        try {
            UiDevice device = getUiDevice();
            freq.launchGFE(device);
            freq.gotoActivity(Activity.Email);

            stepResult[0] = true;
            /**
             * // **********Step1********** Step1:Install GFE on the device
             * 
             */
            Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));

            /**
             * // **********Step2********** Step2: Go to GFE Preferences-> OOO->
             * Auto Response Step2 Expected Results: Auto Response UI display.
             * 
             */

            freq.gotoActivity(Activity.Preferences);

            // Open Out of Office UI then activate OOO
            UiObject OOOField = new UiObject(new UiSelector().resourceId("android:id/title").text("Out of Office"));
            OOOField.click();
            UiObject ActivateOOO = new UiObject(new UiSelector().resourceId("android:id/checkbox"));
            stepResult[1] = ActivateOOO.click();

            // Write testing results to file

            Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));

            /**
             * **********Step3********** Step3: Edit Auto Response message
             * (updates) > tap on "save" Step3 Expected Results: Auto Response
             * message can be saved correctly and synced to OWA/DWA and OL/Notes
             * desktop client.
             */
            UiObject AutoResponse = new UiObject(new UiSelector().resourceId("android:id/title").text("Auto Response"));
            AutoResponse.click();

            UiObject AutoResponseText = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/outofoffice"));
            AutoResponseText.clearTextField();

            AutoResponseText.setText(Frequency.random2);
            getUiDevice().pressEnter();

            UiObject Save = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/save"));
            Save.click();
            getUiDevice().pressBack();

            UiObject GoodTabScroll = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/goodTabScroll"));

            GoodTabScroll.swipeRight(2);
            sleep(1000);
            UiObject EmailTab = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/tab_framer").index(0));
            // Write testing results to file
            stepResult[2] = EmailTab.click();

            Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));
            /**
             * **********Step4********** Step4:Receive an email to the GFE
             * account. Step4 Expected Results: Make sure the sender receives an
             * OOO notification with updated OOO message.
             * 
             * Input email address then send it out. In this case, send the
             * email to itself asia1new@asia.qagood.com
             * 
             */
            UiObject ComposeEmail = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/new_email"));
            ComposeEmail.click();
            UiObject EmailAddressInput = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/to"));
            EmailAddressInput.click();
            EmailAddressInput.setText("asia1new@asia.qagood.com");
            getUiDevice().pressEnter();

            // Input email subject
            UiObject EmailSubjectInput = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/subject"));
            EmailSubjectInput.setText("OOO" + Frequency.random2);
            getUiDevice().pressEnter();

            // Tap send button.
            UiObject EmailSendButton = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/send_button"));
            EmailSendButton.click();

            // Waiting for max 300s for new email arriving then open this email.
            UiObject EmailBodyContent = new UiObject(new UiSelector()
                    .resourceId("com.good.android.gfe:id/body_preview").textContains(Frequency.random2));
            EmailBodyContent.waitForExists(300000);
            stepResult[3] = EmailBodyContent.click();
            if (stepResult[3] == false){
				errorCode = 5;
           	
            }
            getUiDevice().pressBack();

            Tools.doubleLog("@@Step4 result -> " + Tools.getResult(stepResult[3]));

            // Disable OOO after finish this testcase
            // Launch GFE and go to Preferences tab;
            freq.gotoActivity(Activity.Preferences);

            // Open Out of Office UI then Deactivate OOO
            OOOField.click();
            ActivateOOO.click();
  

        } catch (UiObjectNotFoundException e) {
			if (errorCode == 0) {
				errorCode = 400;
				Tools.logInfor("ErrorCode=400");
			}
            Tools.logInfor(e);
        } catch (Exception e) {
            Tools.logInfor(e);
        }

        String ends = date.format(new Date());
        freq.gotoActivity(Activity.Email);
        Tools.doubleLog("@@Smoke ID26 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
        Tools.writeData("26," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
    }

}
