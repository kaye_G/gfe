package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiSelector;
import com.android.uiautomator.core.UiWatcher;


public class ID21SendFileandLinktoGFEwhenGFElock extends Frequency {

    Frequency freq = new Frequency();
    boolean[] stepResult = new boolean[]{false,false,false,false,false,false};
    String starts, ends;

    public void testID21() {
        Tools.doubleLog("==========================================================================================");
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        starts = date.format(new Date());
        Tools.doubleLog("Start to execute method testID21()");
        UiDevice device = getUiDevice();
        freq.setStepResult(stepResult);
        String attchmentNamefortesting = "Basement-Inventory.doc";

        try {
            // RegisterWatcher
            UiWatcher closeAlertWatcher = new UiWatcher() {
                public boolean checkForCondition() {
                    UiObject Loading = new UiObject(new UiSelector().resourceId("android:id/message").text("Loading?"));
                    if (Loading.exists()) {
                        Tools.logInfor("runinto UiWatcher");
                        Loading.waitUntilGone(100000);
                        return true;
                    }

                    return false;
                }
            };
            UiDevice.getInstance().registerWatcher("CLOSE_ALERT_WATCHER", closeAlertWatcher);
            UiDevice.getInstance().runWatchers();

            /**
             * Step1:GC: Set Lock Screen Policies as Require password when idle
             * for more than 1 hours
             */
            Tools.logInfor("Start Step1 ...");
            stepResult[0] = true;

            /**
             * Step2: 2. DEVICE: Launch GDAPP and unlock GFE when GFE login
             * screen display (follow steps, means from step2 to step 5 must be
             * finished in 1 hours))
             */
            Tools.logInfor("Start Step2 ...");
            freq.launchGFE(device);

            /**
             * Force stop GoodShare then launch GFE and GoodShare as working
             * around for stuck at authenticating screen after GFE was re
             * provisioned.
             */
            getUiDevice().pressHome();
            stepResult[2] = freq.forceStopApp(freq.findAppFromAppManager("Good Share"));
            sleep(freq.blink);

            freq.launchGFE(device);
            launchAppCalled("Good Share");
            freq.launchGFE(device);
            stepResult[1] = true;

            /**
             * Step3. Make sure that GFE app is locked in background. (In GD app
             * screen wait till GFE auto locked in background)
             */

            Tools.logInfor("Start Step3 ...");
            /**
             * Set lock timeout to 1min then switch to home screen watiting for
             * GFE auto lock.
             */
            freq.gotoActivity(Activity.Preferences);
            freq.findItemInPreferences("Password").click();
            freq.LockOutTimeOut("1 minute");
            getUiDevice().pressHome();
            // Waiting for 70s for GFE auto out by time out 1 minute.
            sleep(70000);
            stepResult[2] = true;

            /**
             * Step 4. Transfer file(s) from GDApp to Good app when Good app is
             * under the locked status. Check result #4.
             * 
             * Step4-results: Locked screen is shown。
             */

            launchAppCalled("Good Share");

            // Got sub folder Bennew
            freq.selectFolderInGoodShare("Data Sources");

            freq.selectFolderInGoodShare("gs-sp10\\goodqa1");
            freq.selectFolderInGoodShare("Shared Documents");
            freq.selectFolderInGoodShare("Bennew");

            // Select specific file then Email files to GFE
            UiObject actionBarSelect = new UiObject(
                    new UiSelector().resourceId("com.good.goodshare:id/action_bar_select"));
            actionBarSelect.click();
            freq.selectFolderInGoodShare(attchmentNamefortesting);
            freq.tapOverflowSubItemInGoodShare("Email files");
            // sleep 30s for downloading file in goodShare
            sleep(30000);

            UiObject GFELockScreen = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/logo"));
            stepResult[3] = GFELockScreen.exists();

            /**
             * Step5: Input the correct password to unlock Good app. Check
             * result #5. Results 5. After Good app unlocked the email compose
             * screen shown up, and the file(s) should displayed as
             * attachment(s).
             */
            Tools.logInfor("Start Step5 ...");

            freq.unlockGFE("a");

            UiObject AttachmentButton = new UiObject(
                    new UiSelector().resourceId("com.good.android.gfe:id/subject_attachments_button"));
            AttachmentButton.click();
            UiObject attchmentNameInGFE = new UiObject(new UiSelector().resourceId(
                    "com.good.android.gfe:id/attachment_filename").text(attchmentNamefortesting));
            stepResult[4] = attchmentNameInGFE.exists();

            /**
             * Step6: repet step2-5 to test send link result #5. Results 5.
             * After Good app unlocked the email compose screen shown up, and
             * the file(s) should displayed as attachment(s).
             */

            // Presshome then sleep 70s for GFE lock
            getUiDevice().pressHome();
            sleep(70000);
            launchAppCalled("Good Share");
            actionBarSelect.click();
            freq.selectFolderInGoodShare(attchmentNamefortesting);
            freq.tapOverflowSubItemInGoodShare("Email links");
            // sleep 5s waiting for file transfer
            sleep(5000);

            freq.unlockGFE("a");
            UiObject linkNameInGFE = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/body")
                    .textContains("Basement-Inventory.doc - http://gs-sp10/goodqa1/"));
            stepResult[5] = linkNameInGFE.exists();
            // Go back to email list view.
            UiObject backToList = new UiObject(
                    new UiSelector().resourceId("com.good.android.gfe:id/back_to_list_button"));
            backToList.click();

            /**
             * Set lockout timeout to 15 minutes
             */
            freq.gotoActivity(Activity.Preferences);
            freq.findItemInPreferences("Password").click();
            freq.LockOutTimeOut("15 minutes");

        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        } catch (Exception e) {
            Tools.logInfor(e);
        }
        ends = date.format(new Date());
        Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
        Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
        Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));
        Tools.doubleLog("@@Step4 result -> " + Tools.getResult(stepResult[3]));
        Tools.doubleLog("@@Step5 result -> " + Tools.getResult(stepResult[4]));
        Tools.doubleLog("@@Step6 result -> " + Tools.getResult(stepResult[5]));

        Tools.doubleLog("@@Smoke ID21 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
        Tools.writeData("21," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends);

    }

}
