package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.io.*;
import java.util.Date;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;
import com.good.android.gfetest.Frequency.Activity;
import com.good.android.gfetest.Frequency.EmailItem;

public class ID12SendEmailToMultipleRecipients extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
	private int errorCode = 0;
	String starts, ends;
	boolean[] stepResult = new boolean[9];
	private final String repTo = "katharine1@asia.qagood.com, katharine3@asia.qagood.com";
	private final String repCc = "katharine4@asia.qagood.com, benwang1@asia.qagood.com";
	private final String repBcc = "asia1new@asia.qagood.com, katharine2@asia.qagood.com";
	private final String subject = "SmokeID12";
	private final String body = "Test smoke ID12";
	
	public void testID12() {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testID12()");
		UiDevice device = getUiDevice();
		freq.setStepResult(stepResult);
		freq.launchGFE(device);
		
		try {
/**
 * 1. Go to Email Inbox.
 */
			freq.gotoActivity(Activity.Email);
			this.stepResult[0] = freq.isCurrentEmailFolderEquals("inbox");
			freq.screenCapture(device);
/**
 * 2. Tap Compose icon.
 */
			this.stepResult[1] = freq.gotoComposeWindow();
			freq.screenCapture(device);
/**
 * 3. Enter multiple email addresses in the TO/CC/BCC fields.
 */
			this.stepResult[2] = freq.enterMsgRecipent(EmailItem.To, repTo);
			sleep(freq.blink);
			this.stepResult[2] &= freq.enterMsgRecipent(EmailItem.Cc, repCc);
			this.stepResult[2] &= freq.enterMsgRecipent(EmailItem.Bcc, repBcc);
			freq.screenCapture(device);
/**
 * 4. Enter some text in the Subject and the body of the email.
 */
			this.stepResult[3] = freq.enterMsgText(EmailItem.Subject, subject);
			device.pressBack();
			this.stepResult[3] &= freq.enterMsgText(EmailItem.Body, body);
			freq.screenCapture(device);
/**
 * 5. Tap on Send icon.
 */
			this.stepResult[4] = freq.sendMsg();
			freq.screenCapture(device);
/**
 * 6. Tap on the menu button which is next to the Inbox, and select Sent Items.
 */
			freq.gotoEmailFolder("Sent");
			this.stepResult[5] = freq.isCurrentEmailFolderEquals("Sent");
			long st = new Date().getTime();
			while (!freq.isMsgExists(subject)) {
				sleep(freq.tea);
		        if (!Tools.timer(st, 5 * freq.read, "")) {
		        	this.stepResult[5] = false;
		        	errorCode = 1;
					Tools.logInfor("ErrorCode=1");
		            break;
		        }
			}
			freq.screenCapture(device);
/**
 * 7. Tap on Sent, and select Inbox.
 */
			freq.gotoSubFolder(Activity.Email, 0);
			this.stepResult[6] = freq.isCurrentEmailFolderEquals("inbox");
			freq.screenCapture(device);
/**
 * 8. Go to Outlook, verify the Inbox and Sent folders are synced.
 * 9. Go to each recipients email inbox.
 */
			this.stepResult[7] = true;
			st = new Date().getTime();
			this.stepResult[8] = true;
			while (!freq.isMsgExists(subject)) {
				freq.scrollToBegin(1);
				sleep(freq.tea);
				//Waiting for 5 min;
		        if (!Tools.timer(st, 5 * freq.read, "")) {
		        	this.stepResult[8] = false;
		        	if (errorCode == 0) {
		        		errorCode = 1;
						Tools.logInfor("ErrorCode=1");
					}
		            break;
		        }
			}
			freq.screenCapture(device);
			freq.gotoActivity(Activity.Email);
		} catch(UiObjectNotFoundException e) {
			if (errorCode == 0) {
				errorCode = 400;
				Tools.logInfor("ErrorCode=400");
			}
			Tools.logInfor(e);
		} catch(IOException e) {
			if (errorCode == 0) {
				errorCode = 500;
				Tools.logInfor("ErrorCode=500");
			}
			Tools.logInfor(e);
		} catch(Exception e) {
			Tools.logInfor(e);
		} finally {
			ends = date.format(new Date());
			Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
			Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
			Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));
			Tools.doubleLog("@@Step4 result -> " + Tools.getResult(stepResult[3]));
			Tools.doubleLog("@@Step5 result -> " + Tools.getResult(stepResult[4]));
			Tools.doubleLog("@@Step6 result -> " + Tools.getResult(stepResult[5]));
			Tools.doubleLog("@@Step7 result -> " + Tools.getResult(stepResult[6]));
			Tools.doubleLog("@@Step8 result -> Need to check this step on OWA...");
			Tools.doubleLog("@@Step9 result -> " + Tools.getResult(stepResult[8]));
			Tools.doubleLog("@@Smoke ID12 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
			Tools.writeData("12," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
		}
	}
}
