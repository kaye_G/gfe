/**
 * Precondition:
 * 1. Enable add photo policy from GMC(Path: Policies > File Handling > Checked "Allow access to camera & device photo gallery")
 * 2. There is no restriction for attachment size and file extension for sending attachment
 * 3. Gallery has some pictures
 * Step:
 * 1. Go to Email tab view, tap on Compose icon, click paperclip icon
 * 2. Press camera icon
 * 3. Tap on Picture Gallery button
 * 4. Add pictures from gallery
 * 5. Send the mail to recipients
 * Expected Result:
 * 1. Enter into attachment manager
 * 2. Popup active sheet, two buttons "Take photo", "Choose photo"
 * 3. Enter into Photo albums application
 * 4. Back to attachment manager, the photos are attached
 * 5. The mail can be send, the recipients can receive the photos as attachment
 */

package com.good.android.gfetest;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;
import com.good.android.gfetest.Frequency.Activity;
import com.good.android.gfetest.Frequency.EmailItem;

public class ID09AddImageFromGallary extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
	private int errorCode = 0;
	private final String emailRecipient = "katharine2@asia.qagood.com, katharine4@asia.qagood.com, katharine1@asia.qagood.com, asia1new@asia.qagood.com";
	private final String emailSubject = "SmokeID09@" + new Date().getTime();
	private final String emailBody = "test@" + new Date().getTime();
	boolean[] stepResult = {false, false, false, false, false};

	public void testID09() {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testID09()");
		UiDevice device = getUiDevice();
		freq.launchGFE(device);

/** 1. Go to Email tab view, tap on Compose icon, click paperclip icon; */
		Tools.logInfor("Start Step1 ...");
		stepResult[0] = freq.gotoComposeWindow();
		sleep(freq.blink);
		
/** 2. Press camera icon;
  * 3. Tap on Picture Gallery button; 
  * 4. Add pictures from gallery; */
		Tools.logInfor("Start Step2&3&4 ...");
		stepResult[1] = freq.attachFile("Picture");
		sleep(freq.blink);
		stepResult[2] = stepResult[1];
		stepResult[3] = stepResult[1];
		
/** 5. Send the mail to recipients; */
		Tools.logInfor("Start Step5 ...");
		try {
			stepResult[4] = freq.enterMsgRecipent("To", emailRecipient);
			sleep(freq.blink);
			stepResult[4] &= freq.enterMsgText(EmailItem.Subject, emailSubject);
			device.pressBack();
			stepResult[4] &= freq.enterMsgText(EmailItem.Body, emailBody);
			sleep(freq.blink);
			stepResult[4] &= freq.sendMsg();
			sleep(freq.blink);
			if (!freq.isMsgSentOut(emailSubject)) {
				if ((this.errorCode==0) && (freq.errorCode!=0)) {
					this.errorCode = freq.errorCode;
					Tools.logInfor("ErrorCode=" + errorCode);
				}
				freq.screenCapture(device);
				stepResult[4] &= false;
				Tools.logInfor("@Error: The message doesn't send out!!!");
			} else {
				freq.screenCapture(device);
				stepResult[4] &= true;
				sleep(freq.blink);
				freq.backToEmailList();
				sleep(freq.blink);
				freq.gotoSubFolder(Activity.Email, 0); //Navigate to Inbox
				sleep(freq.blink);
				long startsT = new Date().getTime();
				sleep(freq.blink);
				while(!freq.isMsgExists(emailSubject)) {
					sleep(freq.tea);
					if(!Tools.timer(startsT, 5 * freq.read, "")) {
						stepResult[4] &= false;
						errorCode = 1;
						Tools.logInfor("ErrorCode=1");
						break;
					} else {
						stepResult[4] &= true;
					}
				}
				device.takeScreenshot(Tools.setFile(new Date().getTime()));
				sleep(freq.blink);
			}
		} catch(UiObjectNotFoundException e){
			if (errorCode == 0) {
				errorCode = 400;
				Tools.logInfor("ErrorCode=400");
			}
			Tools.logInfor(e);
		} catch(IOException e) {
			Tools.logInfor(e);
		} catch(Exception e) {
			Tools.logInfor(e);
		} finally {
			String ends = date.format(new Date());
			freq.gotoActivity(Activity.Email);
			Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
			Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
			Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));
			Tools.doubleLog("@@Step4 result -> " + Tools.getResult(stepResult[3]));
			Tools.doubleLog("@@Step5 result -> " + Tools.getResult(stepResult[4]));
			Tools.doubleLog("@@Smoke ID09 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
			Tools.writeData("09," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
System.out.println(errorCode);
		}
	}
}
