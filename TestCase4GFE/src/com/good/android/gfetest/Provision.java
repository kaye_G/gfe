/**
 * Precondition:
 * 1. Turn on Developer options;
 * 2. Set device "Lock screen" vaule = None;
 * 3. API Level >= 18;
 * Step:
 * 1. Connect device to computer;
 * 2. Compile test case and create a .jar file;
 * 3. Push .jar file to device, then execute .jar file; 
 * Expect Result:
 * 3. GFE is provisioned on device;
 * 
 * Author: Katharine Ye
 * Date: Feb 2nd, 2015
 */

package com.good.android.gfetest;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;

import java.io.*;
import java.util.*;

public class Provision extends UiAutomatorTestCase {
    Frequency freq = new Frequency();

    public void testProvision() throws UiObjectNotFoundException {
        UiDevice device = getUiDevice();
        freq.launchGFE(device);

        try {
            // Get EmailAddress&PinCode from file
            String emailAddress = null;
            String[] pinCode = new String[3];
            FileReader fr = null;
            BufferedReader br = null;

            // Print&log the start
            Tools.logInfor("==========================================================================================");
            Tools.logInfor("Start to execute method testProvision()");

            // Welcome page
            UiObject startBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/startBtn").text(
                    "Start"));
            if (startBtn.exists() && startBtn.isEnabled()) {
                Tools.logInfor("testProvision() -> Welcome Page");
                startBtn.clickAndWaitForNewWindow();
            }

            // License page
            UiObject acceptBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/acceptBtn").text(
                    "Agree"));
            if (acceptBtn.exists() && acceptBtn.isEnabled()) {
                Tools.logInfor("testProvision() -> License Page");
                acceptBtn.clickAndWaitForNewWindow();
            }

            // Input Email&Pin
            UiObject provEmail = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/prov_email").text(
                    "Enter Email Address"));
            try {
                // Input email address
                fr = new FileReader("/data/local/tmp/Regist.txt");
                br = new BufferedReader(fr);
                emailAddress = br.readLine();
                if (provEmail.exists() && provEmail.isEnabled()) {
                    Tools.logInfor("testProvision() -> Input email address");
                    provEmail.setText(emailAddress);
                }
                sleep(1000);
                // Input Pin Code
                pinCode = br.readLine().split("-");
                UiObject provPin = null;
                for (int i = 0; i < pinCode.length; i++) {
                    int flag = i + 1;
                    provPin = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/prov_pin" + flag));
                    device.takeScreenshot(Tools.setFile(new Date().getTime()));
                    if (provPin.exists() && provPin.isEnabled()) {
                        Tools.logInfor("testProvision() -> Input pin code" + flag);
                        provPin.setText(pinCode[i]);
                    }
                }
            } catch (FileNotFoundException e) {
                Tools.logInfor(">>>>>>@Error: Regist file doesn't exist!");
                Tools.logInfor(e);
            } catch (IOException e) {
                Tools.logInfor(">>>>>>@Error: Fail to read Regist file!");
                Tools.logInfor(e);
            }
            try {
                br.close();
                fr.close();
            } catch (IOException e) {
                Tools.logInfor(e);
            }

            sleep(1000);

            // Click Next to start OTA process
            UiObject goBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/goBtn").text("Next"));
            if (goBtn.exists() && goBtn.isEnabled()) {
                goBtn.clickAndWaitForNewWindow();
                Tools.logInfor("testProvision() -> Start OTA process");
            }

            /**
             * This part is original method freq.waitForOtaCompleted();
             * sleep(3*freq.hearbeat);
             * 
             * freq.setPassword(); sleep(freq.hearbeat);
             */

            /**
             * Set password after provision successfully. This method handle the
             * scenario which Password setting screen will overlap the
             * congratulations screen issue.
             */
            // okBtn is for setting password after provision finished.
            UiObject okBtn = new UiObject(new UiSelector().resourceId("android:id/button1"));
            // doneBtn is is on congratulation screen
            UiObject doneBtn = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/doneBtn"));

            // Waiting for 5mins for provision by check if set password dialog
            // will
            // be pop up and set password
            if (okBtn.waitForExists(300000)) {
                Tools.doubleLog("password seting screen pop up directly, OK button is existed");

                okBtn.click();
                freq.setPasswordNew();

                // if password setting screen
                // is not existed. then check if congratulation screen exist and
                // set
                // password after tap done button on
                // congratulation screen.
            } else if (doneBtn.exists()) {
                Tools.doubleLog("Congratulations screen show up");

                doneBtn.clickAndWaitForNewWindow();
                okBtn.click();
                freq.setPasswordNew();
            }
            // Below is workaround for GFEANDROID-6345 [Provisioning]: GFE is
            // getting stuck in starting services stage.
            else {
                // Go to home screen then launch GFE again as workaround for
                // GFEANDROID-6345
                Tools.doubleLog("Stuck at starting severice 5mins will presshome as workaround");

                device.pressHome();
                try {
                    Runtime.getRuntime()
                            .exec("am start -n com.good.android.gfe/com.good.android.ui.LaunchHomeActivity");
                } catch (IOException e) {
                    Tools.logInfor(e);
                }
                okBtn.click();
                freq.setPasswordNew();
            }
            Tools.logProvisionResult("ProvisionSuccessfully");

            // Sleep 60s for initial sync.
            sleep(60000);

        } catch (UiObjectNotFoundException e) {
            Tools.logProvisionResult("ProvisionFailed");
        } catch (Exception e) {
            Tools.logInfor(e);
        }

        // Take a screenshot
        device.takeScreenshot(Tools.setFile(new Date().getTime()));
        Tools.logInfor("Completed to execute method testProvision()");
    }
}
