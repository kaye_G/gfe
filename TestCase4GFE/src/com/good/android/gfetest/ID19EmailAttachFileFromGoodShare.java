package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiSelector;

public class ID19EmailAttachFileFromGoodShare extends Frequency {

    Frequency freq = new Frequency();
    UiDevice device = getUiDevice();

    boolean[] stepResult = new boolean[]{false,false,false,false,false,false,false};
    String starts, ends;

    public void testID19() {
        Tools.doubleLog("==========================================================================================");
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        starts = date.format(new Date());
        Tools.doubleLog("Start to execute method testID19()");
        UiDevice device = getUiDevice();
        freq.setStepResult(stepResult);
        Random random1 = new Random();
        String RandomEmailSubject = Long.toString(random1.nextLong(), 36);

        try {
            Tools.logInfor("Start Step1 ...");
            /**
             * Step1.Launch GFE
             */
            freq.launchGFE(device);
            stepResult[0] = true;

            /**
             * Force stop GoodShare then launch GFE and GoodShare as working
             * around for stuck at authenticating screen after GFE was re
             * provisioned.
             */
            getUiDevice().pressHome();
            stepResult[2] = freq.forceStopApp(freq.findAppFromAppManager("Good Share"));
            sleep(freq.blink);

            freq.launchGFE(device);
            launchAppCalled("Good Share");
            freq.launchGFE(device);

            /*
             * Step2.Tap to compose a new email
             */

            Tools.logInfor("Start Step2 ...");
            freq.gotoActivity(Activity.Email);
            freq.gotoComposeWindow();
            UiObject ComposeText = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/title")
                    .textContains("Compose"));
            stepResult[1] = ComposeText.exists();

            /**
             * Step3.Tap action overflow icon in email compose window
             * Step3-results: Overflow menu appears with five option,
             */
            Tools.logInfor("Start Step3 ...");
            freq.tapOverflow();

            UiObject overflowItemCounts = new UiObject(new UiSelector().className("android.widget.LinearLayout").index(
                    4));
            stepResult[2] = overflowItemCounts.exists();

            /**
             * Step4: Tap "Attach File" to add attachments to email, check
             * Results 4 Step4-results:Action sheet displays list of options
             */
            Tools.logInfor("Start Step4 ...");
            // Tap "Attach File" in drop down list
            UiObject tapAttachFile = new UiObject(new UiSelector().textContains("Attach File"));
            tapAttachFile.click();

            // Check if pop up a screen to ask user to select app
            UiObject alerttitle = new UiObject(new UiSelector().textContains("Attach"));
            stepResult[3] = alerttitle.exists();

            /**
             * Step5:Tap GoodShre to add files from GoodShre, check results
             * Step5-results:tapping "GoodShre" will transition user to
             * GoodShare
             */
            Tools.logInfor("Start Step5 ...");

            // Tap GoodShare in the app list
            UiObject alertcontent = new UiObject(new UiSelector().textContains("Good Share"));
            // Waiting longest for 60s for GoodShare launch
            alertcontent.clickAndWaitForNewWindow(60000);

            // Check if Good Share is launched.
            stepResult[4] = alertcontent.exists();

            /**
             * Step6:Select some files in GoodShare, check result#6
             * Step6-results:selected files appears in attachments list
             */
            Tools.logInfor("Start Step6 ...");

            // Select file in GoodShare
            freq.selectFolderInGoodShare("Data Sources");
            sleep(10000);

            sleep(10000);
            freq.selectFolderInGoodShare("gs-sp10\\goodqa1");
            freq.selectFolderInGoodShare("Shared Documents");
            freq.selectFolderInGoodShare("Bennew");
            freq.selectFolderInGoodShare("Basement-Inventory.doc");

            // File downloading percent Progress - 100% download
            UiObject percentProgressGoodShare = new UiObject(new UiSelector().resourceId(
                    "com.good.goodshare:id/percentProgress").text("100%"));
            // overflow icon in GoodShare
            UiObject overFlowGoodShare = new UiObject(new UiSelector().className("android.widget.ImageButton").index(2));

            // Waiting for 3mins to download file in GoodShare then attach file
            // to GFE
            if (percentProgressGoodShare.waitForExists(180000)) {
                overFlowGoodShare.clickAndWaitForNewWindow();
                tapAttachFile.clickAndWaitForNewWindow();
            }

            UiObject attchmentNameInGFE = new UiObject(new UiSelector().resourceId(
                    "com.good.android.gfe:id/attachment_filename").text("Basement-Inventory.doc"));
            stepResult[5] = attchmentNameInGFE.exists();

            /**
             * Step7:input some recipenents and send email check results 7
             * Step7-results:tapping email send out successfully and recipients
             * receive the email with the attachments attached from GS correctly
             */

            Tools.logInfor("Start Step7 ...");
            // tap actionbar home to go back to emaili compse view.
            UiObject actionBarHome = new UiObject(
                    new UiSelector().resourceId("com.good.android.gfe:id/action_bar_home"));
            actionBarHome.click();

            // Input recipients email address subject and send email
            freq.enterMsgRecipent("To", "asia1new@asia.qagood.com");
            getUiDevice().pressEnter();
            freq.enterMsgText("subject", RandomEmailSubject);
            getUiDevice().pressEnter();
            freq.sendMsg();

            UiObject EmailSubjectContent = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/subject")
                    .textContains(RandomEmailSubject));
            EmailSubjectContent.waitForExists(300000);
            EmailSubjectContent.click();

            UiObject AttachmentButton = new UiObject(
                    new UiSelector().resourceId("com.good.android.gfe:id/subject_attachments_button"));
            AttachmentButton.click();
            stepResult[6] = attchmentNameInGFE.exists();

        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        } catch (Exception e) {
            Tools.logInfor(e);
        }
        ends = date.format(new Date());
        Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
        Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
        Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));
        Tools.doubleLog("@@Step4 result -> " + Tools.getResult(stepResult[3]));
        Tools.doubleLog("@@Step5 result -> " + Tools.getResult(stepResult[4]));
        Tools.doubleLog("@@Step6 result -> " + Tools.getResult(stepResult[5]));
        Tools.doubleLog("@@Step7 result -> " + Tools.getResult(stepResult[6]));

        Tools.doubleLog("@@Smoke ID19 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
        Tools.writeData("19," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends);

    }

}
