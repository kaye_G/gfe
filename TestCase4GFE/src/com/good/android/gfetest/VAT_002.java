package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;
import com.good.android.gfetest.Frequency.Activity;

public class VAT_002 extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
	UiDevice device = getUiDevice();
	boolean[] stepResult = new boolean[2];
	String starts, ends;
	
	public void testVAT002() {
		Tools.doubleLog("==========================================================================================");
		UiObject signObj = null;
		freq.setStepResult(stepResult);
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testVAT002()");
		try {
/**
 * Step1. Launch GFE, navigate to Preferences tab.
 */
			freq.launchGFE(device);
			freq.gotoActivity(Activity.Preferences);
/**	
 * Step2. Select Signature item from Preferences tab.
 */
			signObj = freq.findItemInPreferences("signature");
			if (signObj.waitForExists(3 * freq.tea)) {
				signObj.clickAndWaitForNewWindow();
				freq.screenCapture(device);
				this.stepResult[0] = !freq.isSignatureEnabled();
			}
/**	
 * Step3. Check on check box "Enable".	
 */		
			freq.enableSignatureOption(!this.stepResult[0]);
			sleep(freq.breath);
			this.stepResult[1] = freq.isSignatureEnabled();
			freq.screenCapture(device);
		} catch(UiObjectNotFoundException e) {
			Tools.logInfor(e);
		}
		ends = date.format(new Date());
		Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
		Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
		Tools.doubleLog("@@VAT_002 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
		Tools.writeData("VAT_002," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends);
	}
}
