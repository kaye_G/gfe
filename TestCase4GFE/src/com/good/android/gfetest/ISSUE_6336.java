package com.good.android.gfetest;

import java.io.IOException;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;
import com.good.android.gfetest.Frequency.EmailItem;

public class ISSUE_6336 extends UiAutomatorTestCase {
	private final String recipient = "katharine1@asia.qagood.com";
	private final String Signed = "Signed";
	Frequency freq = new Frequency();
	public void test6336() {
		for(int i=1; i<=100; i++) {
			freq.gotoComposeWindow();
			freq.enterMsgRecipent(EmailItem.To, recipient);
			try {
				freq.enterMsgText(EmailItem.Subject, "Signed&Encrypted"+i);
			} catch (IOException e) {
				Tools.logInfor(e);
			}
			changeSecurityMode("Signed & Encrypted");
			freq.sendMsg();
			sleep(freq.tea);
		}
		
		for(int i=1; i<=100; i++) {
			freq.gotoComposeWindow();
			freq.enterMsgRecipent("To", recipient);
			freq.enterMsgText("Subject", Signed+i);
			changeSecurityMode("Signed");
			freq.sendMsg();
			sleep(freq.tea);
		}
		
	}
	
	public void changeSecurityMode(String mode) {
		try {
		UiObject selectObj = new UiObject(new UiSelector().
				className("android.view.View").resourceId("com.good.android.gfe:id/smime_email_type_button"));
		selectObj.waitForExists(freq.tea);
		selectObj.clickAndWaitForNewWindow(); 
		UiObject modeObj = new UiObject(new UiSelector().resourceId("android:id/text1").text(mode));
		modeObj.waitForExists(freq.tea);
		modeObj.clickAndWaitForNewWindow();
		} catch(UiObjectNotFoundException e) {
			e.printStackTrace();
		}
	}
}


	