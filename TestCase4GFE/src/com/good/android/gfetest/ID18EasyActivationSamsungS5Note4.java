package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiSelector;

public class ID18EasyActivationSamsungS5Note4 extends Frequency {
    boolean[] stepResult = new boolean[]{false,false,false,false};

    public void testID18EasyActivation() throws UiObjectNotFoundException {
        Tools.doubleLog("==========================================================================================");
        Tools.doubleLog("Start to execute method testID18()");

        /**
         ********** Step1********** Step1: Install and provision newest GFE with password
         * policy. Step1 is done in other testcase isEnabled
         */
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String starts = date.format(new Date());
        try {
            stepResult[0] = true;

            Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));

            // For Step2 and step3 detail please see below two void.
            launchAppCalled("Settings");
            clearAppDataSamsung("Good Access");
            launchAppCalled("Good Access");

            EasyActivation();
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        } catch (Exception e) {
            Tools.logInfor(e);
        }

        String ends = date.format(new Date());
        Tools.doubleLog("@@Smoke ID18 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
        Tools.writeData("18," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends);
    }

    private void EasyActivation() throws UiObjectNotFoundException {

        // sleep 60s waiting for action tap
        // "set up using Good for Enterprise Auth Delegate"
        UiObject EAbyGFE = new UiObject(new UiSelector().className("android.widget.LinearLayout").childSelector(
                new UiSelector().resourceId("com.good.gdgma:id/app_name").text("Good for Enterprise - Auth Delegate")));

        EAbyGFE.waitForExists(60000);
        stepResult[1] = EAbyGFE.click();

        // Step2: Install and launch GD app which support easy activation.
        Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));

        // Step3: Select GFE as delegate.
        stepResult[2] = true;

        Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));

        // Provision GD app using GFE as auth delegate
        UiObject passwordInput = new UiObject(
                new UiSelector().resourceId("com.good.android.gfe:id/lock_screen_unlock_password"));
        passwordInput.click();
        passwordInput.setText("a");

        // sleep 1s for waiting keyboard hiding.
        sleep(1000);
        UiObject okbutton = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/lock_screen_ok"));
        okbutton.click();

        // sleep 300s to wait for provision successfully.
        UiObject EAprovisionsuccessfully = new UiObject(new UiSelector().text("Tap to navigate"));
        stepResult[3] = EAprovisionsuccessfully.waitForExists(300000);
        Tools.doubleLog("@@Step4 result -> " + Tools.getResult(stepResult[3]));

    }

}