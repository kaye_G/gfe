package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;
import com.good.android.gfetest.Frequency.Activity;

public class ID06SubscribeContactSubfolder extends UiAutomatorTestCase {
	UiObject subSync = null;

	Frequency freq = new Frequency();
	Nodes_Preferences np = new Nodes_Preferences();
	Nodes_Contacts nc = new Nodes_Contacts();
	private final String subfolderSync = "Subfolder Sync";
	private final String conFolder1 = "CS1";
	private final String conFolder2 = "CS2";
	private int errorCode = 0;
	boolean[] stepResult = new boolean[6];
	String starts, ends;
	
	public void testID06() {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testID06()");
		UiDevice device = getUiDevice();
		freq.launchGFE(device);
		freq.setStepResult(stepResult);
/**
 * Step1. Device: Go to Preferences->Contacts->Subfolder sync and select one subfolder to subscribe.		
 */
		try {
			Tools.logInfor("Start Step1 ...");
			subSync = freq.findItemInPreferences(subfolderSync);
			freq.screenCapture(device);
			subSync.waitForExists(freq.tea);
			subSync.clickAndWaitForNewWindow();
			this.stepResult[0] = freq.subscribeContactFolder(conFolder1);
			freq.screenCapture(device);
			Tools.logInfor("Waiting for contacts subfolder sync...30s ");
			sleep(freq.tea * 6);
/**
 * Step2. Device: Go to Contact tab and navigate to the subscribed subfolder.			
 */
			Tools.logInfor("Start Step2 ...");
			freq.gotoActivity(Activity.Contacts);
			freq.screenCapture(device);
			freq.gotoContactsFolder(conFolder1);
			freq.screenCapture(device);
			this.stepResult[1] = freq.isCurrentContactsFolderEquals(conFolder1);
			long startsT = new Date().getTime();
			while(freq.isContactSubfolderEmpty()) {
				int inter = 5 * freq.read - 6 * freq.tea;
				if(!Tools.timer(startsT, inter, "")) {
					Tools.logInfor(">>>>>>@Error: No contacts sync to the subscribed Contacts subfolder!!!!!");
					break;
				}
				sleep(freq.tea);
			}
			this.stepResult[1] &= !freq.isContactSubfolderEmpty();
			freq.screenCapture(device);
/**
 * Step3. Device: Go to preference->Contacts->Subfolder sync and check another subfolders in the list.
 */
			Tools.logInfor("Start Step3 ...");
			subSync = freq.findItemInPreferences(subfolderSync);
			freq.screenCapture(device);
			subSync.waitForExists(freq.tea);
			subSync.clickAndWaitForNewWindow();
			this.stepResult[2] = freq.subscribeContactFolder(conFolder2);
			freq.screenCapture(device);
			Tools.logInfor("Waiting for contacts subfolder sync...30s ");
			sleep(freq.tea * 6);
/**
 * Step4. Device: Go to Contact tab and navigate this subfolders.
 */
			Tools.logInfor("Start Step4 ...");
			freq.gotoActivity(Activity.Contacts);
			freq.screenCapture(device);
			freq.gotoContactsFolder(conFolder2);
			freq.screenCapture(device);
			this.stepResult[3] = freq.isCurrentContactsFolderEquals(conFolder2);
			startsT = new Date().getTime();
			while(freq.isContactSubfolderEmpty()) {
				int inter = 5 * freq.read - 6 * freq.tea;
				if(!Tools.timer(startsT, inter, "")) {
					Tools.logInfor(">>>>>>@Error: No contacts sync to the subscribed Contacts subfolder!!!!!");
					break;
				}
				sleep(freq.tea);
			}
			this.stepResult[3] &= !freq.isContactSubfolderEmpty();
			freq.screenCapture(device);
/**
 * Step5. Device: Go to Preference->Contacts-Subfolder Sync and un-check the subfolder in step3 to stop sync.			
 */
			Tools.logInfor("Start Step5 ...");
			subSync = freq.findItemInPreferences(subfolderSync);
			freq.screenCapture(device);
			subSync.waitForExists(freq.tea);
			subSync.clickAndWaitForNewWindow();
			this.stepResult[4] = freq.unSubscribeContactFolder(conFolder2);
			freq.screenCapture(device);
			Tools.logInfor("Waiting for contacts subfolder sync...30s ");
			sleep(freq.tea * 6);
/**
 * Step6. Device: Go to Contact tab and navigate the subfolder.
 */
			Tools.logInfor("Start Step6 ...");
			freq.gotoActivity(Activity.Contacts);
			sleep(freq.tea);
			freq.screenCapture(device);
			freq.gotoContactsFolder(conFolder2);
			freq.screenCapture(device);
			this.stepResult[5] = freq.isCurrentContactsFolderEquals(conFolder2);
			boolean flagC2 = freq.isContactSubfolderEmpty();
			if(flagC2) {
				Tools.logInfor(">>>>>>@Pass: Contacts subfolder [" + conFolder2 + "] is empty");
			} else {
				Tools.logInfor(">>>>>>@Error: Contacts subfolder [" + conFolder2 + "] IS NOT empty even though it's been unsubscribed!!!!!");
			}
			this.stepResult[5] &= flagC2;
			freq.screenCapture(device);
			freq.gotoContactsFolder(conFolder1);
			freq.screenCapture(device);
			this.stepResult[5] &= freq.isCurrentContactsFolderEquals(conFolder1);
			boolean flagC1 = !freq.isContactSubfolderEmpty();
			if(flagC1) {
				Tools.logInfor(">>>>>>@Pass: Contacts subfolder [" + conFolder1 + "] isn't empty, due to it's still been subscribed");
			} else {
				Tools.logInfor(">>>>>>@Error: Contacts subfolder [" + conFolder1 + "] IS empty even though it's NOT been subscribed!!!!!");
			}
			this.stepResult[5] &= flagC1;
			freq.screenCapture(device);
		} catch(UiObjectNotFoundException e) {
			if (errorCode == 0) {
				errorCode = 400;
				Tools.logInfor("ErrorCode=400");
			}
			Tools.logInfor(e);
		} catch(Exception e) {
			Tools.logInfor(e);
		} finally {
			ends = date.format(new Date());
			Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
			Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
			Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));
			Tools.doubleLog("@@Step4 result -> " + Tools.getResult(stepResult[3]));
			Tools.doubleLog("@@Step5 result -> " + Tools.getResult(stepResult[4]));
			Tools.doubleLog("@@Step6 result -> " + Tools.getResult(stepResult[5]));
			Tools.doubleLog("@@Smoke ID06 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
			Tools.writeData("06," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
		}
	}
}
