/**
 * Date: Feb 11, 2015
 * Author: Katharine Ye
 */
package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;
import com.good.android.gfetest.Frequency.Activity;

public class ID10SendThrough extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
	private final String emailRecipient = "katharine4@asia.qagood.com";
	private int errorCode = 0;
	private final String emailSubject = "SmokeID10@" + new Date().getTime();
	private final String emailBody = "test@" + new Date().getTime();
	private final int attNum = 1;
	boolean[] stepResult = {false, false, false};
	
	public void testID10() {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testID10()");
		
		UiDevice device = getUiDevice();
		freq.launchGFE(device); //Launch then unlock GFE if GFE is password protected 
		freq.removeAppsFromRecent(device);
		device.pressHome();
		sleep(freq.breath);
		
		/**Step 1. Open a document via 3rd party editor "Hancom Office 2014" and don't edit the document; */
		Tools.logInfor("Start Step1 ...");
		try {
			freq.findAppFromHome("Hancom Office 2014");
			sleep(freq.breath);
			new UiObject(new UiSelector().className("android.widget.TextView").text("All documents")).clickAndWaitForNewWindow();
			sleep(freq.hearbeat);
			UiObject checkBtn = new UiObject(new UiSelector().className("android.widget.LinearLayout").resourceId("com.hancom.office.editor:id/file_menu_check").description("Selection"));
			checkBtn.waitForExists(5*freq.hearbeat);
			checkBtn.click();
			sleep(freq.breath);
			UiObject doc = new UiObject(new UiSelector().className("android.widget.LinearLayout").resourceId("com.hancom.office.editor:id/filelist_item_frame").index(0));
			doc.waitForExists(5*freq.hearbeat);
			if(doc.exists() && doc.isEnabled()) {
				doc.longClick();
				sleep(3*freq.hearbeat);
				UiObject shareBtn = new UiObject(new UiSelector().className("android.widget.LinearLayout").resourceId("com.hancom.office.editor:id/file_menu_send").description("Share"));
				shareBtn.waitForExists(5*freq.hearbeat);
				if(shareBtn.exists()) {
					stepResult[0] = true; //Step 1: Pass
					shareBtn.click();
					sleep(freq.hearbeat);
				}
		/**Step 2. Press 'Share/Send' and select Good for Enterprise; */
			Tools.logInfor("Start Step2 ...");
				if(new UiObject(new UiSelector().resourceId("android:id/title").text("Share")).exists()) {
					UiObject good = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("android:id/text1").text("Good"));
					good.waitForExists(5*freq.hearbeat);
					good.clickAndWaitForNewWindow(); //Share document to GFE
				}
			}
			sleep(2*freq.hearbeat);
			UiObject clipBtn = new UiObject(new UiSelector().className("android.widget.Button").resourceId("com.good.android.gfe:id/subject_attachments_button"));
			clipBtn.waitForExists(3*freq.tea);
			if(Integer.parseInt(clipBtn.getText()) == attNum) {
				Tools.logInfor("There are (" + attNum + ") files added to GFE");
				stepResult[1] = true; //Step 2: Pass
			}
			freq.enterMsgRecipent("To", emailRecipient);
			sleep(freq.blink);
			freq.enterMsgText("Subject", emailSubject);
			sleep(freq.blink);
			freq.enterMsgText("Body", emailBody);
			sleep(freq.blink);
		/** Step 3. Select Send; */
			Tools.logInfor("Start Step3 ...");
			freq.sendMsg();
			sleep(freq.blink);
			device.pressHome();
			freq.gotoActivity(Activity.Email);
			sleep(freq.blink);
			stepResult[2] = freq.isMsgSentOut(emailSubject); //Step 3: Pass
			if ((!this.stepResult[2]) && (this.errorCode==0)) {
				this.errorCode = freq.errorCode;
				Tools.logInfor("ErrorCode=" + errorCode);
			}
		} catch(UiObjectNotFoundException e) {
			if (errorCode == 0) {
				errorCode = 400;
				Tools.logInfor("ErrorCode=400");
			}
			Tools.logInfor(e);
		} catch(Exception e) {
			Tools.logInfor(e);
		} finally {
			String ends = date.format(new Date());
			freq.gotoActivity(Activity.Email);
			Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
			Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
			Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));
			Tools.doubleLog("@@Smoke ID10 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
			Tools.writeData("10," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
		}
	}
}
