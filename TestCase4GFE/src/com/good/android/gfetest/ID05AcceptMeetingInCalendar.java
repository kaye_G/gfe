package com.good.android.gfetest;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;
import com.good.android.gfetest.Frequency.Activity;
import com.good.android.gfetest.Frequency.CalendarMode;

public class ID05AcceptMeetingInCalendar extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
	Nodes_Calendar nc = new Nodes_Calendar();
	String starts, ends;
	boolean[] stepResult = new boolean[6];
	private static String meetingSubject = "S05";
	private int errorCode = 0;
	
	public void testID05() {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testID05()");
		UiDevice device = getUiDevice();
		freq.setStepResult(stepResult);
		try {
			String ts = Tools.readData();
			meetingSubject += ts;
		} catch (IOException e) {
			Tools.logInfor(e);
		}
		freq.launchGFE(device);
/**
 * Step1. Go to the Calendar view in the GMM invitee.		
 */
		Tools.logInfor("Start Step1 ...");
		freq.gotoActivity(Activity.Calendar); //Go to Calendar tab;
		device.takeScreenshot(Tools.setFile(new Date().getTime()));
		String calView = freq.getCurrentCalendarView();
		if(!calView.equals("month")) {
			freq.switchToCalenderMode(CalendarMode.Month);
		}
/**
 * Step2. Tap on the Meeting.
 */
		Tools.logInfor("Start Step2 ...");
		try {
			this.stepResult[0] = freq.findEventInCalendar(meetingSubject, device); 
			errorCode = freq.errorCode;
			freq.screenCapture(device);
			if(!stepResult[0]) {
				Tools.logInfor("Error: Fail to found event [" + meetingSubject + "]");
			} else {
				freq.openEventInCalendarMonthView(meetingSubject);
				UiObject actBtn = nc.getActionBtnInCalendarDetailView();
				actBtn.waitForExists(freq.tea);
				UiObject delBtn = nc.getDeleteBtnInCalendarDetailView();
				delBtn.waitForExists(freq.tea);
				UiObject respondBtn = nc.getRespondBtnInCalendarDetailView();
				respondBtn.waitForExists(freq.tea);
				if(actBtn.exists() && delBtn.exists() && respondBtn.exists()) {
					this.stepResult[1] = true;
					freq.screenCapture(device);
					Tools.logInfor("Meeting detail view appears with bottom action bar which contains 'Action(response icon)', 'Delete' and 'Forward' buttons");
				}
				UiObject showAs = nc.getShowAsStatus();
				showAs.waitForExists(freq.tea);
				String showAsStatus = showAs.getText();
				if(showAsStatus.toLowerCase().equals("tentative")) {
					this.stepResult[1] &= true;
					Tools.logInfor("Meeting status shown as [Tentative]");
				} else {
					this.stepResult[1] &= false;
					freq.screenCapture(device);
					Tools.logInfor("@Error: This step is Failed, due to meeting status should be shown as  [Tentative], but currently shown as [" + showAsStatus + "]");
				}
/**
 * Step3. Tap on the 'Action(response icon)' button.
 */
				Tools.logInfor("Start Step3 ...");
				if(actBtn.exists()) {
					actBtn.clickAndWaitForNewWindow();
					freq.screenCapture(device);
				}
				if(nc.getRespondtoEventText().exists()) {
					UiObject acceptBtn = nc.getActionBtn("Accept");
					acceptBtn.waitForExists(freq.tea);
					UiObject tentativeBtn = nc.getActionBtn("Tentative");
					tentativeBtn.waitForExists(freq.tea);
					UiObject declineBtn = nc.getActionBtn("Decline");
					declineBtn.waitForExists(freq.tea);
					if(acceptBtn.exists() && tentativeBtn.exists() && declineBtn.exists()) {
						this.stepResult[2] = true;
						freq.screenCapture(device);
						Tools.logInfor("Context menu 'Respond to event' with three option 'Accept', 'Tentative' and 'Decline' displayed.");
					}
/**
 * Step4. Select 'Accept' option item and click OK button.
 */
					Tools.logInfor("Start Step4 ...");
					acceptBtn.clickAndWaitForNewWindow();
					freq.tapButton("OK");
					Tools.logInfor("Select to accept the event and tap OK button");
					this.stepResult[3] = true;
					freq.screenCapture(device);
/**
 * Step5. Select one option item and click OK button.
 */
					Tools.logInfor("Start Step5 ...");
					UiObject meetingAccepted = nc.getMeetingAcceptedText();
					meetingAccepted.waitForExists(freq.tea);
					UiObject defaultResponse = nc.getMeetingAcceptedOption("Send response now");
					defaultResponse.waitForExists(freq.tea);
					UiObject responseWithCommend = nc.getMeetingAcceptedOption("Respond with comment");
					responseWithCommend.waitForExists(freq.tea);
					UiObject noResponse = nc.getMeetingAcceptedOption("No response");
					noResponse.waitForExists(freq.tea);
					if(meetingAccepted.exists()) {
						if(defaultResponse.exists() && responseWithCommend.exists() && noResponse.exists()) {
							Tools.logInfor("Context menu 'Meeting Accepted' with three option 'Send response now', 'Respond with comment' and 'No response' displayed");
							defaultResponse.clickAndWaitForNewWindow();
							freq.tapButton("OK");
							this.stepResult[4] = true;
							freq.screenCapture(device);
						}
					}
					UiObject event = nc.getEventTitle(meetingSubject);
					event.waitForExists(freq.tea);
					if(event.exists()) {
						this.stepResult[4] &= true;
						freq.screenCapture(device);
					}
/**
 * Step6. Open the meeting again.
 */		
					Tools.logInfor("Start Step6 ...");
					actBtn = nc.getActionBtnInCalendarDetailView();
					actBtn.waitForExists(freq.tea);
					delBtn = nc.getDeleteBtnInCalendarDetailView();
					delBtn.waitForExists(freq.tea);
					respondBtn = nc.getRespondBtnInCalendarDetailView();
					respondBtn.waitForExists(freq.tea);
					if(actBtn.exists() && delBtn.exists() && respondBtn.exists()) {
						this.stepResult[5] = true;
						freq.screenCapture(device);
						Tools.logInfor("Meeting detail view appears with bottom action bar which contains 'Action(response icon)', 'Delete' and 'Forward' buttons");
					}
					showAs = nc.getShowAsStatus();
					showAs.waitForExists(freq.tea);
					showAsStatus = showAs.getText();
					if(showAsStatus.toLowerCase().equals("busy")) {
						this.stepResult[5] &= true;
						Tools.logInfor("Meeting status shown as [Busy]");
					} else {
						this.stepResult[5] &= false;
						freq.screenCapture(device);
						Tools.logInfor("Meeting status shown as [" + showAsStatus + "]");
					}
					freq.screenCapture(device);
				}
				freq.gotoActivity(Activity.Email);
			}
		} catch(UiObjectNotFoundException e) {
			if (errorCode == 0) {
				errorCode = 400;
				Tools.logInfor("ErrorCode=400");
			}
			Tools.logInfor(e);
		} catch(Exception e) {
			Tools.logInfor(e);
		} finally {
			ends = date.format(new Date());
			Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
			Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
			Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));
			Tools.doubleLog("@@Step4 result -> " + Tools.getResult(stepResult[3]));
			Tools.doubleLog("@@Step5 result -> " + Tools.getResult(stepResult[4]));
			Tools.doubleLog("@@Step6 result -> " + Tools.getResult(stepResult[5]));
			Tools.doubleLog("@@Smoke ID05 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
			Tools.writeData("05," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
System.out.println(errorCode);
		}
	}
}
