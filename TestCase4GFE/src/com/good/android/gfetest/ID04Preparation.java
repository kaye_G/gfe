package com.good.android.gfetest;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.good.android.gfetest.Frequency.Respond;
import com.good.android.gfetest.Frequency.RespondMsg;
import java.io.*;

public class ID04Preparation extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
	Nodes_Calendar nc = new Nodes_Calendar();
	private static String eventSub = "S04";
	private static final String pre = "";
	private boolean flag = false;
	
	public void testID04Pre() {
		Tools.doubleLog("==========================================================================================");
		Tools.doubleLog("Start to execute method testID04Pre()");
		try {
			String ts = Tools.readData();
			eventSub += ts;
		} catch (IOException e) {
			Tools.logInfor(e);
		}
		UiDevice device = getUiDevice();
		freq.launchGFE(device);
		flag = freq.openMsg(pre + eventSub);
		sleep(freq.hearbeat);
		if(flag) {
			Tools.logInfor("Found meeting request [" + eventSub + "]");
			freq.handleMeetingRequest(Respond.Accept, RespondMsg.DefaultMsg);
			Tools.logInfor("Preparation for TC04 is Done!");
		}
	}
}
