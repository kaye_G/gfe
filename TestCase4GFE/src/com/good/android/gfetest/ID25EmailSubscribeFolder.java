package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiSelector;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.good.android.gfetest.Frequency.Activity;

public class ID25EmailSubscribeFolder extends UiAutomatorTestCase {
	private int errorCode = 0;

	public void testID25EmailSubscribeFolder() throws UiObjectNotFoundException {
		boolean[] stepResult = new boolean[] { false, false, false, false,
				false, false };
		Frequency freq = new Frequency();

		Tools.doubleLog("==========================================================================================");
		Tools.doubleLog("Start to execute method testID25()");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String starts = date.format(new Date());
		try {
			UiDevice device = getUiDevice();
			freq.launchGFE(device);
			// freq.gotoActivity(Activity.Email);
			/**
			 * **********Precondition********** Have a folder in outlook having
			 * more than 100 emails in the folder.
			 * 
			 * In this case, need to create a subfolder under Inbox and
			 * namedSubfolderABC *
			 */

			/**
			 * **********Step1********** Step1. Provision GFE and Launch.
			 * 
			 * Set this steps is true because GFE already been provisioned
			 * previously.
			 */
			stepResult[0] = true;
			Tools.doubleLog("@@Step1 result -> "
					+ Tools.getResult(stepResult[0]));

			/**
			 * **********Step2********** Step2. Go to Preferences.
			 */
			freq.gotoActivity(Activity.Preferences);
			stepResult[1] = true;
			Tools.doubleLog("@@Step2 result -> "
					+ Tools.getResult(stepResult[1]));

			/**
			 * **********Step3********** Step3. Tap on Folder sync option.
			 */
			UiObject EmailFolderSync = new UiObject(new UiSelector()
					.resourceId("android:id/title").text("Folder Sync"));
			stepResult[2] = EmailFolderSync.click();
			Tools.doubleLog("@@Step3 result -> "
					+ Tools.getResult(stepResult[2]));

			/**
			 * **********Step4********** Step4. Tap on one folder to subscribe.
			 * In this case, tap on folder "SubfolderABC"
			 * 
			 */
			UiObject CheckSubfolderABC = new UiObject(new UiSelector()
					.resourceId("com.good.android.gfe:id/folder_name")
					.textContains("SubfolderABC"));

			if (CheckSubfolderABC.exists()) {
				CheckSubfolderABC.click();

			} else {
				UiObject ExpandSubfolder = new UiObject(
						new UiSelector()
								.className("android.widget.LinearLayout")
								.index(0)
								.childSelector(
										new UiSelector()
												.resourceId("com.good.android.gfe:id/child_folders")));
				ExpandSubfolder.click();
				CheckSubfolderABC.click();

			}
			getUiDevice().pressBack();
			stepResult[3] = true;

			Tools.doubleLog("@@Step4 result -> "
					+ Tools.getResult(stepResult[3]));

			/**
			 * **********Step5********** Step5. Go to email and select the
			 * folder subscribed in step 4 from folder view.
			 */
			// Scroll tab to left then Tap on Email
			freq.gotoActivity(Activity.Email);
			// Tap Drawer button then goto subflolder
			freq.tapDrawer();

			stepResult[4] = CheckSubfolderABC.click();
			Tools.doubleLog("@@Step5 result -> "
					+ Tools.getResult(stepResult[4]));

			/**
			 * // **********Step6********** Step6. Wait for the mails to sync
			 * and observe. Open the first email then go go Email detail view,
			 * then check if all 100 email arrived.
			 * 
			 */
			UiObject EmailSubjectContent = new UiObject(new UiSelector()
					.resourceId("com.good.android.gfe:id/subject")
					.textContains("Automated Email ID[100]"));
			EmailSubjectContent.waitForExists(180000);
			freq.screenCapture(device);
			EmailSubjectContent.click();

			// Check if all 100 email arrived
			UiObject EmailCountExpect = new UiObject(new UiSelector()
					.resourceId("com.good.android.gfe:id/count").text(
							"1 of 100"));
			stepResult[5] = EmailCountExpect.waitForExists(120000);
			freq.screenCapture(device);
			UiObject EmailCountCurrently = new UiObject(
					new UiSelector()
							.resourceId("com.good.android.gfe:id/count"));
			Tools.doubleLog("Currently email sync count is "
					+ EmailCountCurrently.getText());
			if (!EmailCountCurrently.getText().equals("1 of 100")) {
				errorCode = 4;
			}
			Tools.doubleLog("@@Step6 result -> "
					+ Tools.getResult(stepResult[5]));

		} catch (UiObjectNotFoundException e) {
			if (errorCode == 0) {
				errorCode = 400;
				Tools.logInfor("ErrorCode=400");
			}
			Tools.logInfor(e);
		} catch (Exception e) {
			Tools.logInfor(e);
		}

		String ends = date.format(new Date());
		freq.gotoActivity(Activity.Email);
		Tools.doubleLog("@@Smoke ID25 result -> "
				+ Tools.getResult(Tools.isTrue(stepResult)));
		Tools.writeData("25,"
				+ Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5)
				+ "," + starts + "," + ends + "," + errorCode);
	}

}
