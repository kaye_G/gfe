package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;
import com.good.android.gfetest.Frequency.Activity;



public class ID11EMLViewer extends UiAutomatorTestCase {
	private final String msgSubject = "SmokeID11";
	private int errorCode = 0;
	private final int attachment = 2;
	boolean[] stepResult = {false, false, false, false, false};
	Frequency freq = new Frequency();
	String starts, ends;
	
	public void testID11() {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testID11()");
		UiDevice device = getUiDevice();
		freq.launchGFE(device);
		try {
	/**
	 * Step 1. Sender Outlook: send email with attached .msg and .eml files;
	 * Step 2. Receiver device: receive and open email. Go to Attachment Manager;
	 */
			Tools.logInfor("Start Step1&2 ...");
			freq.gotoActivity(Activity.Email); //Launch GFE and go to email tab;
			sleep(freq.blink);
			stepResult[1] = freq.openMsg(msgSubject, attachment, stepResult[1]); //Open email, go to Attachment Manager screen;
			if(stepResult[1]) {
				stepResult[0] = true;
			} //Check result of Step1 and Step2;
	/**
	 * Step 3. Receiver device: tap on received files to download them;
	 * Step 4. Receiver device: tap on downloaded file to open it;
	 * Step 5. Repeat steps 2-3 for files that were sent as .msg and .eml;
	 */
			Tools.logInfor("Start Step3&4&5 ...");
			Tools.logInfor("testID11 -> Method downloadAttachments() is called");
			stepResult[2] = freq.downloadAttachments(device, attachment, stepResult[2]);
			stepResult[3] = (stepResult[2] == true) ? true : false; 
			stepResult[4] = (stepResult[3] == true) ? true : false; //Check results of Step3, Step4 and Step5;
			freq.gotoActivity(Activity.Email);
		} catch(Exception e) {
			if (errorCode == 0) {
				errorCode = 400;
				Tools.logInfor("ErrorCode=400");
			}
			Tools.logInfor(e);
		} finally {
			ends = date.format(new Date());
			Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
			Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
			Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));
			Tools.doubleLog("@@Step4 result -> " + Tools.getResult(stepResult[3]));
			Tools.doubleLog("@@Step5 result -> " + Tools.getResult(stepResult[4]));
			Tools.doubleLog("@@Smoke ID11 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
			Tools.writeData("11," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
		}
	}
}
