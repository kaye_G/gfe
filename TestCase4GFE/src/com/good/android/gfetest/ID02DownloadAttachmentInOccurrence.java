package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;
import com.good.android.gfetest.Frequency.Activity;
import com.good.android.gfetest.Frequency.CalendarMode;

public class ID02DownloadAttachmentInOccurrence extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
	private String eventSub = "S02";
	private int errorCode = 0;
	boolean[] stepResult = new boolean[4];
	int num = 3;
	String starts, ends;
	
	public void testID02() {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		starts = date.format(new Date());
		freq.setStepResult(stepResult);
		Tools.doubleLog("Start to execute method testID02()");
		UiDevice device = getUiDevice();
		freq.launchGFE(device);
		freq.gotoActivity(Activity.Calendar);
/**
 * Step1. Desktop: Create a recurring meeting with attachments on Organizer's desktop.
 */
		
		try {
			String ts = Tools.readData();
			eventSub += ts;
			Tools.logInfor("Start Step1 ...");
			freq.backToToday();
			UiObject eventObj = freq.findEventInCalendar(eventSub, CalendarMode.Month); //Find the meeting in Month view
			errorCode = freq.errorCode;
			freq.screenCapture(device);
			if(eventObj.waitForExists(3 * freq.tea)) {
				this.stepResult[0] = true;
				eventObj.clickAndWaitForNewWindow(); //Open the meeting;
				Tools.logInfor("Open meeting [" + eventSub + "] in Monthly view");
				freq.printEventStartAndEndTime();
/**
 * Step2. Device: Reschedule an occurrence on device. Save the update.
 */
				Tools.logInfor("Start Step2 ...");
				freq.accessEditRecurringEventScreen("occurrence");
				device.pressBack(); //Remove keyboard
				freq.editTimeAndDate("Starts");
				this.stepResult[1] = freq.tapIncrementBtn(3);
				freq.tapDoneBtn();
				this.stepResult[1] &=freq.tapSaveBtn();
/**
 * Step3. Wait for a moment.
 */
				Tools.logInfor("Start Step3 ...");
				sleep(6 * freq.tea);
				this.stepResult[2] = true;
				freq.backToToday();
/**
 * Step4. Check the attachments in each occurrence on both device and desktop.
 */
				this.stepResult[3] = true;
				for(int i=0; i<2; i++) {
					eventObj = freq.findEventInCalendar(eventSub, CalendarMode.Month); //Find the meeting in Month view
					freq.screenCapture(device);
					if(eventObj.waitForExists(3 * freq.tea)) {
						eventObj.clickAndWaitForNewWindow(); //Open the rescheduled occurrence;
						Tools.logInfor("Open meeting [" + eventSub + "] in Monthly view");
						freq.printEventStartAndEndTime();
						this.stepResult[3] &= freq.clickClipIconDownloadAttachmentsInEventDetailView(device, num);
						device.pressBack();
					}
					freq.scrollEventListInMonthView(); //Scroll event list in Month view;
				}
			} else {
				Tools.logInfor(">>>>>Error: Fail to find event " + eventSub + "!!!");
			}
		} catch(UiObjectNotFoundException e) {
			if (errorCode == 0) {
				errorCode = 400;
				Tools.logInfor("ErrorCode=400");
			}
			Tools.logInfor(e);
		} catch(Exception e) {
			Tools.logInfor(e);
		} finally {
			ends = date.format(new Date());
			Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
			Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
			Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));
			Tools.doubleLog("@@Step4 result -> " + Tools.getResult(stepResult[3]));
			Tools.doubleLog("@@Smoke ID02 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
			Tools.writeData("02," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
//System.out.println(errorCode);
		}
	}
}
