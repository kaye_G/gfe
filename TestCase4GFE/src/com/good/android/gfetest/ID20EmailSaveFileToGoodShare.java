package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;

public class ID20EmailSaveFileToGoodShare extends Frequency {

    Frequency freq = new Frequency();
    boolean[] stepResult = new boolean[]{false,false,false};
    String starts, ends;

    public void testID20() {
        Tools.doubleLog("==========================================================================================");
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        starts = date.format(new Date());
        Tools.doubleLog("Start to execute method testID20()");
        UiDevice device = getUiDevice();
        freq.setStepResult(stepResult);

        try {
            Tools.logInfor("Start Step1 ...");
            /**
             * Step1.Launch GFE
             * 
             * 
             * Force stop GoodShare then launch GFE and GoodShare as working
             * around for stuck at authenticating screen after GFE was re
             * provisioned.
             */
            getUiDevice().pressHome();
            freq.forceStopApp(freq.findAppFromAppManager("Good Share"));
            sleep(freq.blink);

            freq.launchGFE(device);
            launchAppCalled("Good Share");
            freq.launchGFE(device);

            /**
             * Step2.Navigate to Doc tab
             */
            Tools.logInfor("Start Step2 ...");
            freq.gotoActivity(Activity.Documents);
            UiObject ComposeText = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/title")
                    .textContains("Documents"));
            stepResult[0] = true;
            stepResult[1] = ComposeText.exists();

            /**
             * Step3.Select a file then save to GD app, in this case GD app may
             * Good Share Step3-results: GD app should be launched, and the file
             * should be opened in GD app.
             */
            Tools.logInfor("Start Step3 ...");

            // Capture Picture
            // Tap add photo button.
            UiObject AddPhoto = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/add_photo"));
            AddPhoto.clickAndWaitForNewWindow();

            // Then add photo by Take Photo
            UiObject TakePhoto = new UiObject(new UiSelector().text("Take photo"));
            TakePhoto.clickAndWaitForNewWindow();

            freq.takePhoto();
            // Get Photo name
            UiObject FileName = new UiObject(
                    new UiSelector().resourceId("com.good.android.gfe:id/rename_activity_filename_edittext"));
            String Originalnamewithoutext = FileName.getText();
            String Originalname = Originalnamewithoutext + ".jpg";
            Tools.logInfor(Originalname);

            // Save photo in GFR
            UiObject SaveButton = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/save_button"));
            SaveButton.clickAndWaitForNewWindow();

            // Chose the photo then save it in GoodShare
            UiScrollable listViews = new UiScrollable(new UiSelector().className("android.widget.ListView"));
            listViews.scrollTextIntoView(Originalname);

            UiObject SavePhoto = new UiObject(new UiSelector().resourceId("com.good.android.gfe:id/info_container")
                    .childSelector(
                            new UiSelector().resourceId("com.good.android.gfe:id/display_name").text(Originalname)));

            SavePhoto.longClick();
            // Save photo to GoodShare by tap "Save As"
            UiObject SaveAs = new UiObject(new UiSelector().resourceId("android:id/title").text("Save As"));
            SaveAs.clickAndWaitForNewWindow();
            SaveButton.clickAndWaitForNewWindow();

            sleep(2000);

            UiObject goBackDataSource = new UiObject(new UiSelector().resourceId("android:id/home"));
            goBackDataSource.clickAndWaitForNewWindow(60000);

            // Select file in GoodShare
            freq.selectFolderInGoodShare("Data Sources");
            sleep(10000);
            freq.selectFolderInGoodShare("GS-SP10");
            freq.selectFolderInGoodShare("goodqa1");
            freq.selectFolderInGoodShare("Shared Documents");
            freq.selectFolderInGoodShare("AndroidGFEtest1234");
            freq.tapOverflowSubItemInGoodShare("Sort");
            freq.sortInGoodShare("Date", "Descending");

            UiObject FileNameInGoodShare = new UiObject(new UiSelector().resourceId("com.good.goodshare:id/title")
                    .instance(0));
            String NewName = FileNameInGoodShare.getText();
            Tools.logInfor(NewName);
            stepResult[2] = Originalname.equals(NewName);

        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        } catch (Exception e) {
            Tools.logInfor(e);
        }
        ends = date.format(new Date());
        Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
        Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
        Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));

        Tools.doubleLog("@@Smoke ID20 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
        Tools.writeData("20," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends);

    }

}
