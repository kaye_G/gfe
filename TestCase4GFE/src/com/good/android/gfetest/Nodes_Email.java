package com.good.android.gfetest;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;

public class Nodes_Email extends UiAutomatorTestCase {
/* Nodes in Email List View */
	/**
	 * Returns the scrollable collection for Email List View.
	 * @return The scrollable collection for Email List View.
	 */
	public UiScrollable getEmailListView() {
		UiScrollable emalListView = new UiScrollable(new UiSelector().className("android.widget.ListView").scrollable(true));
		return emalListView;
	}
	
	/**
	 * Set the search criteria to match the Flag icon in Email List View.
	 * @return UiSelector with the specified search criteria.
	 */
	public UiSelector getFlagIcon() {
		UiSelector flag = new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/followup");
		return flag;
	}
	
	/**
	 * Returns a UI element for the title of Email Folder, call the method in Email Activity.
	 * @return A UI element for the title of Email Folder.
	 */
	public UiObject getTitleInEmailView() {
		UiObject titleObj = new UiObject(new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/title"));
		return titleObj;
	}
	
	/**
	 * Returns a UI element for flag icon of email, call the method in Email List View.
	 * @return A UI element for flag icon of email, call the method in Email List View.
	 */
	public UiObject getFlagIconInEmailListView() {
		UiObject flagIconObj = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/followup"));
		return flagIconObj;
	}
	
	
/* Nodes in Email Detail View */
	/**
	 * Set the search criteria to match the Email Subject in Email Detail View.
	 * @return UiSelector with the specified search criteria.
	 */
	public UiSelector getEmailSubject() {
		UiSelector sub = new UiSelector().className("android.widget.TextView").resourceId("com.good.android.gfe:id/subject");
		return sub;
	} 
	
	/**
	 * Set the search criteria to match the icons_container in Email Detail View.
	 * @return UiSelector with the specified search criteria.
	 */
	public UiSelector getIconsContainer() {
		UiSelector icons = new UiSelector().className("android.widget.LinearLayout").resourceId("com.good.android.gfe:id/icons_container");
		return icons;
	}
	
	/**
	 * Returns a UI element for back_to_list_button, call the method in Email Detail View.
	 * @return A UI element for back_to_list_button.
	 */
	public UiObject getBackToListBtn() {
		UiObject backBtnObj = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/back_to_list_button"));
		return backBtnObj;
	}
	
	/**
	 * Returns a UI element for Flag button, call the method in Email Detail View.
	 * @return A UI element for Flag button.
	 */
	public UiObject getFlagBtn() {
		UiObject flagBtnObj = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/tick_menu"));
		return flagBtnObj;
	}
	
	/**
	 * Returns a UI element for Read/Unread button, call the method in Email Detail View.
	 * @return A UI element for Read/Unread button.
	 */
	public UiObject getReadUnreadBtn() {
		UiObject readBtnObj = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/unread_menu"));
		return readBtnObj;
	}
	
	/**
	 * Returns a UI element for Move_To_Folder button, call the method in Email Detail View.
	 * @return A UI element for Move_To_Folder button.
	 */
	public UiObject getMovetoFolderBtn() {
		UiObject moveBtnObj = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/move_to_folder"));
		return moveBtnObj;
	}
	
	/**
	 * Returns a UI element for Delete button, call the method in Email Detail View.
	 * @return A UI element for Delete button.
	 */
	public UiObject getDeleteBtn() {
		UiObject delBtnObj = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/delete"));
		return delBtnObj;
	}
	
	/**
	 * Returns a UI element for Respond button, call the method in Email Detail View.
	 * @return A UI element for Respond button.
	 */
	public UiObject getRespondBtn() {
		UiObject resBtnObj = new UiObject(new UiSelector().className("android.widget.ImageView").resourceId("com.good.android.gfe:id/reply_button"));
		return resBtnObj;
	}
	
	/**
	 * Returns a UI element for Flag Context Menu, call the method in Email Detail View.
	 * @return A UI element for Flag Context Menu, call the method in Email Detail View.
	 */
	public UiObject getFlagMenu() {
		UiObject flagMenu = new UiObject(new UiSelector().className("android.widget.ListView").resourceId("android:id/select_dialog_listview"));
		return flagMenu;
	}
}
