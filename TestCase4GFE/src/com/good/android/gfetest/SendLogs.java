package com.good.android.gfetest;

import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.android.uiautomator.core.*;
import com.good.android.gfetest.Frequency.Activity;

public class SendLogs extends UiAutomatorTestCase {

    Frequency freq = new Frequency();

    public void testGFESendLogs() throws UiObjectNotFoundException {
        Tools.doubleLog("==========================================================================================");
        Tools.doubleLog("Start to Send log");
        UiDevice device = getUiDevice();
        freq.launchGFE(device);
        try {
            // Go to Preference then tap About
            freq.gotoActivity(Activity.Preferences);
            freq.findItemInPreferences("About").click();

            // Tap send log button
            UiObject SendLogs = new UiObject(new UiSelector().resourceId("android:id/summary").textContains(
                    "Send diagnostic logs to Good Technology"));
            SendLogs.clickAndWaitForNewWindow(2000);

            // Waiting for sending logs complete then tap OK button
            UiObject SendLogsSuccess = new UiObject(new UiSelector().resourceId("android:id/button1")
                    .textContains("OK"));
            UiObject SendLogsRetry = new UiObject(new UiSelector().resourceId("android:id/button1").textContains(
                    "Retry"));

            // Try to resend log 3 times until successfully, else will finish
            // send log.
            for (int i = 1; i < 4; i++) {
                if (SendLogsRetry.waitForExists(60000)) {
                    SendLogsRetry.click();
                    Tools.doubleLog("Send log failed, will try to resend   " + i + "  times");
                }

                else {
                    SendLogsSuccess.waitForExists(60000);
                    SendLogsSuccess.click();
                    Tools.doubleLog("Send log successfully");
                    break;
                }
            }
        } catch (UiObjectNotFoundException e) {
            Tools.logInfor(e);
        } catch (Exception e) {
            Tools.logInfor(e);
        }

    }
}
