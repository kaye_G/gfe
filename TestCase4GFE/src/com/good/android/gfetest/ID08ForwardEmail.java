package com.good.android.gfetest;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.uiautomator.core.*;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
import com.good.android.gfetest.Frequency.Activity;
import com.good.android.gfetest.Frequency.EmailItem;

public class ID08ForwardEmail extends UiAutomatorTestCase {
	Frequency freq = new Frequency();
	boolean[] stepResult = new boolean[3];
	private final String emailSubject = "SmokeID08";
	private final String recipients = "katharine1@asia.qagood.com, asia1new@asia.qagood.com, katharine4@asia.qagood.com";
	private int errorCode = 0;
	private final String body = "forward the email with attach files";
	private final int attNum = 2;
	private final String[] att = {"Attachment01.doC", "beauty04.jpeg"};
	String starts, ends;
	
	public void testID08() {
		Tools.doubleLog("==========================================================================================");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String starts = date.format(new Date());
		Tools.doubleLog("Start to execute method testID08()");
		try {
			freq.setStepResult(stepResult);
			UiDevice device = getUiDevice();
			freq.launchGFE(device);
			freq.gotoActivity(Activity.Email); //Go to Email tab;
			sleep(freq.breath);
/**
 * Step1. Receive the email on device with attachments and open it in detail view;		
 */
			Tools.logInfor("Start Step1 ...");
			freq.selectMsg(emailSubject); //Open the meeting request in email list view;
			freq.screenCapture(device);
			
/**
 * Step2. Select to Forward the email;
 */
			Tools.logInfor("Start Step2 ...");
			if(freq.isToolBarAvailable()) {
				freq.respondMsgWithOption("Forward");
				freq.screenCapture(device);
			}
/**
 * Step3. Enter valid recipients in the To field and add some text in the body of email, send the email;
 */
			Tools.logInfor("Start Step3 ...");
			freq.enterMsgRecipent(EmailItem.To, recipients);
			freq.screenCapture(device);
			freq.enterMsgText(EmailItem.Body, body);
			freq.screenCapture(device);
			freq.sendMsg();
			freq.screenCapture(device);
/**
 * Check result:		
 */
			//Verify that email is received by intended recipient along with the original attachments.
			long startsT = new Date().getTime();
			while(!freq.isMsgExists("FW: " + emailSubject, "Inbox")) {
				sleep(freq.tea);
				//Waiting for 5 min;
				if(!Tools.timer(startsT, 5 * freq.read, "")) {
					errorCode = 1;
					Tools.logInfor("ErrorCode=1");
					break;
				}
			}
			this.stepResult[0] = freq.openMsg("FW: " + emailSubject, attNum, this.stepResult[0]);
			String[] fileName = freq.getAttachmentsName(attNum);
			for(int i=0; i<fileName.length; i++) {
				if(fileName[i].toLowerCase().trim().equals(att[i].toLowerCase().trim())) {
					this.stepResult[0] &= true;
				} else {
					this.stepResult[0] &= false;
				}
			}
			//Recipient should be able to open the attachment files on his device or the email client on desktop.
			this.stepResult[1] = freq.downloadAttachments(device, attNum);
			if ((!this.stepResult[1]) && (this.errorCode!=0)) {
				this.errorCode = freq.errorCode;
				Tools.logInfor("ErrorCode=" + errorCode);
			}
			freq.screenCapture(device);
			device.pressBack();
			//Verify recipient can view the added text in the body.
			UiObject addTextObj = new UiObject(new UiSelector().className("android.view.View").description(body));
			addTextObj.waitForExists(freq.tea);
			freq.screenCapture(device);
			if(addTextObj.exists()) {
				Tools.logInfor("The added text [" + body + "] can be found!");
				this.stepResult[2] = true;
			}
			freq.gotoActivity(Activity.Email);
		} catch(UiObjectNotFoundException e) {
			if (errorCode == 0) {
				errorCode = 400;
				Tools.logInfor("ErrorCode=400");
			}
			Tools.logInfor(e);
		} catch(Exception e) {
			Tools.logInfor(e);
		} finally {
			ends = date.format(new Date());
			Tools.doubleLog("@@Step1 result -> " + Tools.getResult(stepResult[0]));
			Tools.doubleLog("@@Step2 result -> " + Tools.getResult(stepResult[1]));
			Tools.doubleLog("@@Step3 result -> " + Tools.getResult(stepResult[2]));
			Tools.doubleLog("@@Smoke ID08 result -> " + Tools.getResult(Tools.isTrue(stepResult)));
			Tools.writeData("08," + Tools.getResult(Tools.isTrue(stepResult)).substring(1, 5) + "," + starts + "," + ends + "," + errorCode);
//System.out.println(errorCode);
		}
	}

}
